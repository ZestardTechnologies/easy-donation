<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('app_version', 'callbackController@app_version')->name('app_version');
Route::any('usage_charge/{id}', 'callbackController@usage_charge')->name('usage_charge');
Route::get('callback', 'callbackController@index')->name('callback');
Route::get('redirect', 'callbackController@redirect')->name('redirect');
Route::any('change_currency', 'callbackController@Currency')->name('change_currency');
Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');
Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');
Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');
Route::any('getshopid', 'DonationproController@getshopid')->middleware('cors')->name('getshopid');
Route::any('update-modal-status', 'callbackController@update_modal_status')->name('update-modal-status');
/*
 * For Donation updated Version Route List
 */
Route::get('donationpro', 'DonationproController@index')->name('donationpro');
Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'callbackController@dashboard']);
Route::get('donation-pro-index', ['as' => 'donation_pro_index', 'uses' => 'DonationproController@storeIndex']);
Route::post('donation-pro-save', ['as' => 'donation_pro_save', 'uses' => 'DonationproController@storeSave']);
Route::get('donation-list', ['as' => 'datatable_listing_pro', 'uses' => 'DonationproController@dataTableListing']);
Route::get('donation-pro-edit/{id}', ['as' => 'donation_pro_edit', 'uses' => 'DonationproController@storeEdit']);
Route::post('donation-pro-update', ['as' => 'donation_pro_update', 'uses' => 'DonationproController@storeUpdate']);
Route::get('donation-pro-delete/{id}/{dbid}', ['as' => 'donation_pro_delete', 'uses' => 'DonationproController@storeDelete']);
Route::get('donation-basic-delete/{id}/{dbid}', ['as' => 'donation_basic_delete', 'uses' => 'DonationproController@basicStoreDelete']);
Route::get('track-donation', ['as' => 'track_donationpro', 'uses' => 'DonationproController@trackDonation']);
Route::get('settings', ['as' => 'email_configurationpro', 'uses' => 'DonationproController@emailConfigurationGet']);
Route::post('email-config-pro-save', ['as' => 'email_config_pro_save', 'uses' => 'DonationproController@emailConfigProSave']);
Route::get('donation-basic-index', ['as' => 'donation_basic_index', 'uses' => 'DonationproController@basicStoreIndex']);
Route::post('donation-basic-save', ['as' => 'donation_basic_save', 'uses' => 'DonationproController@basicStoreSave']);
Route::get('help', ['as' => 'help_pro', 'uses' => 'DonationproController@helpPro']);
/*
 * 
 */
Route::get('donation_image', 'DonationproController@donationimage')->name('donation_image');
Route::post('DonationproProductId', 'DonationproController@product_delete')->name('DonationproProductId');
Route::get('front_previewpro', 'DonationproController@front_preview')->middleware('cors')->name('front_previewpro');
Route::get('productimagepro', 'DonationproController@productimagepro')->middleware('cors')->name('productimagepro');
Route::any('advance', 'callbackController@advance')->name('advance');
Route::any('basic', 'callbackController@basic')->name('basic');
Route::any('webhook', 'webhookController@webhook')->name('webhook');
Route::any('draft-order', 'webhookController@draft_order')->name('draft-order');
Route::any('create-order', 'webhookController@create_order')->name('create-order');
Route::get('front_preview', 'DonationController@front_preview')->middleware('cors')->name('front_preview');



// Route for basic donation
Route::group(['prefix' => 'basic-donation'], function () {
    Route::get('create-edit/{id?}/{shop?}', 'BasicDonationController@createEdit')->name('basic_donation_create_edit');
    Route::post('save', 'BasicDonationController@save')->name('basic_donation_save');
});


// Route for advance donation
Route::group(['prefix' => 'advance-donation'], function () {
    Route::get('create-edit/{id?}', 'AdvanceDonationController@createEdit')->name('advance_donation_create_edit');
    Route::post('save', 'AdvanceDonationController@save')->name('advance_donation_save');
});

Route::get('preview', 'previewController@index')->middleware('cors')->name('preview');
Route::post('frontend/dropdown/save', 'previewController@donate')->middleware('cors')->name('frontend/dropdown/save');
Route::any('frontend/dropdown/savepro', 'previewController@donatepro')->middleware('cors')->name('frontend/dropdown/savepro');
Route::get('testing', 'previewController@testing')->name('testing');

//Route::post('remove-variants/{id?}', 'AdvanceDonationController@RemoveVariantsCron')->name('remove_variants_cron');

Route::post('snippet-create-product', 'AdvanceDonationController@SnippetCreateProduct')->name('snippet_create_product');
Route::post('snippet-create-cart', 'AdvanceDonationController@SnippetCreateCart')->name('snippet_create_cart');



Route::any('update_demo_status', 'DonationproController@update_demo_status')->name('update_demo_status');

Route::post('set-default-email', 'DonationproController@SetDefaultEmail')->name('set_default_email');