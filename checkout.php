<?php

$json = '' . file_get_contents('php://input') . '';
$result = json_decode($json);
$shopify_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];

$connection = new mysqli("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_easy_donation");

$select_store = "select product_id from usersettings WHERE store_name= '" . $shopify_domain . "' OR domain = '" . $shopify_domain . "'";
$usersettingsData = $connection->query($select_store);

$fetchObject = $usersettingsData->fetch_object();
$donation_product_id = $fetchObject->product_id;

foreach ($result->customer as $customerDetail) {
    if ($customerDetail->first_name) {
        $first_name = $customerDetail->first_name;
    }
    if ($customerDetail->last_name) {
        $last_name = $customerDetail->last_name;
    }
}

$email = $result->email;
$created = $result->created_at;
$explDate = explode('T', $created, -1);
$date = implode(" ", $explDate);

foreach ($result->line_items as $value) {
    if ($donation_product_id == $value->product_id) {
        $product_id = $value->product_id;
        $product_name = $value->name;
        $varient_name = explode(" - ",$product_name);
        $price = intval($value->price) * intval($value->quantity);
    }
}

$currency = $result->currency;
$id = $result->id;

if ($product_id != NULL) {
	$to = $email;
	//$to = 'dipal.zestard@gmail.com'; 
    
        
    $select_store_id = "select id from usersettings WHERE store_name= '" . $shopify_domain . "' OR domain = '" . $shopify_domain . "'";
    $storeData = $connection->query($select_store_id);

    $fetchObject = $storeData->fetch_object();
    $store_id = $fetchObject->id;

    $select_html_content = "select * from email_template WHERE shop_id= '" . $store_id . "'";
    $emailData = $connection->query($select_html_content);

    $fetchObject = $emailData->fetch_object();
    $from_email = $fetchObject->admin_email;
    $cc_email = $fetchObject->cc_email;
    $message = $fetchObject->html_content;
    $email_subject = ($fetchObject->email_subject)?$fetchObject->email_subject:'Easy Donation receipt';
    
    $subject = $email_subject;

    $message = str_replace(
            array("{{first_name}}", "{{last_name}}", "{{email}}", "{{date}}", "{{product_id}}", "{{donation_name}}", "{{price}}", "{{currency}}"), array($first_name, $last_name, $email, $date, $product_id, $varient_name[0], $price, $currency), $message
    );

    // Change the subject line for compassion-in-world-farming.myshopify.com store 
    if($shopify_domain == "compassion-in-world-farming.myshopify.com"){
        $subject = "Thank you for your donation to fight factory farming"; 
	}
	if($shopify_domain == "love-one-another-book.myshopify.com"){ 
        $subject = "Your Donation Receipt from Think With Me";
    }

	$select_donation_settings = "select * from donation_settings WHERE shop_id= '" . $store_id . "'";
    $donation_settings = $connection->query($select_donation_settings);

    $fetchObject = $donation_settings->fetch_object();
    $send_email = $fetchObject->send_email;

    // Always set content-type when sending HTML email
    $headers = "From: $from_email\r\n";
    $headers .= 'Cc:' . " $cc_email\r\n";
    $headers .= "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	if($shopify_domain != "cac-store.myshopify.com" && $shopify_domain != "columbiamo-delivery-on-demand.myshopify.com" && $shopify_domain != "epilepsy-action.myshopify.com" && $shopify_domain != "pastorrobertmorrisministries.myshopify.com" && $shopify_domain != "ciwfnederland.myshopify.com")
	{
		if($send_email == 1)
		{
			mail("$to, $cc_email", $subject, $message, $headers);
		}
	}
	
	if($shopify_domain == "cac-store.myshopify.com")
	{
		$order_id 		= $id;
		$api_key 		= "bbc0aab6b0c0986dd59d0a03e6a6c479";
		$shared_secret  = "e66204b1f5b592276efeb4ef34c06e86";
		$shop_name 		= "cac-store.myshopify.com";
		$access_token	= "95391ef72f8b6626ccba4a7314f4f19f";
		
		$url = "https://$api_key:$shared_secret@$shop_name/admin/locations.json";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token: $access_token"));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");		
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		curl_close($curl);
			
		$location_data_array = json_decode($response, true);				
		$location_id = $location_data_array['locations'][0]['id'];
		
		$url = "https://$api_key:$shared_secret@$shop_name/admin/orders/$order_id.json"; 				
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token: $access_token"));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");		
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		curl_close ($curl);
		$order_data_array = json_decode($response);		
		
		foreach($order_data_array as $order) 
		{
			foreach($order->line_items as $line_item) 
			{			
				if($donation_product_id == $line_item->product_id) 
				{					
					$line_id = $line_item->id;
				}
			}
		}				
		$fulfillment_data = [
			"fulfillment"=> [
				"location_id" => $location_id,				
				"line_items"  => [
					[
						"id"  => $line_id
					]			  
				]
			]
		];						
		$url = "https://$api_key:$shared_secret@$shop_name/admin/orders/$order_id/fulfillments.json";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token:$access_token"));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($fulfillment_data));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		curl_close ($curl);
	}
	if($shopify_domain == "pandere-shoes.myshopify.com")
	{
		$select_store = "select access_token,product_id from usersettings WHERE store_name= '" . $shopify_domain . "' OR domain = '" . $shopify_domain . "'";
		$usersettingsData = $connection->query($select_store);
		$fetchObject 	= $usersettingsData->fetch_object();		
		$access_token 			= $fetchObject->access_token;		
		$donation_product_id 	= $fetchObject->product_id;

		$select_settings 		= "select * from appsettings WHERE id=1";
		$app_settings_data 	= $connection->query($select_settings);
		$fetchObject 		= $app_settings_data->fetch_object();
		$api_key 			= $fetchObject->api_key;
		$shared_secret  	= $fetchObject->shared_secret;
		$shop_name 			= "pandere-shoes.myshopify.com";

		$url = "https://$api_key:$shared_secret@$shop_name/admin/products/$donation_product_id.json";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token: $access_token"));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");		
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		curl_close($curl);
			
		$product_variant = json_decode($response, true);	
		$product_variant_id = $product_variant['product']['variants'][0]['id'];

		//$product_variant_id = "19037745020982";
		$variant_argument = [
			'variant' => [
				'price' => 1
			]
		];
		$url = "https://$api_key:$shared_secret@$shop_name/admin/variants/$product_variant_id.json";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token:$access_token"));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($variant_argument));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		curl_close ($curl);
	}
}
?>