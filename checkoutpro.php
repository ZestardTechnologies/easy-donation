<?php

$json = '' . file_get_contents('php://input') . '';
$result = json_decode($json);
$shopify_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];

$connection = new mysqli("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_easy_donation");
$store_id = "select id from usersettings WHERE store_name= '" . $shopify_domain . "' OR domain = '" . $shopify_domain . "'";
$shop_id = $connection->query($store_id);
$shopId = $shop_id->fetch_object();

$donation = "select donation_data from donation_settings WHERE shop_id= '" . $shopId->id . "'";

$donationGroup = $connection->query($donation);
$donationData = $donationGroup->fetch_object();

$unserData = unserialize(base64_decode($donationData->donation_data));

$productIds = array();
foreach ($unserData as $key => $value) {
    foreach ($value as $k => $v) {
        if ($k == 'product_id') {
            $productIds[] = $v;
        }
    }
}

$donation_implodeData = implode(",", $productIds);

// $myfile = fopen("newfile.txt", "w+");
// fwrite($myfile,'2=='.$donation_implodeData.'\n');
// mail("sejal.zestard@gmail.com","data2",$donation_implodeData);
// fclose($myfile);


foreach ($result->customer as $customerDetail) {
    if ($customerDetail->first_name) {
        $first_name = $customerDetail->first_name;
    }
    if ($customerDetail->last_name) {
        $last_name = $customerDetail->last_name;
    }
}

$email = $result->email;
$created = $result->created_at;
$explDate = explode('T', $created, -1);
$date = implode(" ", $explDate);

$product_name = array();
$price = array();
foreach ($result->line_items as $value) {
    if (in_array($value->product_id, $productIds)) {
        $product_id = $value->product_id;
        $product_name[] = $value->name;
        $price[] = intval($value->price) * intval($value->quantity);
    }
}
$count = count($product_name);
$product_name_group = implode(", ", $product_name);
$price_group = implode(", ", $price);
$total_amount = array_sum($price);
$total_price = number_format($total_amount, 2, '.', '');
$currency = $result->currency;
$id = $result->id;

if ($product_id != NULL) {
    $to = $email;   

    $select_store_id = "select id from usersettings WHERE store_name= '" . $shopify_domain . "' OR domain = '" . $shopify_domain . "'";
    $storeData = $connection->query($select_store_id);

    $fetchObject = $storeData->fetch_object();
    $store_id = $fetchObject->id;

    $select_html_content = "select * from email_template WHERE shop_id= '" . $store_id . "'";
    $emailData = $connection->query($select_html_content);

    $fetchObject = $emailData->fetch_object();
    $from_email = $fetchObject->admin_email;
    $cc_email = $fetchObject->cc_email;
    $message = $fetchObject->html_content;
    $email_subject = ($fetchObject->email_subject)?$fetchObject->email_subject:'Easy Donation receipt';
    
    $subject = $email_subject;

    $donation_details = '<br><table border="0" cellspacing="0" cellpadding="0" class="donation_type" style="margin-top: 40px;border: 1px solid #efefef;padding-bottom: 10px;margin-bottom: 30px;">
    <tbody><tr><th style="border-right: 2px solid #d8d7d7;font-size: 18px;color: #373634;background-color: #efefef;padding: 10px 20px;">Donation Type</th><th style="font-size: 18px;color: #373634;background-color: #efefef;padding: 10px 20px;">Donated Amount</th></tr>';

    for ($i = 0; $i < $count; $i++) {
        $varient_name = explode('- Donate', $product_name[$i]);
        $donation_details .= '<tr><td style="padding: 7px 20px;font-weight: 600; color: #484746;">' . $varient_name[0] . '</td><td style="padding: 7px 20px;font-weight: 600; color: #484746;">' . $currency . ' ' . $price[$i] . '</td></tr>';
    }

    $donation_details .= '<tr><td style="padding: 7px 20px;color: #cb372b;font-weight: 700;">Total Amount</td><td style="padding: 7px 20px;color: #cb372b;font-weight: 700;">' . $currency . ' ' . $total_price . '</td></tr></tbody></table>';

    $message = str_replace(
            array("{{first_name}}", "{{last_name}}", "{{email}}", "{{date}}", "{{product_id}}", "{{donation_name}}", "{{price}}", "{{currency}}", "{{total_price}}", "{{donation_details}}"), array($first_name, $last_name, $email, $date, $product_id, $product_name_group, $price_group, $currency, $total_price, $donation_details), $message
    );

    $select_donation_settings = "select * from donation_settings WHERE shop_id= '" . $store_id . "'";
    $donation_settings = $connection->query($select_donation_settings);

    $fetchObject = $donation_settings->fetch_object();
    $send_email = $fetchObject->send_email;

    // Always set content-type when sending HTML email
    $headers = "From: $from_email\r\n";
    $headers .= 'Cc:' . " $cc_email\r\n";
    $headers .= "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	if($shopify_domain != "epilepsy-action.myshopify.com" && $shopify_domain != "pastorrobertmorrisministries.myshopify.com" && $shopify_domain != "amika-1.myshopify.com")
    {
        if($send_email == 1)
		{
            mail($to, $subject, $message, $headers);
        }
	}
}
?>
