<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DonationSettings extends Model {

    use SoftDeletes;
    protected $table = 'donation_settings';
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $fillable = [
        'status',
        'select_page',
        'title',
        'donation_name',
        'description',
        'field_option',
        'dropdown_option',
        'dropdown_other',
        'text_dropdown_other',
        'bar_min',
        'bar_max',
        'add_min_amount',
        'variant_ids',
        'donation_data',
        'donation_button_text',
        'additional_css',
        'shop_currency',
        'shop_id',
        'send_email',
        'show_popup',
        'popup_message',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

}
