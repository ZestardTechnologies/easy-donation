<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ShopModel;
use App\AppSettingModel;
use App\DonationSettings;
use App\DevelopmentStores;
use DB;
use Illuminate\Support\Facades\Log;

class RemoveVariantsCronJobs extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'removeVariantsCronJobs:variants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        /*
         * For Testing Purpuse cron
         */
        //DonationSettings::where('id',68)->update(['donation_name'=> 'Test']);

        /*
         * Remove Variants 
         */

        $DevelopmentStores = DevelopmentStores::get();
        foreach ($DevelopmentStores as $key => $shop) {
            $shop = $shop->dev_store_name;
            //$shop = session('shop');
            if ($shop == 'order-additional-fields.myshopify.com') {
                $shop_find = ShopModel::where('store_name', $shop)->first();
                $app_settings = AppSettingModel::where('id', 1)->first();
                $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
                $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
                $donation_data = unserialize(base64_decode($donation_config->donation_data));
                foreach ($donation_data as $key => $data) {
                    $products = $sh->call(['URL' => '/admin/products/' . $data['product_id'] . '.json', 'METHOD' => 'GET']);
                    foreach ($products->product->variants as $variants) {
                        $variant = $sh->call(['URL' => "/admin/products/" . $data['product_id'] . "/variants/" . $variants->id . ".json", 'METHOD' => 'DELETE']);
                    }
                }
            }
        }
    }

}
