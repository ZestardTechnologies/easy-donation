<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevelopmentStores extends Model
{
    protected $table = 'development_stores';
    protected $primaryKey = 'id';
    protected $fillable =[
        'dev_store_name'
    ];
}
