<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\AppSettingModel;
use App\ShopModel;
use App\DonationSettings;

trait TrackDonation {

    public function TrackDonationGraph($request) {
        $trackNumber = $request->track;
        $product_id = $request->donation_title;
        $app_settings = AppSettingModel::where('id', 1)->first();
        $shop_name = session('shop');
        if (!isset($shop_name)) {
            $shop_name = $_REQUEST['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $app_version = $shop_find->app_version;
        $shop_id = $shop_find->product_id;
        $donation = new DonationSettings;
        $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
        $donation_data = array();
        $donation_ids = array();
        if ($donation_config) {
            if (!empty($donation_config->donation_data)) {
                $donationData = unserialize(base64_decode($donation_config->donation_data));
                foreach ($donationData as $donation) {
                    $donation_data[$donation->product_id] = $donation->donation_name;
                    array_push($donation_ids, $donation->product_id);
                }
            } else {
                $donationData = $donation_config;
                $donation_data[$shop_id] = $donationData->donation_name;
            }
            //echo "<pre>";print_r($donation_data); die;
            $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $shop_find->access_token]);

            $orderCollections = $sh->call(['URL' => '/admin/orders.json', 'METHOD' => 'GET']);
            $orderCollections = array();
            $count = $sh->call(['URL' => '/admin/orders/count.json?status=any', 'METHOD' => 'GET']);
            $products_count = (array) $count;
            $product_count = $products_count['count'];
            //$pages = ceil($product_count / 250);
            $pages = 1;
            $limit = 250;
            for ($i = 1; $i <= $pages; $i++) {
                $orders = (array) $sh->call(['URL' => '/admin/orders.json?status=any&limit=' . $limit . '&page=' . $i, 'METHOD' => 'GET']);
                array_push($orderCollections, $orders);                
            }
            $orderCollections = $sh->call(['URL' => '/admin/orders.json?status=any&limit=' . $limit, 'METHOD' => 'GET']);
            if ($orderCollections != NULL) {
                $total_collection_donation = 0;
                $current_month_donation = 0;
                $currency = 0;
                $total_price = 0;
                foreach ($orderCollections->orders as $orderData) {
                    $order_date = date('Y-m-d', strtotime($orderData->created_at));
                    $current_month = date('Y-m-d');
                    $created_at = date('m', strtotime($orderData->created_at));
                    $current_month = date('m');
                    $currency = $orderData->currency;
                    //Total Donations Collection
                    foreach ($orderData->line_items as $productData) {
                        $donate_id = $productData->product_id;
                        $total_price = $total_price + $orderData->total_price;
                        if ($productData->vendor == 'zestard-easy-donation') {
                            $total_collection_donation = $total_collection_donation + ($productData->price * $productData->quantity);
                        }
                    }
                    //Total Donation This Month
                    if ($created_at == $current_month) {
                        foreach ($orderData->line_items as $productData) {
                            $donate_id = $productData->product_id;
                            $total_price = $total_price + $orderData->total_price;
                            if ($productData->vendor == 'zestard-easy-donation') {
                                $current_month_donation = $current_month_donation + ($productData->price * $productData->quantity);
                            }
                        }
                    }
                }
            }          

            $returnArray = array('status' => 1 ,'orderCollections' => $orderCollections, 'trackNumber' => $trackNumber, 'product_id' => $product_id, 'donation_data' => $donation_data, 'shop' => $shop_name, 'donation_ids' => $donation_ids, 'currency' => $currency, 'total_collection_donation' => $total_collection_donation, 'current_month_donation' => $current_month_donation);
            return json_encode($returnArray);
        } else {
            $returnArray = array('status' => 0 ,'orderCollection' => array(), 'trackNumber' => '', 'donate_product_id' => '', 'donation_data' => array(), 'shop' => $shop_name, 'currency' => '', 'total_collection_donation' => '0', 'current_month_donation' => '0');
            return json_encode($returnArray);
        }
    }

}
