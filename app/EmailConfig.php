<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//email_config
class EmailConfig extends Model
{
    protected $table = 'email_template';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable =[
        'html_content',
        'shop_id',
        'admin_email',
        'cc_email',
        'email_subject'
    ];
}
