<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopModel extends Model
{
    protected $table = 'usersettings';
    protected $primaryKey = 'id';
    protected $fillable =[
        'app_version',
        'access_token',
        'store_name',
        'store_encrypt',
        'created_at',
        'updated_at',
        'charge_id',
        'api_client_id',
        'price',
        'status',
        'billing_on',
        'payment_created_at',
        'activated_on',
        'trial_ends_on',
        'cancelled_on',
        'trial_days',
        'decorated_return_url',
        'confirmation_url',
        'domain',
        'product_id',
        'type_of_app',
        'usage_price',
        'usage_charge_id',
        'hide_product_flag',
        'charge_flag',
        'new_install',        
        
    ];
}
