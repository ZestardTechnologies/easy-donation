<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class field_config extends Model
{
    protected $table = 'field_config';
    public $timestamps = false;
    protected $fillable =[
    'fields'
  ];
}
