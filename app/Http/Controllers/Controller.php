<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use Session;
use DB;
use App\AppSettingModel;
use App\ShopModel;
use App\EmailConfig;
use App\DonationSettings;
use App\TrialInfo;
use App\DevelopmentStores;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $shop_name = session('shop');
            $shop_find = ShopModel::where('store_name', $shop_name)->first();
            if ($shop_find) {
                View::share('shop_find', $shop_find);
                $donation_settings = DonationSettings::where('shop_id', $shop_find->id)->first();
                if ($shop_find->app_version == 1 && $shop_find->type_of_app == 1) {
                    // for basic verison
                    $add_donation_route = 'basic_donation_create_edit';
                    if (count($donation_settings)) {
                        $donation_data = unserialize(base64_decode($donation_settings->donation_data));
                        if (!empty($donation_data)) {
                            $product_id = $donation_data[0]->{'product_id'};
                        } else {
                            $product_id = $shop_find->product_id;
                        }
                        
                    } else {
                        $product_id = '';
                    }
                } else {
                    // for advance verison
                    if (count($donation_settings)) {
                        $add_donation_route = 'datatable_listing_pro';
                    } else {
                        $add_donation_route = 'advance_donation_create_edit';
                    }
                    $product_id = '';
                }

                View::share('add_donation_route', $add_donation_route);
                View::share('product_id', $product_id);
            } else {
                View::share('shop_find', '');
                View::share('add_donation_route', '');
                View::share('product_id', '');
            }
            return $next($request);
        });
    }

}
