<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\ShopModel;
use DB;
use App\DonationSettings;
use App\Symbol;

class previewController extends Controller {

    public function testing() 
	{
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shopData = DB::table('usersettings')->select('id')->where('store_encrypt', $id)->first();
        $shop_id = $shopData->id;
        $shop_find = ShopModel::where('id', $shop_id)->first();
        $shop = $shop_find->store_name;
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $sh->call(['URL' => '/admin/products/.json', 'METHOD' => 'GET']);
    }

    //For returning front preview based on app version
    public function index(Request $request) 
	{		
        $id = $request['id'];
        $page = $request['page'];
        $productid = $request['productid'];
        $shopData = DB::table('usersettings')->select('id', 'app_version','store_name', 'access_token', 'product_id')->where('store_encrypt', $id)->first();
        $shop = (array) $shopData;		
        $shop_id = $shop['id'];
		$donation_config = DonationSettings::where('shop_id', $shop_id)->first();
		/* dd($donation_config); */
        $statusdata = DB::table('donation_settings')->select('status')->where('shop_id', $shop_id)->first();
		$status = (array) $statusdata;
		
		if($shop['store_name'] == "pandere-shoes.myshopify.com")
		{
			// $app_settings = DB::table('appsettings')->where('id', 1)->first();
			// $api_key = $app_settings->api_key;
			// $shared_secret = $app_settings->shared_secret;
			// $product_variant_id = "19037745020982";
			// $variant_argument = [
			// 	'variant' => [
			// 		'price' => 1
			// 	]
			// ];
			// $url = "https://$api_key:$shared_secret@pandere-shoes.myshopify.com/admin/variants/$product_variant_id.json";
			// $curl = curl_init();
			// curl_setopt($curl, CURLOPT_URL, $url);
			// curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token:$access_token"));
			// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			// curl_setopt($curl, CURLOPT_VERBOSE, 0);
			// curl_setopt($curl, CURLOPT_HEADER, 0);
			// curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			// curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($variant_argument));
			// curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			// $response = curl_exec($curl);
			// curl_close ($curl);
			// 			
			// $shop_details = DB::table('usersettings')->select('id')->where('store_encrypt', $id)->first();
			// $shop_id = $shop_details->id;
			// $shop_find = ShopModel::where('id', $shop_id)->first();
			// $shop_name = $shop_find->store_name;
			// $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $shop_find->access_token]);
			// $amount = $request['data'];						
			// $product_variant = $sh->call(['URL' => '/admin/products/' . $shop_find->product_id . '/variants.json', 'METHOD' => 'GET']);
			// $product_variant_id = $product_variant->variants[0]->id;									
			// $variant_argument = [
			// 	'variant' => [
			// 		'price' => 1
			// 	]
			// ];
			// $variant = $sh->call(['URL' => '/admin/variants/'. $product_variant_id .'.json', 'METHOD' => 'PUT', 'DATA' => $variant_argument]);
			// dd($variant);
		}
		/* dd($statusdata);		 */
        //If settings exists for particular store then return view
        if (count($status) > 0) 
		{
            $appstatus = $status['status'];
            if ($appstatus == 1) 
			{
				/* if($shop['store_name'] == "zestardgiftshop.myshopify.com" || $shop['store_name'] == "free-theme-test.myshopify.com") */
				{
					$app_settings = DB::table('appsettings')->where('id', 1)->first();                
					$sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shopData->store_name, 'ACCESS_TOKEN' => $shopData->access_token ]);					
					$shop_api = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
					$currency = Symbol::where('currency_code', $shop_api->shop->currency)->first();					
					$shop_id = $shopData->id;
											
					$donation_config = DonationSettings::where('shop_id', $shop_id)->first();					
					$donation_config->shop_currency = $currency->symbol_html;										
					$donation_config->save();										
					if($shop['app_version'] == 2)
					{
						$donation_settings_array = array();
						$donation_settings_array['status'] = $donation_config->status;					
						$donation_settings_array['title'] = $donation_config->title;
						$donation_settings_array['donation_button_text'] = $donation_config->donation_button_text;
						$donation_settings_array['additional_css'] = $donation_config->additional_css;
						$donation_settings_array['variant_ids'] = $donation_config->variant_ids;
						$donation_settings_array['donation_data'] = unserialize(base64_decode($donation_config->donation_data));
						$donation_settings_array['shop_currency'] = ($donation_config->shop_currency);
						$donation_settings_array['show_popup'] = $donation_config->show_popup;
						$donation_settings_array['popup_message'] = $donation_config->popup_message;
						$donation_data_length = count($donation_settings_array['donation_data']);
						$temp_donation_data = ($donation_settings_array['donation_data']);
						$select_page = array();		
						if($shop['store_name'] == "www-therapythreads-com.myshopify.com" || $shop['store_name'] == "liveinloose.myshopify.com")
						{
							/* dd($donation_config); */
						}						
						foreach($temp_donation_data as $donation_data)
						{
							if(isset($donation_config->select_page) || $donation_config->select_page != null)
								array_push($select_page, $donation_config->select_page);
							else
								array_push($select_page, 1);
						}						
						$donationData[] = $donation_settings_array['donation_data'];
						$product_image_url = [];
						$product_ids = [];
						$i = 0;
						foreach ($donationData as $donationCollection) 
						{						
							foreach ($donationCollection as $donation) 
							{
								$donation = (object)$donation;
								array_push($product_ids, $donation->product_id);
							}
						}

						//$url = "/admin/products/" . $donation_productId . "/images/" . $donation_productId . ".json";
						$products = $sh->call(['URL' => '/admin/products.json?ids=' . implode(",", $product_ids), 'METHOD' => 'GET']);
						foreach ($products as $product) {
							foreach ($product as $attributes) {
								//echo '<pre>';print_r($attributes);die;
								$product_verify_id = $attributes->variants[0]->product_id;
								if (empty($attributes->images)) {
									//echo '<pre>';print_r("empty");die;
									$product_image_url[$product_verify_id] = "";
								} else {
									$img_src = isset($attributes->images[0]->src) ? $attributes->images[0]->src : "";
									$product_image_url[$product_verify_id] = $img_src;
									
								}
							}
						}	
					}					
					else if($shop['app_version'] == 1)
					{                                                                                              
                                                //$product_id_array = unserialize(base64_decode($donation_config->donation_data));
                                                //$product_id =  $product_id_array[0]['product_id'];                                                
                                                //$product_image = $sh->call(['URL' => '/admin/products/' . $product_id . '.json', 'METHOD' => 'GET']);
						$product_image = $sh->call(['URL' => '/admin/products/' . $shopData->product_id . '.json', 'METHOD' => 'GET']);
						if(isset($product_image->product->images[0]->src))			
						{
							$product_image_url = $product_image->product->images[0]->src;										
							$images_json = ($product_image_url);						
						}
						else
						{
							$images_json = "";
						}	
						$donation_settings = json_encode($donation_config);		
					}
					/* 
						$url = 'https://' . $shopData->store_name . '/admin/webhooks.json';
						$webhookData = [
							'webhook' => [
								'topic' =>   'products/update',
								'address' => 'https://shopifydev.anujdalal.com/dev_easy_donation/public/cache-clear',
								'format' =>  'json'
							]
						];
						dd($uninstall = $sh->appUninstallHook($shopData->access_token, $url, $webhookData));							
						dd($sh->call(['METHOD' =>'GET','URL' => '/admin/webhooks.json']));												
						if(Cache::has($id)) 
						{														
							return Cache::get($id);	
						}
						else
						{
							$cache_data = (string) view('testing_pro', ['id' => $id, 'page' => $page, 'productid' => $productid, 'images_json' => json_encode($product_image_url), 'donation_settings' => json_encode($donation_settings_array)]);						
							Cache::put($id, $cache_data, 24*60);						
							return Cache::get($id);
						}		 
					*/
					
					if($shop['app_version'] == 1 && $page == "cart") 
					{						
						return view('frontpreview', compact('id', 'page', 'images_json','donation_settings'));
					}
					if($shop['app_version'] == 1 && $page == "product") 
					{						
						return view('frontpreview', compact('id', 'page', 'images_json','donation_settings'));
					}
					if($shop['app_version'] == 2 && $page == "cart") 
					{																												
						return view('frontpreviewpro', ['id' => $id, 'page' => $page, 'select_page' => json_encode($select_page), 'productid' => $productid, 'images_json' => json_encode($product_image_url), 'donation_settings' => json_encode($donation_settings_array), 'temp_product_ids' => $product_ids ]);
					}
					if($shop['app_version'] == 2 && $page == "product") 
					{													
						return view('frontpreviewpro', ['id' => $id, 'page' => $page, 'select_page' => json_encode($select_page), 'productid' => $productid, 'images_json' => json_encode($product_image_url), 'donation_settings' => json_encode($donation_settings_array), 'temp_product_ids' => $product_ids]);
					}
					if ($shop['app_version'] == 1 && $page == "checkout") {						
						return view('frontpreview_sweetlegs', ['id' => $id, 'page' => $page]);
					}
				}
				/* else
				{
					if ($shop['app_version'] == 1 && $page == "checkout") {						
						return view('frontpreview_sweetlegs', ['id' => $id, 'page' => $page]);
					}
					if ($shop['app_version'] == 1 && $page == "cart") {
						return view('frontpreview', ['id' => $id, 'page' => $page]);
					}
					if ($shop['app_version'] == 1 && $page == "product") {
						return view('frontpreview', ['id' => $id, 'page' => $page]);
					}
					if ($shop['app_version'] == 2 && $page == "cart"){
						$donation_settings_array = array();
						$donation_settings_array['status'] = $donation_config->status;					
						$donation_settings_array['title'] = $donation_config->title;
						$donation_settings_array['donation_button_text'] = $donation_config->donation_button_text;
						$donation_settings_array['additional_css'] = $donation_config->additional_css;
						$donation_settings_array['variant_ids'] = $donation_config->variant_ids;
						$donation_settings_array['donation_data'] = unserialize(base64_decode($donation_config->donation_data));
						$donation_settings_array['shop_currency'] = ($donation_config->shop_currency);
						$donation_data_length = count($donation_settings_array['donation_data']);
						$donation_settings = json_encode($donation_config);		
						$product_ids = [];						
						$temp_donation_data = unserialize(base64_decode($donation_config->donation_data));	
						foreach($temp_donation_data as $donation) 
						{							
								array_push($product_ids, $donation->product_id);						
						}
						return view('frontpreviewpro', ['id' => $id, 'page' => $page, 'productid' => $productid, 'temp_product_ids' => $product_ids, 'donation_settings' => json_encode($donation_settings_array)]);
					}
					if ($shop['app_version'] == 2 && $page == "product"){
						$donation_settings_array = array();
						$donation_settings_array['status'] = $donation_config->status;					
						$donation_settings_array['title'] = $donation_config->title;
						$donation_settings_array['donation_button_text'] = $donation_config->donation_button_text;
						$donation_settings_array['additional_css'] = $donation_config->additional_css;
						$donation_settings_array['variant_ids'] = $donation_config->variant_ids;
						$donation_settings_array['donation_data'] = unserialize(base64_decode($donation_config->donation_data));
						$donation_settings_array['shop_currency'] = ($donation_config->shop_currency);
						$donation_data_length = count($donation_settings_array['donation_data']);
						$donation_settings = json_encode($donation_config);		
						$product_ids = [];						
						$temp_donation_data = unserialize(base64_decode($donation_config->donation_data));							
						foreach($temp_donation_data as $donationCollection) 
						{							
							//foreach ($donationCollection as $donation) 							
							{								
								array_push($product_ids, $donationCollection->product_id);
							}
						}
						return view('frontpreviewpro', ['id' => $id, 'page' => $page, 'productid' => $productid, 'temp_product_ids' => $product_ids, 'donation_settings' => json_encode($donation_settings_array)]);
					}
				} */
            }
        } else {
            //Notify
        }
    }

    //For updating product price and returning variant ID(Basic Version)
    public function donate(Request $request){
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $id = $request['id'];
        $shopData = DB::table('usersettings')->select('id')->where('store_encrypt', $id)->first();
        $shop_id = $shopData->id;
        $shop_find = ShopModel::where('id', $shop_id)->first();
        $shop = $shop_find->store_name;
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

        $amount = $request['data'];
                		
		$product_variant = $sh->call(['URL' => '/admin/products/' . $shop_find->product_id . '/variants.json', 'METHOD' => 'GET']);
		$product_variant_id = $product_variant->variants[0]->id;			
					
        $variant_argument = [
            'variant' => [
                'price' => $amount
            ]
        ];
        $variant = $sh->call(['URL' => '/admin/variants/'. $product_variant_id .'.json', 'METHOD' => 'PUT', 'DATA' => $variant_argument]);

        return $product_variant_id;         
    }
        
    //For updating product price and returning variant ID(Pro Version)
    public function donatepro(Request $request) {
		
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $id = $request['id'];
        $shopData = DB::table('usersettings')->select('id')->where('store_encrypt', $id)->first();
        $shop_id = $shopData->id;
        $donation = DonationSettings::where('shop_id', $shop_id)->first();
        $donationArray = unserialize(base64_decode($donation->donation_data));
        $product = $request['product_id'];
        $shop_find = ShopModel::where('id', $shop_id)->first();
        $shop = $shop_find->store_name;
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

        $amount = $request['price'];
        
        $product_variant = $sh->call(['URL' => '/admin/products/' . $product . '/variants.json', 'METHOD' => 'GET']);
        $product_variant_id = $product_variant->variants[0]->id;
        
        $variant_argument = [
            'variant' => [
                'price' => $amount
            ]
        ];
        $variant = $sh->call(['URL' => '/admin/variants/'. $product_variant_id .'.json', 'METHOD' => 'PUT', 'DATA' => $variant_argument]);

        return $product_variant_id; 
    }

}
