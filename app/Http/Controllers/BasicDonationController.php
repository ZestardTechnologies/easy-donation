<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopModel;
use App\AppSettingModel;
use App\DonationSettings;
use App\EmailConfig;
use App\Symbol;
use App;
use DB;
use Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;

class BasicDonationController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function createEdit(Request $request,$product_id = '') {
        
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $request->input('shop');
        }
        
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $product_id = $shop_find->product_id;                
        $app_settings = AppSettingModel::where('id', 1)->first();        
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();  
        
        if ($donation_config != '' && $product_id != '') {
            $donationData = [];
            $selected_product = array();
            if (!empty($donation_config->donation_data)) {                
                $donationData['donation_data'] = unserialize(base64_decode($donation_config->donation_data));                
                
                $selected_product = $donationData['donation_data'][0];                
                $selected_product = (object) $selected_product;                
            } else {                
                $donationData['donation_data'] = $donation_config;
                $selected_product = $donationData['donation_data'];
                $selected_product['drop_down'] = $selected_product['dropdown_option'];
                $selected_product['donation_min'] = $selected_product['bar_min'];
                $selected_product['donation_max'] = $selected_product['bar_max'];
                unset($selected_product['dropdown_option']);
                $selected_product = (object) $selected_product;
            }
            //echo '<pre>';print_r(($selected_product));exit;
            $product = $sh->call(['URL' => '/admin/products/' . $product_id . '.json', 'METHOD' => 'GET']);
            return view('basic.edit_product', compact(['shop', 'product', 'donation_config', 'selected_product']));
        } else {
            $shop_name = session('shop');
            return view('basic.add_product', ['shop' => $shop]);
        }
    }

    public function save(Request $request) {
        
        $rules = [
            'upload_donation_image' => 'max:2048|mimes:jpeg,png,jpg',
        ];
        $message = [
            'upload_donation_image.max' => 'The upload donation image may not be greater than 2 MB'
        ];
        $this->validate($request, $rules, $message);
        $shop_name = session('shop');
        if (!isset($shop)) {
            $shop_name = $request->input('shop');
        }
        
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $app_settings = AppSettingModel::where('id', 1)->first();
        $select_store = ShopModel::where('store_name', $shop_name)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store->access_token]);
        //$donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();

        if ($request->product_id) {
            //echo 'update';exit;
            $shop_name = session('shop');
            if (!isset($shop_name)) {
                $shop_name = $input['shop'];
            }
            $sh = app('ShopifyAPI');
            $app_settings = AppSettingModel::where('id', 1)->first();
            $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store->access_token]);
            $product = $sh->call(['URL' => '/admin/products/' . $request->product_id . '.json', 'METHOD' => 'GET']);
            $shop_find = ShopModel::where('store_name', $shop_name)->first();
            $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
            $donationArray = unserialize(base64_decode($donation_config->donation_data));

            if (!empty($donation_config->donation_data)) {
                $donationArray = unserialize(base64_decode($donation_config->donation_data));
            } else {
                $donationArray = $donation_config;
            }
            $product_variant_array = array();
            $temp_product_variant_array = array();
            $productArr = array();
            //Checking if Variant ID's Exists
            if ($donation_config->variant_ids != NULL) {
                $temp_product_variant_array = json_decode($donation_config->variant_ids, true);
            }
            foreach ($donationArray as $key => $donation) {
                //if ($request['product_id'] == $donation['product_id']) {                    
                if ($request->upload_donation_image != '') {
                    $images = time() . '_' . $request->upload_donation_image->getClientOriginalName();
                    $request->upload_donation_image->move(('image/product/'), $images);
                    $img = config('app.url') . 'public/image/product/' . $images;
                } else if ($request->upload_donation_image == '' && $product->product->image != '') {
                    $image = $product->product->image->src;
                    $img = $image;
                } else {
                    $img = config('app.url') . 'public/image/Donate.png';
                }
                $productArr = $donation;                
                $field_option = $request['field_option'];
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => $img
                            )
                        ),
                        'variants' => array(
                            '0' => array(
                                'option1' => 'Donate',
                                'price' => '00.00',
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                $product = $sh->call(['URL' => '/admin/products/' . $request['product_id'] . '.json', 'METHOD' => 'PUT', 'DATA' => $product_argument]);
                $productArr = array();
                $productFinalArr = array();
                $productArr['donation_name'] = $request->donation_name;
                $productArr['donation_description'] = $request->donation_Description;
                $productArr['select_page'] = $request->select_page;
                $productArr['field_option'] = $request->ui_select;
                $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
                if ($request->dropdown_other == "checked") {
                    $productArr['dropdown_other'] = 1;
                } else {
                    $productArr['dropdown_other'] = 0;
                }
                if ($request->ui_select == 'P') {
                    $productArr['donation_min'] = $request->donation_min;
                    $productArr['donation_max'] = $request->donation_max;
                }
                if ($request->ui_select == 'T') {
                    $productArr['add_min_amount'] = $request->add_min_amount;
                }
                if ($request->ui_select == 'D') {
                    $productArr['text_dropdown_other'] = ($request->text_dropdown_other) ? $request->text_dropdown_other : 'Other';
                }
                $productArr['product_id'] = $product->product->id;
                $donation_productId = $request['product_id'];                
                $donationName = $request['donation_name'];
                $donationDescription = $request['donation_description'];
                $productFinalArr[0] = (object) $productArr;
                
                // convert into object to match with old version
                $productFinalArr = $productFinalArr;
                //echo '<pre>';print_r($productFinalArr);exit;
                $updtaedSertext = base64_encode(serialize($productFinalArr));
                if ($request->ui_select == 'T') {
                    $update_data = [
                        'status' => '1',
                        'select_page' => $request->select_page,
                        'donation_name' => $request->donation_name,
                        'description' => $request->donation_Description,
                        'field_option' => $request->ui_select,
                        'dropdown_option' => '',
                        'dropdown_other' => 0,
                        'text_dropdown_other' => '',
                        'bar_min' => 0,
                        'bar_max' => 0,
                        'add_min_amount' => ($request->add_min_amount) ? $request->add_min_amount : 0,
                        'donation_data' => $updtaedSertext,
                        'shop_currency' => '',
                        'shop_id' => $shop_find->id,

                    ];
                } elseif ($request->ui_select == 'P') {
                    $update_data = [
                        'status' => '1',
                        'select_page' => $request->select_page,
                        'donation_name' => $request->donation_name,
                        'description' => $request->donation_Description,
                        'field_option' => $request->ui_select,
                        'dropdown_option' => '',
                        'dropdown_other' => 0,
                        'text_dropdown_other' => '',
                        'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                        'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                        'add_min_amount' => 0,
                        'donation_data' => $updtaedSertext,
                        'shop_currency' => '',
                        'shop_id' => $shop_find->id,
                    ];
                } elseif ($request->ui_select == 'D') {
                    $i = 1;
                    //For Checking if Variants Exists
                    if (!empty($temp_product_variant_array)) {
                        //For Checking if Particular Product Variant Exists
                        if (!empty($temp_product_variant_array["$donation_productId"])) {
                            //For Deleteing Existing Variants	                                
                            $old_variants = array();
                            $variants = $sh->call(['URL' => "admin/products/$donation_productId/variants.json", 'METHOD' => 'GET']);
                            foreach ($variants as $product_variant) {
                                foreach ($product_variant as $variant) {
                                    array_push($old_variants, $variant->id);
                                }
                            }
                            $old_variants = array_slice($old_variants, 1);
                            foreach ($old_variants as $variant) {
                                $variant = $sh->call(['URL' => "/admin/products/$donation_productId/variants/$variant.json", 'METHOD' => 'DELETE']);
                            }
                        }
                    }
                    //DropDown Amounts
                    $amounts = $request->dropdown_option;
                    $variant_ids = array();
                    //For Creating Product Variants
                    foreach ($amounts as $donation_amount) {
                        $variant_argument = [
                            'variant' => [
                                'option1' => "Donation" . $i,
                                'price' => "$donation_amount",
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            ]
                        ];
                        $variant = $sh->call(['URL' => "/admin/products/$donation_productId/variants.json", 'METHOD' => 'POST', 'DATA' => $variant_argument]);
                        array_push($variant_ids, $variant->variant->id);
                        $i++;
                    }
                    //For Storing Product-Wise Variant Id's
                    $product_variant_array ["$donation_productId"] = $variant_ids;

                    $updtaedSertext = base64_encode(serialize($productFinalArr));
                    $update_data = [
                        'status' => '1',
                        'select_page' => $request->select_page,
                        'donation_name' => $request->donation_name,
                        'description' => $request->donation_Description,
                        'field_option' => $request->ui_select,
                        'dropdown_option' => ($request->dropdown_option) ? json_encode($request->dropdown_option) : '',
                        'dropdown_other' => ($request->dropdown_other) ? 1 : 0,
                        'text_dropdown_other' => ($request->text_dropdown_other) ? $request->text_dropdown_other : 'Other',
                        'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                        'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                        'add_min_amount' => 0,
                        'donation_data' => $updtaedSertext,
                        'shop_currency' => '',
                        'shop_id' => $shop_find->id,

                    ];
                }
                $update_donationData = DonationSettings::where('shop_id', $shop_find->id)->update($update_data);
                if (!empty($product_variant_array)) {
                    DonationSettings::where('shop_id', $shop_find->id)->update(['variant_ids' => json_encode($variant_ids)]);
                }
                Session::flash('success', 'Your donation has been updated successfully');
                return redirect()->route('basic_donation_create_edit', $request->product_id);
                //}
            }
        } else {
            //Add Product           
            if ($request->upload_donation_image != '') {
                $images = time() . '_' . $request->upload_donation_image->getClientOriginalName();
                $request->upload_donation_image->move(('image/product/'), $images);
                $img = config('app.url') . 'public/image/product/' . $images;
            } else {
                $img = config('app.url') . 'public/image/Donate.png';
            }

            if ($request->ui_select == 'D') {
                $i = 1;
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => $img
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                //Api call for product
                $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                $product_id = $product->product->id;
                //For deleting existing product variants if exists
                if (!empty($temp_product_variant_array)) { {
                        $old_variants = array();
                        $variants = $sh->call(['URL' => "admin/products/$product_id/variants.json", 'METHOD' => 'GET']);
                        foreach ($variants as $product_variant) {
                            foreach ($product_variant as $variant) {
                                array_push($old_variants, $variant->id);
                            }
                        }
                        $old_variants = array_slice($old_variants, 1);
                    }
                    foreach ($old_variants as $variant) {
                        $variant = $sh->call(['URL' => "/admin/products/$product_id/variants/$variant.json", 'METHOD' => 'DELETE']);
                    }
                }
                $amounts = $request->dropdown_option;
                $variant_ids = array();
                //Creating product variants
                foreach ($amounts as $donation_amount) {
                    $variant_argument = [
                        'variant' => [
                            'option1' => "Donation" . $i,
                            'price' => "$donation_amount",
                            'taxable' => 'false',
                            'requires_shipping' => 'false',
                            'inventory_policy' => 'continue'
                        ]
                    ];
                    $variant = $sh->call(['URL' => "/admin/products/$product_id/variants.json", 'METHOD' => 'POST', 'DATA' => $variant_argument]);
                    array_push($variant_ids, $variant->variant->id);
                    $i++;
                }
                $product_variant_array["$product_id"] = $variant_ids;
            } else {
                //For Textbox
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => $img
                            )
                        ),
                        'variants' => array(
                            '0' => array(
                                'option1' => 'Donate',
                                'price' => '00.00',
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                //Api call for product
                $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                $product_id = $product->product->id;
            }
            $productArr = array();
            $productFinalArr = array();
            $productArr['donation_name'] = $request->donation_name;
            $productArr['donation_description'] = $request->donation_Description;
            $productArr['select_page'] = $request->select_page;
            $productArr['field_option'] = $request->ui_select;
            $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
            if ($request->dropdown_other == "checked") {
                $productArr['dropdown_other'] = 1;
            } else {
                $productArr['dropdown_other'] = 0;
            }
            if ($request->ui_select == 'P') {
                $productArr['donation_min'] = $request->donation_min;
                $productArr['donation_max'] = $request->donation_max;
            }
            if ($request->ui_select == 'T') {
                $productArr['add_min_amount'] = $request->add_min_amount;
            }
            if ($request->ui_select == 'D') {
                $productArr['text_dropdown_other'] = ($request->text_dropdown_other) ? $request->text_dropdown_other : 'Other'; //conf
            }
            $productArr['product_id'] = $product->product->id;
            $productFinalArr[0] = (object) $productArr;
            $updtaedSertext = base64_encode(serialize($productFinalArr));
            $data = [
                'status' => '1',
                'select_page' => $request->select_page,
                //'title' => '',
                'donation_name' => $request->donation_name,
                'description' => $request->donation_Description,
                'field_option' => $request->ui_select,
                'dropdown_option' => ($request->dropdown_option) ? json_encode($request->dropdown_option) : '',
                'dropdown_other' => ($request->dropdown_other) ? 1 : 0,
                'text_dropdown_other' => ($request->text_dropdown_other) ? $request->text_dropdown_other : 'Other',
                'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                'add_min_amount' => ($request->add_min_amount) ? $request->add_min_amount : 0,
                'donation_data' => $updtaedSertext,
                //'donation_button_text' => '',
                //'additional_css' => '',
                'shop_currency' => '',
                'shop_id' => $shop_find->id,
                    //'send_email' => 0,
                    //'show_popup' => 0,
                    //'popup_message' => ''
            ];
            DonationSettings::insert($data);
            $update_donationData = ShopModel::where('id', $shop_find->id)->update(['product_id' => $product_id]);
            if (!empty($product_variant_array)) {
                DonationSettings::where('shop_id', $shop_find->id)->update(['variant_ids' => json_encode($variant_ids)]);
            }
            Session::flash('success', 'Your donation has been added successfully');
            return redirect()->route('basic_donation_create_edit', $product->product->id);
        }
    }

}
