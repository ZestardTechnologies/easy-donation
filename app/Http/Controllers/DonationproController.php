<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopModel;
use App\DonationSettings;
use App\EmailConfig;
use App\Symbol;
use App;
use DB;
use Session;
use Illuminate\Support\Facades\View;
use App\AppSettingModel;
use App\Traits\TrackDonation;

class DonationproController extends Controller {

    use TrackDonation;

    /*
     * Constructor
     */

    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request) {
        $shop_name = session('shop');
        if (!isset($shop_name)) {
            $shop_name = $request->input('shop');
        }
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $shop_id = $shop_find->id;
        $donation_config = DonationSettings::where('shop_id', $shop_id)->first();
        if (count($donation_config) > 0) {
            $donationData = [];
            $donationData['status'] = $donation_config->status;
            $donationData['title'] = $donation_config->title;
            $donationData['donation_button_text'] = $donation_config->donation_button_text;
            $donationData['additional_css'] = $donation_config->additional_css;
            $donationData['popup_message'] = $donation_config->popup_message;
            $donationData['show_popup'] = $donation_config->show_popup;
            $donationData['send_email'] = $donation_config->send_email;
            $donationData['donation_data'] = unserialize(base64_decode($donation_config->donation_data));
            return json_encode($donationData);
        }
    }

    /*
     * Get Track Donation Graph And Total Donation and Collection
     */

    public function trackDonation(Request $request) {
		
        $jsonArray = json_decode($this->TrackDonationGraph($request));
        $status = $jsonArray->status;
        if ($status == 1) {
            $orderCollections = $jsonArray->orderCollections;
            $donation_data = $jsonArray->donation_data;
            $trackNumber = $jsonArray->trackNumber;
            $product_id = $jsonArray->product_id;
            $shop_name = $jsonArray->shop;
            $donation_ids = $jsonArray->donation_ids;
            $currency = $jsonArray->currency;
            $total_collection_donation = $jsonArray->total_collection_donation;
            $current_month_donation = $jsonArray->current_month_donation;
        } else {
            $orderCollections = '';
            $donation_data = ($donation_data[$jsonArray->shop] = $jsonArray->donation_data);
            $trackNumber = '';
            $product_id = '';
            $shop_name = '';
            $donation_ids = '';
            $currency = '';
            $total_collection_donation = '0';
            $current_month_donation = '0';
        }

        return view('track_donation_pro', array('orderCollections' => $orderCollections, 'trackNumber' => $trackNumber, 'product_id' => $product_id, 'donation_data' => $donation_data, 'shop' => $shop_name, 'donation_ids' => $donation_ids, 'currency' => $currency, 'total_collection_donation' => $total_collection_donation, 'current_month_donation' => $current_month_donation));
    }

    /*
     * Email Configuring View Load
     */

    public function emailConfigurationGet(Request $request) {
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $request->input('shop');
        } 
        $shop_id = ShopModel::where('store_name', $shop)->first();
        $data = EmailConfig::where('shop_id', $shop_id->id)->first();
        $setting = DonationSettings::where('shop_id', $shop_id->id)->first();
        return view('email_config_pro', compact('data', 'shop', 'setting'));
    }

    /*
     * Add HTML Email Template And Update Email Template Also Edit From and To EmailIds
     */

    public function emailConfigProSave(Request $request) {
        $shop_name = session('shop');
        if (!isset($shop_name)) {
            $shop_name = $request->input('shop');
        }
        $Shop_id = ShopModel::where('store_name', $shop_name)->first();
        $UpdateData = [
            'status' => $request->status,
            'title' => $request->title,
            'donation_button_text' => $request->donation_button_text,
            'additional_css' => $request->additional_css,
            'send_email' => $request->send_email,
            'show_popup' => $request->show_popup,
            'popup_message' => $request->popup_message,
        ];
        $settingUpdate = DonationSettings::where('shop_id', $Shop_id->id)->update($UpdateData);        	

        $exist_data = EmailConfig::where('shop_id', $Shop_id->id)->first();
        if (count($exist_data) == 0) {
            //Add Email Template
            $data = [
                'shop_id' => $Shop_id->id,
                'admin_email' => $request->from_email,
                'cc_email' => $request->cc_email,
                'email_subject' => $request->email_subject,
                'html_content' => $request->email_content
            ];
            //$update_data = DB::table('email_template')->where('shop_id', $Shop_id->id)->insert($data);
            $update_data = EmailConfig::where('shop_id', $Shop_id->id)->insert($data);
            Session::flash('success', 'App settings has been added successfully');
        } else {
            //Update Email Template
            $data = [
                'admin_email' => $request->from_email,
                'cc_email' => $request->cc_email,
                'email_subject' => $request->email_subject,
                'html_content' => $request->email_content
            ];
            //$update_data = DB::table('email_template')->where('shop_id', $Shop_id->id)->update($data);
            $update_data = EmailConfig::where('shop_id', $Shop_id->id)->update($data);
            Session::flash('success', 'App settings has been updated successfully');
        }
        return redirect()->route('email_configurationpro', ['shop' => $shop_name]);
    }

    /*
     * Display All Products
     */

    public function dataTableListing(Request $request) {
        $shop_name = session('shop');
        if (!isset($shop_name)) {
            $shop_name = $request->input('shop');
        }
        $limit = 1;
        $donation_datas = array();
        $product = array();
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $shop_id = $shop_find->id;
        $app_settings = AppSettingModel::where('id', 1)->first();
        $user_settings = ShopModel::where('store_name', $shop_name)->first();
        $donation_config = DonationSettings::where('shop_id', $shop_id)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $user_settings->access_token]);

        if (count($donation_config) > 0 && $donation_config != '') {
            $donationData = [];
            $donation_datas = unserialize(base64_decode($donation_config->donation_data));

            return view('donation_list', ['shop' => $shop_name, 'donation_datas' => $donation_datas, 'donation_config' => $donation_config, 'user_settings' => $user_settings]);
        }
        return view('donation_list', ['shop' => $shop_name, 'donation_datas' => $donation_datas, 'donation_config' => $donation_config, 'user_settings' => $user_settings]);
    }

    /*
     * Add Donation Index View
     */

    function storeIndex(Request $request) {
        $shop_name = session('shop');
        return view('add_product_pro', ['shop' => $shop_name]);
    }

    /*
     * Store Or Update Multiple Donation
     */

    function storeSave(Request $request) {
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $app_settings = AppSettingModel::where('id', 1)->first();
        $select_store = ShopModel::where('store_name', $shop_name)->first();
        //VJ
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store->access_token]);
        $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
        if (count($donation_config) > 0) {
            //Edit Product
            $saveData = DonationSettings::where('shop_id', $shop_find->id)->first();
            $donationArray = unserialize(base64_decode($saveData->donation_data));
            $donationData[] = $donationArray;
            //For Checking if Variant ID's Exists
            if ($donation_config->variant_ids != NULL) {
                $temp_product_variant_array = json_decode($donation_config->variant_ids, true);
            }
            if ($request->ui_select == 'D') {
                $i = 1;
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => config('app.url') . 'public/image/Donate.png'
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                //api call for updating product
                $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                $product_id = $product->product->id;
                //For Checking if Variants Exists
                if (!empty($temp_product_variant_array)) {
                    //For Checking if Particular Product Variant Exists
                    if (!empty($temp_product_variant_array["$product_id"])) {
                        foreach ($old_variants as $variant) {
                            $variant = $sh->call(['URL' => "/admin/products/$product_id/variants/$variant.json", 'METHOD' => 'DELETE']);
                        }
                    }
                }
                //DropDown Amounts
                $amounts = $request->dropdown_option;
                $variant_ids = array();
                //For Creating Product Variants	
                foreach ($amounts as $donation_amount) {
                    $variant_argument = [
                        'variant' => [
                            'option1' => "Donation" . $i,
                            'price' => "$donation_amount",
                            'taxable' => 'false',
                            'requires_shipping' => 'false',
                            'inventory_policy' => 'continue'
                        ]
                    ];
                    $variant = $sh->call(['URL' => "/admin/products/$product_id/variants.json", 'METHOD' => 'POST', 'DATA' => $variant_argument]);
                    array_push($variant_ids, $variant->variant->id);
                    $i++;
                }
                //For Storing Product-Wise Variant Id's
                $product_variant_array ["$product_id"] = $variant_ids;
                $productArr = array();
                $productArr['donation_name'] = $request->donation_name;
                $productArr['donation_description'] = $request->donation_Description;
                $productArr['select_page'] = $request->select_page;
                $productArr['field_option'] = $request->ui_select;
                $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
                if ($request->dropdown_other == "checked") {
                    $productArr['dropdown_other'] = 1;
                } else {
                    $productArr['dropdown_other'] = 0;
                }
                $productArr['text_dropdown_other'] = $request->text_dropdown_other;
                $productArr['add_min_amount'] = $request->add_min_amount;
                $productArr['product_id'] = $product->product->id;
                array_push($donationArray, $productArr);
                $updtaedSertext = base64_encode(serialize($donationArray));
            } else {
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => config('app.url') . 'public/image/Donate.png'
                            )
                        ),
                        'variants' => array(
                            '0' => array(
                                'option1' => 'Donate',
                                'price' => '00.00',
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                //Api call for updating product
                $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                $product_id = $product->product->id;
                $productArr = array();
                $productArr['donation_name'] = $request->donation_name;
                $productArr['donation_description'] = $request->donation_Description;
                $productArr['select_page'] = $request->select_page;
                $productArr['field_option'] = $request->ui_select;
                $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
                if ($request->ui_select == 'P') {
                    $productArr['donation_min'] = $request->donation_min;
                    $productArr['donation_max'] = $request->donation_max;
                }
                if ($request->ui_select == 'T') {
                    $productArr['dropdown_other'] = $request->dropdown_other;
                    $productArr['add_min_amount'] = $request->add_min_amount;
                }
                $productArr['product_id'] = $product->product->id;
                array_push($donationArray, $productArr);
                $updtaedSertext = base64_encode(serialize($donationArray));
            }
            //$update_donationData = DB::table('donation_settings')->where('shop_id', $shop_find->id)->update(['donation_data' => "'" . $updtaedSertext . "'"]);
            $update_donationData = DonationSettings::where('shop_id', $shop_find->id)->update(['donation_data' => "'" . $updtaedSertext . "'"]);
            Session::flash('success', 'Your product has been added successfully');
            return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
        } else {
            //Add Product
            if ($request->ui_select == 'D') {
                $i = 1;
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => config('app.url') . 'public/image/Donate.png'
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                //Api call for product
                $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                $product_id = $product->product->id;
                //For deleting existing product variants if exists
                if (!empty($temp_product_variant_array)) { {
                        $old_variants = array();
                        $variants = $sh->call(['URL' => "admin/products/$product_id/variants.json", 'METHOD' => 'GET']);
                        foreach ($variants as $product_variant) {
                            foreach ($product_variant as $variant) {
                                array_push($old_variants, $variant->id);
                            }
                        }
                        $old_variants = array_slice($old_variants, 1);
                    }
                    foreach ($old_variants as $variant) {
                        $variant = $sh->call(['URL' => "/admin/products/$product_id/variants/$variant.json", 'METHOD' => 'DELETE']);
                    }
                }
                $amounts = $request->dropdown_option;
                $variant_ids = array();
                //Creating product variants
                foreach ($amounts as $donation_amount) {
                    $variant_argument = [
                        'variant' => [
                            'option1' => "Donation" . $i,
                            'price' => "$donation_amount",
                            'taxable' => 'false',
                            'requires_shipping' => 'false',
                            'inventory_policy' => 'continue'
                        ]
                    ];
                    $variant = $sh->call(['URL' => "/admin/products/$product_id/variants.json", 'METHOD' => 'POST', 'DATA' => $variant_argument]);
                    array_push($variant_ids, $variant->variant->id);
                    $i++;
                }
                $product_variant_array["$product_id"] = $variant_ids;
            } else {
                //For Textbox     
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => config('app.url') . 'public/image/Donate.png'
                            )
                        ),
                        'variants' => array(
                            '0' => array(
                                'option1' => 'Donate',
                                'price' => '00.00',
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                //Api call for product
                $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                $product_id = $product->product->id;
            }
            $productArr = array();
            $productFinalArr = array();
            $productArr['donation_name'] = $request->donation_name;
            $productArr['donation_description'] = $request->donation_Description;
            $productArr['select_page'] = $request->select_page;
            $productArr['field_option'] = $request->ui_select;
            $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
            if ($request->dropdown_other == "checked") {
                $productArr['dropdown_other'] = 1;
            } else {
                $productArr['dropdown_other'] = 0;
            }
            if ($request->ui_select == 'P') {
                $productArr['donation_min'] = $request->donation_min;
                $productArr['donation_max'] = $request->donation_max;
            }
            if ($request->ui_select == 'T') {
                $productArr['add_min_amount'] = $request->add_min_amount;
            }
            if ($request->ui_select == 'D') {
                $productArr['text_dropdown_other'] = $request->text_dropdown_other; //conf
            }
            $productArr['product_id'] = $product->product->id;
            $productFinalArr[0] = $productArr;
            $updtaedSertext = base64_encode(serialize($productFinalArr));
            $data = [
                'status' => '1',
                'select_page' => $request->select_page,
                'title' => '',
                'donation_name' => $request->donation_name,
                'description' => $request->donation_Description,
                'field_option' => $request->ui_select,
                'dropdown_option' => ($request->dropdown_option) ? json_encode($request->dropdown_option) : '',
                'dropdown_other' => ($request->dropdown_other) ? 1 : 0,
                'text_dropdown_other' => ($request->text_dropdown_other) ? $request->text_dropdown_other : '',
                'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                'add_min_amount' => ($request->add_min_amount) ? $request->add_min_amount : 0,
                'variant_ids' => '',
                'donation_data' => $updtaedSertext,
                'donation_button_text' => '',
                'additional_css' => '',
                'shop_currency' => '',
                'shop_id' => $shop_find->id,
                'send_email' => 0,
                'show_popup' => 0,
                'popup_message' => ''
            ];
            //DB::table('donation_settings')->insert($data);
            DonationSettings::insert($data);

            Session::flash('success', 'Your product has been added successfully');
            return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
        }
    }

    /*
     * Donation Edit
     */

    function storeEdit(Request $request, $product_id) {
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $input['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettingModel::where('id', 1)->first();
        $select_store = ShopModel::where('store_name', $shop)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
        $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
        $donationData = [];
        $donationData['donation_data'] = unserialize(base64_decode($donation_config->donation_data));
        $selected_product = array();
        foreach ($donationData['donation_data'] as $key => $data) {
            if ($data['product_id'] == $product_id) {
                //array_push($selected_product,$data);
                $selected_product = $data;
            }
        }
        $product = $sh->call(['URL' => '/admin/products/' . $product_id . '.json', 'METHOD' => 'GET']);
        return view('edit_product_pro', compact(['shop', 'product', 'donation_config', 'selected_product']));
    }

    /*
     * Donation Update 
     */

    function storeUpdate(Request $request) {
        $shop_name = session('shop');
        if (!isset($shop_name)) {
            $shop_name = $input['shop'];
        }
        $sh = app('ShopifyAPI');
        $app_settings = AppSettingModel::where('id', 1)->first();
        $select_store = ShopModel::where('store_name', $shop_name)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store->access_token]);
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
        $donationArray = unserialize(base64_decode($donation_config->donation_data));
        $product_variant_array = array();
        $temp_product_variant_array = array();
        $productArr = array();
        //Checking if Variant ID's Exists
        if ($donation_config->variant_ids != NULL) {
            $temp_product_variant_array = json_decode($donation_config->variant_ids, true);
        }
        //dd($request['product_id']);
        foreach ($donationArray as $key => $donation) {
            if ($request['product_id'] == $donation['product_id']) {
                if ($request->upload_donation_image != '') {
                    $images = time() . '_' . $request->upload_donation_image->getClientOriginalName();
                    $request->upload_donation_image->move(('image/product/'), $images);
                    $img = config('app.url') . 'public/image/product/' . $images;
                } else {
                    $img = config('app.url') . 'public/image/Donate.png';
                }
                $productArr = $donation;
                $field_option = $donation['field_option'];
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => $img
                            )
                        ),
                        'variants' => array(
                            '0' => array(
                                'option1' => 'Donate',
                                'price' => '00.00',
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                $product = $sh->call(['URL' => '/admin/products/' . $request['product_id'] . '.json', 'METHOD' => 'PUT', 'DATA' => $product_argument]);
                $productArr = array();
                $productFinalArr = array();
                $productArr['donation_name'] = $request->donation_name;
                $productArr['donation_description'] = $request->donation_Description;
                $productArr['select_page'] = $request->select_page;
                $productArr['field_option'] = $request->ui_select;
                $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
                if ($request->dropdown_other == "checked") {
                    $productArr['dropdown_other'] = 1;
                } else {
                    $productArr['dropdown_other'] = 0;
                }
                if ($request->ui_select == 'P') {
                    $productArr['donation_min'] = $request->donation_min;
                    $productArr['donation_max'] = $request->donation_max;
                }
                if ($request->ui_select == 'T') {
                    $productArr['add_min_amount'] = $request->add_min_amount;
                }
                if ($request->ui_select == 'D') {
                    $productArr['text_dropdown_other'] = $request->text_dropdown_other;
                }
                $productArr['product_id'] = $product->product->id;
                $donation_productId = $donation['product_id'];
                $donationName = $donation['donation_name'];
                $donationDescription = $donation['donation_description'];
                $donationArray[$key] = $productArr;
                $updtaedSertext = base64_encode(serialize($donationArray));
                if ($request->ui_select == 'T') {
                    $update_data = [
                        'status' => '1',
                        'select_page' => $request->select_page,
                        'title' => '',
                        'donation_name' => $request->donation_name,
                        'description' => $request->donation_Description,
                        'field_option' => $request->ui_select,
                        'dropdown_option' => '',
                        'dropdown_other' => 0,
                        'text_dropdown_other' => '',
                        'bar_min' => 0,
                        'bar_max' => 0,
                        'add_min_amount' => ($request->add_min_amount) ? $request->add_min_amount : 0,
                        'variant_ids' => '',
                        'donation_data' => $updtaedSertext,
                        'donation_button_text' => '',
                        'additional_css' => '',
                        'shop_currency' => '',
                        'shop_id' => $shop_find->id,
                        'send_email' => 0,
                        'show_popup' => 0,
                        'popup_message' => ''
                    ];
                } elseif ($request->ui_select == 'P') {
                    $update_data = [
                        'status' => '1',
                        'select_page' => $request->select_page,
                        'title' => '',
                        'donation_name' => $request->donation_name,
                        'description' => $request->donation_Description,
                        'field_option' => $request->ui_select,
                        'dropdown_option' => '',
                        'dropdown_other' => 0,
                        'text_dropdown_other' => '',
                        'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                        'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                        'add_min_amount' => 0,
                        'variant_ids' => '',
                        'donation_data' => $updtaedSertext,
                        'donation_button_text' => '',
                        'additional_css' => '',
                        'shop_currency' => '',
                        'shop_id' => $shop_find->id,
                        'send_email' => 0,
                        'show_popup' => 0,
                        'popup_message' => ''
                    ];
                } elseif ($request->ui_select == 'D') {
                    $i = 1;
                    //For Checking if Variants Exists
                    if (!empty($temp_product_variant_array)) {
                        //For Checking if Particular Product Variant Exists
                        if (!empty($temp_product_variant_array["$donation_productId"])) {
                            //For Deleteing Existing Variants                            
                            foreach ($old_variants as $variant) {
                                $variant = $sh->call(['URL' => "/admin/products/$donation_productId/variants/$variant.json", 'METHOD' => 'DELETE']);
                            }
                        }
                    }
                    //DropDown Amounts
                    $amounts = $request->dropdown_option;
                    $variant_ids = array();
                    //For Creating Product Variants	
                    foreach ($amounts as $donation_amount) {
                        $variant_argument = [
                            'variant' => [
                                'option1' => "Donation" . $i,
                                'price' => "$donation_amount",
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            ]
                        ];
                        $variant = $sh->call(['URL' => "/admin/products/$donation_productId/variants.json", 'METHOD' => 'POST', 'DATA' => $variant_argument]);
                        array_push($variant_ids, $variant->variant->id);
                        $i++;
                    }
                    //For Storing Product-Wise Variant Id's
                    $product_variant_array ["$donation_productId"] = $variant_ids;
                    $donationArray[$key] = $productArr;
                    $updtaedSertext = base64_encode(serialize($donationArray));
                    $update_data = [
                        'status' => '1',
                        'select_page' => $request->select_page,
                        'title' => '',
                        'donation_name' => $request->donation_name,
                        'description' => $request->donation_Description,
                        'field_option' => $request->ui_select,
                        'dropdown_option' => ($request->dropdown_option) ? json_encode($request->dropdown_option) : '',
                        'dropdown_other' => ($request->dropdown_other) ? 1 : 0,
                        'text_dropdown_other' => ($request->text_dropdown_other) ? $request->text_dropdown_other : '',
                        'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                        'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                        'add_min_amount' => 0,
                        'variant_ids' => '',
                        'donation_data' => $updtaedSertext,
                        'donation_button_text' => '',
                        'additional_css' => '',
                        'shop_currency' => '',
                        'shop_id' => $shop_find->id,
                        'send_email' => 0,
                        'show_popup' => 0,
                        'popup_message' => ''
                    ];
                }
                //$update_donationData = DB::table('donation_settings')->where('shop_id', $shop_find->id)->update($update_data);
                $update_donationData = DonationSettings::where('shop_id', $shop_find->id)->update($update_data);
                Session::flash('success', 'Your product has been updated successfully');
                return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
            }
        }
    }

    /*
     * Delete Donation
     */

    function storeDelete(Request $request, $product_id, $dbid) {
        $shop_name = session('shop');
        $sh = app('ShopifyAPI');
        $app_settings = AppSettingModel::where('id', 1)->first();
        $select_store = ShopModel::where('store_name', $shop_name)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store->access_token]);
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $saveData = DonationSettings::where('shop_id', $shop_find->id)->first();
        $donationArray = unserialize(base64_decode($saveData->donation_data));

        if (count($donationArray) > 0) {
            foreach ($donationArray as $key => $data) {
                //echo "<pre>"; print_r($data); die;
                if ($data->product_id == $product_id) {
                    unset($donationArray[$key]);
                }
            } //die;
            $re_arrange_array = array_values($donationArray);
            $updtaedSertext = base64_encode(serialize($re_arrange_array));
            //$update_donationData = DB::table('donation_settings')->where('shop_id', $shop_find->id)->update(['donation_data' => "'" . $updtaedSertext . "'"]);
            $update_donationData = DonationSettings::where('shop_id', $shop_find->id)->update(['donation_data' => "'" . $updtaedSertext . "'"]);
            //$donation_config_delete = DonationSettings::where(['id' => $dbid, 'shop_id' => $shop_find->id])->delete();
            $product = $sh->call(['URL' => '/admin/products/' . $product_id . '.json', 'METHOD' => 'DELETE']);
            Session::flash('success', 'Your product has been deleted successfully');
            return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
        } else {
            return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
        }
    }

    /*
     * Delete Single Donation For BASIC Version
     */

    function basicStoreDelete(Request $request, $product_id, $dbid) {
        $shop_name = session('shop');
        $sh = app('ShopifyAPI');
        $app_settings = AppSettingModel::where('id', 1)->first();
        $select_store = ShopModel::where('store_name', $shop_name)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store->access_token]);
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $donation_config_delete = DonationSettings::where(['id' => $dbid, 'shop_id' => $shop_find->id])->delete();
        $product = $sh->call(['URL' => '/admin/products/' . $product_id . '.json', 'METHOD' => 'DELETE']);
        Session::flash('success', 'Your product has been deleted successfully');
        return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
    }

    //Single Donation Load View
    function basicStoreIndex(Request $request) {
        $shop_name = session('shop');
        return view('add_product_basic', ['shop' => $shop_name]);
    }

    //Single Donation Store
    function basicStoreSave(Request $request) {
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $app_settings = AppSettingModel::where('id', 1)->first();
        $select_store = ShopModel::where('store_name', $shop_name)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store->access_token]);
        $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
        if (count($donation_config) > 0) {
            //Edit Product
            $saveData = DonationSettings::where('shop_id', $shop_find->id)->first();
            $donationArray = unserialize(base64_decode($saveData->donation_data));
            $donationData[] = $donationArray;
            //For Checking if Variant ID's Exists
            if ($donation_config->variant_ids != NULL) {
                $temp_product_variant_array = json_decode($donation_config->variant_ids, true);
            }
            Session::flash('error', 'Already added your product');
            return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
        } else {
            //Add Product
            if ($request->ui_select == 'D') {
                $i = 1;
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => config('app.url') . 'public/image/Donate.png'
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                //Api call for product
                $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                $product_id = $product->product->id;
                //Deleting Existing Product Variants If Exists
                if (!empty($temp_product_variant_array)) { {
                        $old_variants = array();
                        $variants = $sh->call(['URL' => "admin/products/$product_id/variants.json", 'METHOD' => 'GET']);
                        foreach ($variants as $product_variant) {
                            foreach ($product_variant as $variant) {
                                array_push($old_variants, $variant->id);
                            }
                        }
                        $old_variants = array_slice($old_variants, 1);
                    }

                    foreach ($old_variants as $variant) {
                        $variant = $sh->call(['URL' => "/admin/products/$product_id/variants/$variant.json", 'METHOD' => 'DELETE']);
                    }
                }
                $amounts = $request->dropdown_option;
                $variant_ids = array();
                //For creating product variants
                foreach ($amounts as $donation_amount) {
                    $variant_argument = [
                        'variant' => [
                            'option1' => "Donation" . $i,
                            'price' => "$donation_amount",
                            'taxable' => 'false',
                            'requires_shipping' => 'false',
                            'inventory_policy' => 'continue'
                        ]
                    ];
                    $variant = $sh->call(['URL' => "/admin/products/$product_id/variants.json", 'METHOD' => 'POST', 'DATA' => $variant_argument]);
                    array_push($variant_ids, $variant->variant->id);
                    $i++;
                }
                $product_variant_array["$product_id"] = $variant_ids;
            } else {
                //For Textbox     
                $product_argument = [
                    'product' => [
                        'title' => $request->donation_name,
                        'body_html' => $request->donation_Description,
                        'vendor' => 'zestard-easy-donation',
                        'product_type' => 'Donation',
                        'images' => array(
                            '0' => array(
                                'src' => config('app.url') . 'public/image/Donate.png'
                            )
                        ),
                        'variants' => array(
                            '0' => array(
                                'option1' => 'Donate',
                                'price' => '00.00',
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            )
                        ),
                        'inventory_policy' => 'continue'
                    ]
                ];
                //Api call for product
                $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                $product_id = $product->product->id;
            }
            $productArr = array();
            $productFinalArr = array();
            $productArr['donation_name'] = $request->donation_name;
            $productArr['donation_description'] = $request->donation_Description;
            $productArr['select_page'] = $request->select_page;
            $productArr['field_option'] = $request->ui_select;
            $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
            $productArr['dropdown_other'] = $request->dropdown_other;
            $productArr['add_min_amount'] = $request->add_min_amount;
            $productArr['product_id'] = $product->product->id;
            $productFinalArr[] = $productArr;
            $updtaedSertext = base64_encode(serialize($productFinalArr));
            $data = [
                'status' => '1',
                'select_page' => $request->select_page,
                'title' => '',
                'donation_name' => $request->donation_name,
                'description' => $request->donation_Description,
                'field_option' => $request->ui_select,
                'dropdown_option' => ($request->dropdown_option) ? json_encode($request->dropdown_option) : '',
                'dropdown_other' => ($request->dropdown_other) ? 1 : 0,
                'text_dropdown_other' => ($request->text_dropdown_other) ? $request->text_dropdown_other : '',
                'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                'add_min_amount' => ($request->add_min_amount) ? $request->add_min_amount : 0,
                'variant_ids' => '',
                'donation_data' => $updtaedSertext,
                'donation_button_text' => '',
                'additional_css' => '',
                'shop_currency' => '',
                'shop_id' => $shop_find->id,
                'send_email' => 0,
                'show_popup' => 0,
                'popup_message' => ''
            ];
            //DB::table('donation_settings')->insert($data);            
            DonationSettings::insert($data);
            Session::flash('success', 'Your product has been added successfully');
            return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
        }
    }

    public function helpPro() {
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $_GET['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        return view('help_pro', array('shop' => $shop));
    }

    /*
     * Update advance version Chandraprakash
     */

    public function Testupdate($update_data) {
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $_GET['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        return view('help_pro', array('shop' => $shop));
    }

    public function front_preview(Request $request) {
        $id = $request['id'];
        $shopData = ShopModel::where('store_encrypt', $id)->first();
        $app_settings = AppSettingModel::where('id', 1)->first();
        $shop_name = $shopData->store_name;
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $shop_api = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
        //print_r($shop_api);die;
        $currency = Symbol::where('currency_code', $shop_api->shop->currency)->first();

        //$shop = (array) $shopData;
        $shop_id = $shopData->id;
        $donation_config = DonationSettings::where('shop_id', $shop_id)->first();
        $donation_config->shop_currency = $currency->symbol_html;
        $donation_config->save();

        $donationData = [];
        $donationData['status'] = $donation_config->status;
        $donationData['title'] = $donation_config->title;
        $donationData['donation_button_text'] = $donation_config->donation_button_text;
        $donationData['additional_css'] = $donation_config->additional_css;
        $donationData['variant_ids'] = $donation_config->variant_ids;
        $donationData['donation_data'] = unserialize(base64_decode($donation_config->donation_data));
        $donationData['shop_currency'] = $donation_config->shop_currency;
        return json_encode($donationData);
    }

    public function update_demo_status(Request $request) {
        $shop = session('shop');
        //$select_store = ShopModel::where('store_name', $shop)->first();
        $update_status = ShopModel::where('store_name', $shop)->update(['new_install' => "N"]);
        return back();
    }

    public function SetDefaultEmail(Request $request) {
        $shop = session('shop');
        $shop_id = ShopModel::where('store_name', $shop)->first();
        if ($shop_id->app_version == 1) {

            $email_template = '<html><head><meta charset="utf-8"/>
	<style>.receipt-css {border: 1px solid black; padding: 20px; display: inline-block;}.title-left{float:left;}</style>
	<title>Easy Donation</title>
	</head>
	<body class="receipt-css"><div class="receipt-css">
	<header><h1 class="heading">Donation Receipt</h1></header>                                                
		<table class="donation-receipt">
			<tr><th class="title-left">First Name:</th><td>{{first_name}}</td></tr>
			<tr><th class="title-left">Last Name:</th><td>{{last_name}}</td></tr>
			<tr><th class="title-left">Email:</th><td>{{email}}</td></tr>
			<tr><th class="title-left">Date:</th><td>{{date}}</td></tr>
			<tr><th class="title-left">Donation Name:</th><td>{{donation_name}}</td></tr>
			<tr><th class="title-left">Donate Price:</th><td>{{price}}</td></tr>
			<tr><th class="title-left">Currency:</th><td>{{currency}}</td></tr>
		</table>
		<h1>Thanks For Donating!</h1>
	</div></body>
</html>';
        } elseif ($shop_id->app_version == 2) {

            $email_template = '<html><head><meta charset="utf-8"/>
	<title>Easy Donation</title>
	<style type="text/css">
	h1,h2,h3,h4,h5,h6,p,body{margin: 0;}
		a{font-weight: 600;font-size: 14px;}                                               
	</style>
	</head>
	<body style="font-family: Open Sans, sans-serif; font-weight: 700;">
		<div style="padding: 15px;background-color:#cdcdcd;float:left;margin-left: 20px;margin-top: 20px;">
			<div style="padding: 30px;background-color: #fff;" >
			<div><h1 style="font-size: 36px;color: #373634;">Donation Receipt</h1></div>                                                
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tbody>
						<tr>
							<td style="font-size: 14px;text-transform: capitalize;color: #515150;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;">First Name:</td>
							<td style="color: #515150;font-weight: 600;font-size: 14px;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;"><span>{{first_name}}<span></td>
						</tr>
						<tr>
							<td style="font-size: 14px;text-transform: capitalize;color: #515150;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;">Last Name:</td>
							<td style="color: #515150;font-weight: 600;font-size: 14px;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;"><span>{{last_name}}</span></td>
						</tr>
						<tr>
							<td style="font-size: 14px;text-transform: capitalize;color: #515150;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;">Email:</td>
							<td style="color: #515150;font-weight: 600;font-size: 14px;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;">{{email}}</td>
						</tr>
						<tr>
							<td style="font-size: 14px;text-transform: capitalize;color: #515150;padding-bottom: 5px;padding-top: 20px; border-bottom: 1px solid #b5b5b5;">Date:</td>
							<td style="color: #515150;font-weight: 600;font-size: 14px;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;"><span>{{date}}</span></td>
						</tr>
					</tbody>                                                
				</table>
				{{donation_details}}
				<div><h1 style="font-size: 32px;color: #373634;">Thanks For Donating!</h1></div>
			</div>
		</div>
	</body>
</html>	';
        }

        $emailConfig = EmailConfig::where('shop_id', $shop_id->id)->update(['html_content' => $email_template]);
    }

}
