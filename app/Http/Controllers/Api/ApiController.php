<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\ShopModel;
use App\DonationSettings;
use App\Symbol;
use App\AppSettingModel;
use App\Http\Controllers\previewController;

class ApiController extends Controller {

    public $successStatus = 200;
    
    // Test API
    public function test() {
        $user = "TEST API 1111";
        //return response()->json(['success' => 1, 'data' => $user], 200);
        return $user;
    }
    
    // Set preview in front side Shopify
    public function PreviewOnShopify(Request $request) {
        
        $previewController = new previewController();
        return $previewController->PreviewToShopify($request);
        /*$id = $request->id;        
        $shopData = ShopModel::where('store_encrypt', $id)->first(); 
        //return response()->json(['success' => 1, 'data' => $user], 200); 
        $user = "TEST1212333";
        return $request->id;*/
    }
    
    

}
