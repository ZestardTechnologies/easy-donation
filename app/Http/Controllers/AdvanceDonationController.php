<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Symbol;
use App;
use DB;
use Session;
use Illuminate\Support\Facades\View;
use App\AppSettingModel;
use App\ShopModel;
use App\EmailConfig;
use App\DonationSettings;
use App\DevelopmentStores;
use RocketCode\Shopify\API;
use Illuminate\Support\Facades\Validator;

class AdvanceDonationController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /*
     * CronJob Test Function Dont Remove Its for cronJob Testing purpose
     */
    /* public function RemoveVariantsCron(Request $request) {
      $DevelopmentStores = DevelopmentStores::get();
      foreach ($DevelopmentStores as $key => $shop) {
      $shop = $shop->dev_store_name;
      //$shop = session('shop');
      if ($shop == 'order-additional-fields.myshopify.com') {

      $shop_find = ShopModel::where('store_name', $shop)->first();
      $app_settings = AppSettingModel::where('id', 1)->first();
      $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
      $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
      $donation_data = unserialize(base64_decode($donation_config->donation_data));
      foreach ($donation_data as $key => $data) {
      $products = $sh->call(['URL' => '/admin/products/' . $data['product_id'] . '.json', 'METHOD' => 'GET']);
      foreach ($products->product->variants as $variants) {
      $variant = $sh->call(['URL' => "/admin/products/" . $data['product_id'] . "/variants/" . $variants->id . ".json", 'METHOD' => 'DELETE']);
      }
      }
      //return redirect()->route('datatable_listing_pro', ['shop' => $shop]);
      }
      }
      } */

    /*
     * For Add Shortcode in product-template.liquid file
     */

    public function SnippetCreateProduct(Request $request) {
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $request->input('shop');
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettingModel::where('id', 1)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        //api call for get theme info
        $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
        foreach ($theme->themes as $themeData) {
            if ($themeData->role == 'main') {
                $theme_id = $themeData->id;
                $product_template = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=sections/product-template.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);
                $old_str = $product_template->asset->value;
                $is_exist = "{% include 'donation' %}";
                if (strpos($old_str, $is_exist) === false) {

                    $str_to_insert = " {% include 'donation' %} ";
                    $find = "{{ product.description }}";
                    if (strpos($old_str, $find) !== false) {
                        //if find string available in liquide file
                        //Debut,Pop,Boundless,Venture,Jumpstart,Supply,Narrative,Brooklyn,Minimal THEME
                        $pos = strpos($old_str, $find) + 30;
                        $newstr = substr_replace($old_str, $str_to_insert, $pos, 0);
                        $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'sections/product-template.liquid', 'value' => $newstr]]]);
                    } else {
                        //if find string NOT available in liquide file
                        //Simple THEME
                        $find1 = "{% unless product == empty %}";
                        $pos1 = strpos($old_str, $find1) - 9;
                        $newstr = substr_replace($old_str, $str_to_insert, $pos1, 0);
                        $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'sections/product-template.liquid', 'value' => $newstr]]]);
                    }
                } else {
                    Session::flash('error', 'Your Shortcode has been already pasted in the product template page. If the donation section still does not appear, contact us for more support.');
                    return redirect()->route('dashboard', ['shop' => $shop]);
                }
            }else{
                Session::flash('error', 'Someting went wrong, Please try manual process.');
                return redirect()->route('dashboard', ['shop' => $shop]);
            }
        }
        Session::flash('success', 'Your shortcode has been added successfully in product template page');
        return redirect()->route('dashboard', ['shop' => $shop]);
    }

    /*
     * For Add Shortcode in cart-template.liquid file
     */

    function SnippetCreateCart(Request $request) {
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $request->input('shop');
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettingModel::where('id', 1)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        //api call for get theme info
        $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
        foreach ($theme->themes as $themeData) {
            if ($themeData->role == 'main') {
                $theme_id = $themeData->id;

                $product_template = $sh->customCall(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=sections/cart-template.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);
                if ($product_template) {
                    //if cart-template.liquid available
                    $product_template = $sh->customCall(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=sections/cart-template.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);
                    $old_str = $product_template->asset->value;

                    $is_exist = "{% include 'donation' %}";
                    if (strpos($old_str, $is_exist) === false) {
                        $str_to_insert = " {% include 'donation' %} ";
                        $find = "{% if section.settings.cart_notes_enable %}";
                        if (strpos($old_str, $find) !== false) {
                            //if find string available in liquide file
                            //Debut,Pop,Boundless,Venture,Jumpstart,Supply,Narrative,Brooklyn,Minimal THEME
                            $pos = strpos($old_str, $find);
                            $newstr = substr_replace($old_str, $str_to_insert, $pos, 0);
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'sections/cart-template.liquid', 'value' => $newstr]]]);
                        } else {
                            //if find string NOT available in liquide file
                            //Simple THEME
                            $find1 = "{{ cart.note }}";
                            $pos1 = strpos($old_str, $find1) + 30;
                            $newstr = substr_replace($old_str, $str_to_insert, $pos1, 0);
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'sections/cart-template.liquid', 'value' => $newstr]]]);
                        }
                    } else {
                        Session::flash('error', 'Your Shortcode has been already pasted in the cart template page. If the donation section still does not appear, contact us for more support.');
                        return redirect()->route('dashboard', ['shop' => $shop]);
                    }
                } else {
                    //if cart-template.liquid not available
                    $product_template = $sh->customCall(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=templates/cart.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);
                    $old_str = $product_template->asset->value;
                    $is_exist = "{% include 'donation' %}";
                    if (strpos($old_str, $is_exist) === false) {

                        $str_to_insert = " {% include 'donation' %} ";
                        $find = "{{ cart.note }}";
                        $pos = strpos($old_str, $find) + 30;
                        $newstr = substr_replace($old_str, $str_to_insert, $pos, 0);
                        //api call for creating snippets                 
                        $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'templates/cart.liquid', 'value' => $newstr]]]);
                    } else {
                        Session::flash('error', 'Your Shortcode has been already pasted in the cart template page. If the donation section still does not appear, contact us for more support.');
                        return redirect()->route('dashboard', ['shop' => $shop]);
                    }
                }
            }
        }
        Session::flash('success', 'Your shortcode has been added successfully in cart template page');
        return redirect()->route('dashboard', ['shop' => $shop]);
    }

    public function createEdit($product_id = '') {
        if ($product_id) {

            $shop = session('shop');
            if (!isset($shop)) {
                $shop = $input['shop'];
            }
            $shop_find = ShopModel::where('store_name', $shop)->first();
            $app_settings = AppSettingModel::where('id', 1)->first();
            $select_store = ShopModel::where('store_name', $shop)->first();
            $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
            $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
            $donationData = [];
            $donationData['donation_data'] = unserialize(base64_decode($donation_config->donation_data));
            //echo "<pre>"; print_r($donationData['donation_data']); die;
            $selected_product = array();
            
            foreach ($donationData['donation_data'] as $key => $data) {
                // If Data is not array then convert to object (fix old version conflict)
                if(!is_object($data)){
                    $data = (object)$data;
                }
                if ($data->product_id == $product_id) {
                    $selected_product = $data;
                }
            }
            $product = $sh->call(['URL' => '/admin/products/' . $product_id . '.json', 'METHOD' => 'GET']);
            return view('advance.edit_product', compact(['shop', 'product', 'donation_config', 'selected_product']));
        } else {
            $shop_name = session('shop');
            return view('advance.add_product', ['shop' => $shop_name]);
        }
    }

    public function save(Request $request) {
        $rules = [
            'upload_donation_image' => 'max:2048|mimes:jpeg,png,jpg',
        ];
        $message = [
            'upload_donation_image.max' => 'The upload donation image may not be greater than 2 MB'
        ];
        $this->validate($request, $rules, $message);

        $shop_name = session('shop');
        $temp_product_variant_array = array();
        if ($request->product_id) {
            //Update            
            if (!isset($shop_name)) {
                $shop_name = $input['shop'];
            }
            $sh = app('ShopifyAPI');
            $app_settings = AppSettingModel::where('id', 1)->first();
            $select_store = ShopModel::where('store_name', $shop_name)->first();
            $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store->access_token]);
            $product = $sh->call(['URL' => '/admin/products/' . $request->product_id . '.json', 'METHOD' => 'GET']);
            $shop_find = ShopModel::where('store_name', $shop_name)->first();
            $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();
            $donationArray = unserialize(base64_decode($donation_config->donation_data));
            
            $product_variant_array = array();
            $productArr = array();
            //Checking if Variant ID's Exists
            if ($donation_config->variant_ids != NULL) {
                $temp_product_variant_array = json_decode($donation_config->variant_ids, true);
            }
            foreach ($donationArray as $key => $donation) {
                if(!is_object($donation)){
                    $donation = (object) $donation;
                }
                if ($request['product_id'] == $donation->product_id) {
                    if ($request->upload_donation_image != '') {
                        $images = time() . '_' . $request->upload_donation_image->getClientOriginalName();
                        $request->upload_donation_image->move(('image/product/'), $images);
                        $img = config('app.url') . 'public/image/product/' . $images;
                    } else if ($request->upload_donation_image == '' && $product->product->image != '') {
                        $image = $product->product->image->src;
                        $img = $image;
                    } else {
                        $img = config('app.url') . 'public/image/Donate.png';
                    }
                    $productArr = $donation;
                    $field_option = $donation->field_option;
                    $product_argument = [
                        'product' => [
                            'title' => $request->donation_name,
                            'body_html' => $request->donation_Description,
                            'vendor' => 'zestard-easy-donation',
                            'product_type' => 'Donation',
                            'images' => array(
                                '0' => array(
                                    'src' => $img
                                )
                            ),
                            'variants' => array(
                                '0' => array(
                                    'option1' => 'Donate',
                                    'price' => '00.00',
                                    'taxable' => 'false',
                                    'requires_shipping' => 'false',
                                    'inventory_policy' => 'continue'
                                )
                            ),
                            'inventory_policy' => 'continue'
                        ]
                    ];
                    $product = $sh->call(['URL' => '/admin/products/' . $request['product_id'] . '.json', 'METHOD' => 'PUT', 'DATA' => $product_argument]);
                    $productArr = array();
                    $productFinalArr = array();
                    $productArr['donation_name'] = $request->donation_name;
                    $productArr['donation_description'] = $request->donation_Description;
                    $productArr['select_page'] = $request->select_page;
                    $productArr['field_option'] = $request->ui_select;
                    $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
                    if ($request->dropdown_other == "checked") {
                        $productArr['dropdown_other'] = 1;
                    } else {
                        $productArr['dropdown_other'] = 0;
                    }
                    if ($request->ui_select == 'P') {
                        $productArr['donation_min'] = $request->donation_min;
                        $productArr['donation_max'] = $request->donation_max;
                    }
                    if ($request->ui_select == 'T') {
                        $productArr['add_min_amount'] = $request->add_min_amount;
                    }
                    if ($request->ui_select == 'D') {
                        $productArr['text_dropdown_other'] = ($request->text_dropdown_other) ? $request->text_dropdown_other : 'Other';
                    }
                    
                    $productArr['product_id'] = $product->product->id;
                    $donation_productId = $donation->product_id;
                    $donationName = $donation->donation_name;
                    $donationDescription = $donation->donation_description;
                    $donationArray[$key] = (object) $productArr;
                    $updtaedSertext = base64_encode(serialize($donationArray));
                    if ($request->ui_select == 'T') {
                        $update_data = [
                            'status' => '1',
                            'select_page' => $request->select_page,
                            'donation_name' => $request->donation_name,
                            'description' => $request->donation_Description,
                            'field_option' => $request->ui_select,
                            'dropdown_option' => '',
                            'dropdown_other' => 0,
                            'text_dropdown_other' => '',
                            'bar_min' => 0,
                            'bar_max' => 0,
                            'add_min_amount' => ($request->add_min_amount) ? $request->add_min_amount : 0,
                            'donation_data' => $updtaedSertext,
                            'shop_currency' => '',
                            'shop_id' => $shop_find->id,
                        ];
                    } elseif ($request->ui_select == 'P') {
                        $update_data = [
                            'status' => '1',
                            'select_page' => $request->select_page,
                            'donation_name' => $request->donation_name,
                            'description' => $request->donation_Description,
                            'field_option' => $request->ui_select,
                            'dropdown_option' => '',
                            'dropdown_other' => 0,
                            'text_dropdown_other' => '',
                            'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                            'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                            'add_min_amount' => 0,
                            'donation_data' => $updtaedSertext,
                            'shop_currency' => '',
                            'shop_id' => $shop_find->id,
                        ];
                    } elseif ($request->ui_select == 'D') {
                        $i = 1;
                        //For Checking if Variants Exists
                        if (!empty($temp_product_variant_array)) {
                            //For Checking if Particular Product Variant Exists
                            if (!empty($temp_product_variant_array["$donation_productId"])) {
                                //For Deleteing Existing Variants
                                $old_variants = array();
                                $variants = $sh->call(['URL' => "admin/products/$donation_productId/variants.json", 'METHOD' => 'GET']);
                                foreach ($variants as $product_variant) {
                                    foreach ($product_variant as $variant) {
                                        array_push($old_variants, $variant->id);
                                    }
                                }
                                $old_variants = array_slice($old_variants, 1);
                                foreach ($old_variants as $variant) {
                                    $variant = $sh->call(['URL' => "/admin/products/$donation_productId/variants/$variant.json", 'METHOD' => 'DELETE']);
                                }
                            }
                        }
                        //DropDown Amounts
                        $amounts = $request->dropdown_option;
                        $variant_ids = array();
                        //For Creating Product Variants
                        foreach ($amounts as $donation_amount) {
                            $variant_argument = [
                                'variant' => [
                                    'option1' => "Donation" . $i,
                                    'price' => "$donation_amount",
                                    'taxable' => 'false',
                                    'requires_shipping' => 'false',
                                    'inventory_policy' => 'continue'
                                ]
                            ];
                            $variant = $sh->call(['URL' => "/admin/products/$donation_productId/variants.json", 'METHOD' => 'POST', 'DATA' => $variant_argument]);
                            array_push($variant_ids, $variant->variant->id);
                            $i++;
                        }
                        //For Storing Product-Wise Variant Id's
                        $product_variant_array ["$donation_productId"] = $variant_ids;
                        $donationArray[$key] = (object) $productArr;

                        $updtaedSertext = base64_encode(serialize($donationArray));
                        $update_data = [
                            'status' => '1',
                            'select_page' => $request->select_page,
                            'donation_name' => $request->donation_name,
                            'description' => $request->donation_Description,
                            'field_option' => $request->ui_select,
                            'dropdown_option' => ($request->dropdown_option) ? json_encode($request->dropdown_option) : '',
                            'dropdown_other' => ($request->dropdown_other) ? 1 : 0,
                            'text_dropdown_other' => ($request->text_dropdown_other) ? $request->text_dropdown_other : 'Other',
                            'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                            'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                            'add_min_amount' => 0,
                            'donation_data' => $updtaedSertext,
                            'shop_currency' => '',
                            'shop_id' => $shop_find->id,
                        ];
                    }
                    $update_donationData = DonationSettings::where('shop_id', $shop_find->id)->update($update_data);
                    $firstKey = array_keys($product_variant_array);
                    if ($request->ui_select == 'D') {
                        $newkey = $firstKey[0];
                        $temp_product_variant_array[$newkey] = $product_variant_array[$newkey];
                        if (!empty($product_variant_array)) {
                            DonationSettings::where('shop_id', $shop_find->id)->update(['variant_ids' => json_encode($temp_product_variant_array)]);
                        }
                    }
                    Session::flash('success', 'Your product has been updated successfully');
                    return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
                }
            }
        } else {
            //Add            
            $shop_find = ShopModel::where('store_name', $shop_name)->first();
            $app_settings = AppSettingModel::where('id', 1)->first();
            $select_store = ShopModel::where('store_name', $shop_name)->first();
            $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store->access_token]);
            $donation_config = DonationSettings::where('shop_id', $shop_find->id)->first();

            if ($request->upload_donation_image != '') {
                $images = time() . '_' . $request->upload_donation_image->getClientOriginalName();
                $request->upload_donation_image->move(('image/product/'), $images);
                $img = config('app.url') . 'public/image/product/' . $images;
            } else {
                $img = config('app.url') . 'public/image/Donate.png';
            }

            if (count($donation_config) > 0) {
                //Edit Product
                $saveData = DonationSettings::where('shop_id', $shop_find->id)->first();
                $donationArray = unserialize(base64_decode($saveData->donation_data));
                $donationData[] = $donationArray;
                $product_variant_array = array();
                //For Checking if Variant ID's Exists                
                if ($donation_config->variant_ids != NULL) {
                    $temp_product_variant_array = json_decode($donation_config->variant_ids, true);
                }
                if ($request->ui_select == 'D') {
                    $i = 1;
                    $product_argument = [
                        'product' => [
                            'title' => $request->donation_name,
                            'body_html' => $request->donation_Description,
                            'vendor' => 'zestard-easy-donation',
                            'product_type' => 'Donation',
                            'images' => array(
                                '0' => array(
                                    'src' => $img
                                )
                            ),
                            'inventory_policy' => 'continue'
                        ]
                    ];
                    //api call for updating product
                    $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                    $product_id = $product->product->id;
                    $donation_productId = $product->product->id;
                    //For Checking if Variants Exists
                    if (!empty($temp_product_variant_array)) {
                        //For Checking if Particular Product Variant Exists                        
                        if (!empty($temp_product_variant_array["$donation_productId"])) {
                            //For Deleteing Existing Variants	
                            $old_variants = array();
                            $variants = $sh->call(['URL' => "admin/products/$donation_productId/variants.json", 'METHOD' => 'GET']);
                            foreach ($variants as $product_variant) {
                                foreach ($product_variant as $variant) {
                                    array_push($old_variants, $variant->id);
                                }
                            }
                            $old_variants = array_slice($old_variants, 1);
                            foreach ($old_variants as $variant) {
                                $variant = $sh->call(['URL' => "/admin/products/$donation_productId/variants/$variant.json", 'METHOD' => 'DELETE']);
                            }
                        }
                    }
                    //DropDown Amounts
                    $amounts = $request->dropdown_option;
                    $variant_ids = array();
                    //For Creating Product Variants
                    foreach ($amounts as $donation_amount) {
                        $variant_argument = [
                            'variant' => [
                                'option1' => "Donation" . $i,
                                'price' => "$donation_amount",
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            ]
                        ];
                        $variant = $sh->call(['URL' => "/admin/products/$product_id/variants.json", 'METHOD' => 'POST', 'DATA' => $variant_argument]);
                        array_push($variant_ids, $variant->variant->id);
                        $i++;
                    }

                    //For Storing Product-Wise Variant Id's
                    $product_variant_array ["$product_id"] = $variant_ids;
                    $productArr = array();
                    $productArr['donation_name'] = $request->donation_name;
                    $productArr['donation_description'] = $request->donation_Description;
                    $productArr['select_page'] = $request->select_page;
                    $productArr['field_option'] = $request->ui_select;
                    $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
                    if ($request->dropdown_other == "checked") {
                        $productArr['dropdown_other'] = 1;
                    } else {
                        $productArr['dropdown_other'] = 0;
                    }
                    $productArr['text_dropdown_other'] = ($request->text_dropdown_other) ? $request->text_dropdown_other : 'Other';
                    $productArr['add_min_amount'] = $request->add_min_amount;
                    $productArr['product_id'] = $product->product->id;
                    // converted to object to match with old version of code
                    if (!is_array($donationArray)) {
                        $donationArray = array();
                    }
                    $productObject = (object) $productArr;
                    array_push($donationArray, $productObject);

                    $updtaedSertext = base64_encode(serialize($donationArray));
                } else {
                    $product_argument = [
                        'product' => [
                            'title' => $request->donation_name,
                            'body_html' => $request->donation_Description,
                            'vendor' => 'zestard-easy-donation',
                            'product_type' => 'Donation',
                            'images' => array(
                                '0' => array(
                                    'src' => $img
                                )
                            ),
                            'variants' => array(
                                '0' => array(
                                    'option1' => 'Donate',
                                    'price' => '00.00',
                                    'taxable' => 'false',
                                    'requires_shipping' => 'false',
                                    'inventory_policy' => 'continue'
                                )
                            ),
                            'inventory_policy' => 'continue'
                        ]
                    ];
                    //Api call for updating product
                    $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                    $product_id = $product->product->id;
                    $productArr = array();
                    $productArr['donation_name'] = $request->donation_name;
                    $productArr['donation_description'] = $request->donation_Description;
                    $productArr['select_page'] = $request->select_page;
                    $productArr['field_option'] = $request->ui_select;
                    $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
                    if ($request->ui_select == 'P') {
                        $productArr['donation_min'] = $request->donation_min;
                        $productArr['donation_max'] = $request->donation_max;
                    }
                    if ($request->ui_select == 'T') {
                        $productArr['dropdown_other'] = $request->dropdown_other;
                        $productArr['add_min_amount'] = $request->add_min_amount;
                    }
                    $productArr['product_id'] = $product->product->id;
                    if (!is_array($donationArray)) {
                        $donationArray = array();
                    }
                    //echo "<pre>"; print_r($donationArray); die;
                    $productObject = (object) $productArr;
                    array_push($donationArray, $productObject);
                    $updtaedSertext = base64_encode(serialize($donationArray));
                }
                $update_donationData = DonationSettings::where('shop_id', $shop_find->id)->update(['donation_data' => "'" . $updtaedSertext . "'"]);
                //Update Variants JSON                
                $final_variant_array = $temp_product_variant_array + $product_variant_array;
                if (!empty($product_variant_array)) {
                    DonationSettings::where('shop_id', $shop_find->id)->update(['variant_ids' => json_encode($final_variant_array)]);
                }
                Session::flash('success', 'Your product has been added successfully');
                return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
            } else {
                //Add Product                    
                if ($request->ui_select == 'D') {
                    $i = 1;
                    $product_argument = [
                        'product' => [
                            'title' => $request->donation_name,
                            'body_html' => $request->donation_Description,
                            'vendor' => 'zestard-easy-donation',
                            'product_type' => 'Donation',
                            'images' => array(
                                '0' => array(
                                    'src' => $img
                                )
                            ),
                            'inventory_policy' => 'continue'
                        ]
                    ];
                    //Api call for product
                    $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                    $product_id = $product->product->id;
                    //For deleting existing product variants if exists
                    if (!empty($temp_product_variant_array)) { {
                            $old_variants = array();
                            $variants = $sh->call(['URL' => "admin/products/$product_id/variants.json", 'METHOD' => 'GET']);
                            foreach ($variants as $product_variant) {
                                foreach ($product_variant as $variant) {
                                    array_push($old_variants, $variant->id);
                                }
                            }
                            $old_variants = array_slice($old_variants, 1);
                        }
                        foreach ($old_variants as $variant) {
                            $variant = $sh->call(['URL' => "/admin/products/$product_id/variants/$variant.json", 'METHOD' => 'DELETE']);
                        }
                    }
                    $amounts = $request->dropdown_option;
                    $variant_ids = array();
                    //Creating product variants
                    foreach ($amounts as $donation_amount) {
                        $variant_argument = [
                            'variant' => [
                                'option1' => "Donation" . $i,
                                'price' => "$donation_amount",
                                'taxable' => 'false',
                                'requires_shipping' => 'false',
                                'inventory_policy' => 'continue'
                            ]
                        ];
                        $variant = $sh->call(['URL' => "/admin/products/$product_id/variants.json", 'METHOD' => 'POST', 'DATA' => $variant_argument]);
                        array_push($variant_ids, $variant->variant->id);
                        $i++;
                    }
                    $product_variant_array["$product_id"] = $variant_ids;
                } else {
                    //For Textbox
                    $product_argument = [
                        'product' => [
                            'title' => $request->donation_name,
                            'body_html' => $request->donation_Description,
                            'vendor' => 'zestard-easy-donation',
                            'product_type' => 'Donation',
                            'images' => array(
                                '0' => array(
                                    'src' => $img
                                )
                            ),
                            'variants' => array(
                                '0' => array(
                                    'option1' => 'Donate',
                                    'price' => '00.00',
                                    'taxable' => 'false',
                                    'requires_shipping' => 'false',
                                    'inventory_policy' => 'continue'
                                )
                            ),
                            'inventory_policy' => 'continue'
                        ]
                    ];
                    //Api call for product
                    $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
                    $product_id = $product->product->id;
                }
                $productArr = array();
                $productFinalArr = array();
                $productArr['donation_name'] = $request->donation_name;
                $productArr['donation_description'] = $request->donation_Description;
                $productArr['select_page'] = $request->select_page;
                $productArr['field_option'] = $request->ui_select;
                $productArr['drop_down'] = ($request->dropdown_option) ? json_encode($request->dropdown_option) : '';
                if ($request->dropdown_other == "checked") {
                    $productArr['dropdown_other'] = 1;
                } else {
                    $productArr['dropdown_other'] = 0;
                }
                if ($request->ui_select == 'P') {
                    $productArr['donation_min'] = $request->donation_min;
                    $productArr['donation_max'] = $request->donation_max;
                }
                if ($request->ui_select == 'T') {
                    $productArr['add_min_amount'] = $request->add_min_amount;
                }
                if ($request->ui_select == 'D') {
                    $productArr['text_dropdown_other'] = ($request->text_dropdown_other) ? $request->text_dropdown_other : 'Other'; //conf
                }
                $productArr['product_id'] = $product->product->id;                
                
                $productFinalArr[0] = (object) $productArr;
                $updtaedSertext = base64_encode(serialize($productFinalArr));               
                
                $data = [
                    'status' => '1',
                    'select_page' => $request->select_page,
                    //'title' => '',
                    'donation_name' => $request->donation_name,
                    'description' => $request->donation_Description,
                    'field_option' => $request->ui_select,
                    'dropdown_option' => ($request->dropdown_option) ? json_encode($request->dropdown_option) : '',
                    'dropdown_other' => ($request->dropdown_other) ? 1 : 0,
                    'text_dropdown_other' => ($request->text_dropdown_other) ? $request->text_dropdown_other : 'Other',
                    'bar_min' => ($request->donation_min) ? $request->donation_min : 0,
                    'bar_max' => ($request->donation_max) ? $request->donation_max : 0,
                    'add_min_amount' => ($request->add_min_amount) ? $request->add_min_amount : 0,
                    'donation_data' => $updtaedSertext,
                    //'donation_button_text' => '',
                    //'additional_css' => '',
                    'shop_currency' => '',
                    'shop_id' => $shop_find->id,
                        //'send_email' => 0,
                        //'show_popup' => 0,
                        //'popup_message' => ''
                ];
                DonationSettings::insert($data);
                if (!empty($product_variant_array)) {
                    DonationSettings::where('shop_id', $shop_find->id)->update(['variant_ids' => json_encode($product_variant_array)]);
                }
                Session::flash('success', 'Your product has been added successfully');
                return redirect()->route('datatable_listing_pro', ['shop' => $shop_name]);
            }
        }
    }

}
