<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\EmailConfig;
use App\DonationSettings;
use Mail;
use Session;
use Illuminate\Support\Facades\View;
use App\AppSettingModel;
use App\TrialInfo;
use App\DevelopmentStores;

class callbackController extends Controller {

    var $headers = "";
    public function __construct() {
        $this->headers = 'MIME-Version: 1.0' . "\r\n";
        $this->headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $this->middleware(function ($request, $next) {
            $shop_name = session('shop');
            $shop_find = ShopModel::where('store_name', $shop_name)->first();
            View::share('shop_find', $shop_find);
            return $next($request);
        });
    }

    public function index(Request $request) {
        @session_start();
        $sh = app('ShopifyAPI');
        $app_settings = AppSettingModel::where('id', 1)->first();
        if (!empty($_GET['shop'])) {            
            $shop = $_GET['shop'];
            //$select_store = ShopModel::where('store_name', $shop)->get();
            $select_store = ShopModel::where('store_name', $shop)->first();            
            if (count($select_store) > 0) {
                //remove this comment  if you want to create free version of app
                //session(['shop' => $shop]);
                //return redirect()->route('dashboardpro');
                //Remove comment for the Payment method
                $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
                $id = $select_store->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store->charge_id;
                $charge_status = $select_store->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    if ($select_store->app_version > 0 && $select_store->type_of_app > 0) {
                        session(['shop' => $shop]);
                        $_SESSION["shop"] = $shop;
                        if ($select_store->app_version == 1) {
                            return redirect()->route('dashboard', ['shop' => $shop]);
                        } else {
                            return redirect()->route('dashboard', ['shop' => $shop]);
                        }
                    } else {
                        return redirect()->route('app_version', ['shop' => $shop]);
                    }
                } else {
                    return redirect()->route('payment_process');
                }
            } else {
                $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
				    
                if ($shop == "easy-donation-demo.myshopify.com")
				{
					$permission_url = $sh->installURL(['permissions' => array('read_script_tags', 'write_script_tags', 'read_themes', 'write_content', 'write_themes', 'read_products', 'write_products', 'read_orders', 'read_all_orders', 'write_orders', 'read_locations'), 'redirect' => $app_settings->redirect_url]);
				}
				else{
				  
				
                    $permission_url = $sh->installURL(['permissions' => array('read_script_tags', 'write_script_tags', 'read_themes', 'write_content', 'write_themes', 'read_products', 'write_products', 'read_orders', 'read_all_orders', 'write_orders', 'read_locations'), 'redirect' => $app_settings->redirect_url]);
                }
                
                return redirect($permission_url);
            }
        }
    }

    public function redirect(Request $request) {
        $app_settings = AppSettingModel::where('id', 1)->first();
        if (!empty($request->input('shop')) && !empty($request->input('code'))) {
            $shop = $request->input('shop'); //shop name            
            $select_store = ShopModel::where('store_name', $shop)->first();
            if (count($select_store) > 0) {
                //remove this comment  if you want to create free version of app
                /* session(['shop' => $shop]);
                  return redirect()->route('dashboardpro'); */
                //Remove coment for the Payment method
                $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
                $id = $select_store->charge_id;
                $url = 'admin/recurring_application_charges' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store->charge_id;
                $charge_status = $select_store->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard', ['shop' => $shop]);
                } else {
                    return redirect()->route('payment_process');
                }
            }
            $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
            try {
                $verify = $sh->verifyRequest($request->all());                
                if ($verify) {
                    $code = $request->input('code');
                    $accessToken = $sh->getAccessToken($code);
                    //ShopModel::insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' => ""]);
                    DB::table('usersettings')->insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' => ""]);
                    $shop_find = ShopModel::where('store_name', $shop)->first();
                    $shop_id = $shop_find->id;
                    $donation_encrypt = crypt($shop_id, "ze");
                    $finaly_encrypt = str_replace(['/', '.'], "Z", $donation_encrypt);
                    $update_encrypt = ShopModel::where('id', $shop_id)->update(['store_encrypt' => $finaly_encrypt]);
                    $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
                    //for creating the uninstall webhook
                    $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
                    $webhookData = [
                        'webhook' => [
                            'topic' => 'app/uninstalled',
                            'address' => config('app.url') . 'uninstall.php',
                            'format' => 'json'
                        ]
                    ];
                    $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);
                    //deleting the donation product when install 2nd time
                    $allProduct = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'GET']);
                    foreach ($allProduct->products as $productData) {
                        if ($productData->vendor == "zestard-easy-donation") {
                            $oldDonateId = $productData->id;
                            $productDelete = $sh->call(['URL' => '/admin/products/' . $oldDonateId . '.json', 'METHOD' => 'DELETE']);
                        }
                    }

                    //api call for get theme info
                    $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);

                    foreach ($theme->themes as $themeData) {
                        if ($themeData->role == 'main') {
                            $snippets_arguments = ['id' => $finaly_encrypt];
                            $theme_id = $themeData->id;
                            $view = (string) View('snippets', $snippets_arguments);
                            //api call for creating snippets
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/donation.liquid', 'value' => $view]]]);
                            $hide_product = (string) View('hide_product', $snippets_arguments);
                            //api call for creating snippets
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/hide_product.liquid', 'value' => $hide_product]]]);
                        }
                    }
                    //remove comment to stop creating the recurring charge create
                    //return redirect('dashboardpro');
                    //Check if trial is still running
                    $check_trial = TrialInfo::where('store_name', $shop)->first();
                    if (count($check_trial) > 0) { // when trial is started
                        $total_trial_days = $check_trial->trial_days;
                        $trial_activated_date = $check_trial->activated_on;
                        $trial_over_date = $check_trial->trial_ends_on;
                        $current_date = date("Y-m-d");
                        if (strtotime($current_date) < strtotime($trial_over_date)) { // in trial days
                            $date1 = date_create($trial_over_date);
                            $date2 = date_create($current_date);
                            $trial_remain = date_diff($date2, $date1);
                            $new_trial_days = $trial_remain->format("%a");
                        } else {
                            $new_trial_days = 0;
                        }                        
                        if ($shop == "easy-donation-demo.myshopify.com" || $shop == "mobile-app-demo.myshopify.com" || $shop == "zankar-test.myshopify.com" || $shop == "anywhere-custom-forms.myshopify.com" || $shop == "customer-additional-field.myshopify.com" || $shop == "order-additional-fields.myshopify.com" || $shop == "article-additional-fields.myshopify.com" || $shop == "collection-additional-fields.myshopify.com" || $shop == "broadcast-bar.myshopify.com" || $shop == "ishita-test.myshopify.com" || $shop == "vijay-test.myshopify.com" || $shop == "free-theme-test.myshopify.com" || $shop == "find-test.myshopify.com" || $shop == "dipal-test.myshopify.com" || $shop == "new-easy-donation-demo.myshopify.com") {
                            $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                            $charge = $sh->call([
                                'URL' => $url,
                                'METHOD' => 'POST',
                                'DATA' => array(
                                    'recurring_application_charge' => array(
                                        'name' => 'Easy Donation',
                                        'price' => 0.01,
                                        'return_url' => url('payment_success'),
                                        'capped_amount' => 20,
                                        'terms' => 'Easy donation Advance feature also avaliable',
                                        'trial_days' => $new_trial_days,
                                        'test' => true
                                    )
                                )], false);
                        } else {
                            //creating the Recuring charge for app
                            $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                            $charge = $sh->call([
                                'URL' => $url,
                                'METHOD' => 'POST',
                                'DATA' => array(
                                    'recurring_application_charge' => array(
                                        'name' => 'Easy Donation',
                                        'price' => 4.99,
                                        'return_url' => url('payment_success'),
                                        'capped_amount' => 20,
                                        'terms' => 'Easy donation Advance feature also avaliable',
                                        'trial_days' => $new_trial_days,
                                        //'test' => true
                                    )
                                )
                                    ], false);
                        }
                    } else {
                        //creating the trial for first time
                        if ($shop == "easy-donation-demo.myshopify.com" || $shop == "mobile-app-demo.myshopify.com" || $shop == "zankar-test.myshopify.com" || $shop == "anywhere-custom-forms.myshopify.com" || $shop == "customer-additional-field.myshopify.com" || $shop == "order-additional-fields.myshopify.com" || $shop == "article-additional-fields.myshopify.com" || $shop == "collection-additional-fields.myshopify.com" || $shop == "broadcast-bar.myshopify.com" || $shop == "ishita-test.myshopify.com" || $shop == "vijay-test.myshopify.com" || $shop == "free-theme-test.myshopify.com" || $shop == "find-test.myshopify.com" || $shop == "dipal-test.myshopify.com" || $shop == "new-easy-donation-demo.myshopify.com") {
                            
                            $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                            $charge = $sh->call([
                                'URL' => $url,
                                'METHOD' => 'POST',
                                'DATA' => array(
                                    'recurring_application_charge' => array(
                                        'name' => 'Easy Donation',
                                        'price' => 0.01,
                                        'return_url' => url('payment_success'),
                                        'capped_amount' => 20,
                                        'terms' => 'Easy donation Advance feature also avaliable',
                                        'trial_days' => 3,
                                        'test' => true
                                    )
                                )
                                    ], false);
                        } else {
                            //creating the Recuring charge for app
                            $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                            $charge = $sh->call([
                                'URL' => $url,
                                'METHOD' => 'POST',
                                'DATA' => array(
                                    'recurring_application_charge' => array(
                                        'name' => 'Easy Donation',
                                        'price' => 4.99,
                                        'return_url' => url('payment_success'),
                                        'capped_amount' => 20,
                                        'terms' => 'Easy donation Advance feature also avaliable',
                                        'trial_days' => 3,
                                        //'test' => true
                                    )
                                )
                                    ], false);
                        }
                    }
                    //updating the status of the recuring charge in database 
                    $create_charge = ShopModel::where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);
                    $shopi_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);

                    //for the installation follow up mail for cliant
                    $subject = "Zestard Installation Greetings :: Easy Donation";
                    $sender = "support@zestard.com";
                    $sender_name = "Zestard Technologies";
                    $app_name = "Easy Donation";
                    $logo = config('app.url') . 'public/image/zestard-logo.png';
                    $installation_follow_up_msg = '<html>

                        <head>
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <style>
                                @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i");
                                @media only screen and (max-width:599px) {
                                    table {
                                        width: 100% !important;
                                    }
                                }
                                
                                @media only screen and (max-width:412px) {
                                    h2 {
                                        font-size: 20px;
                                    }
                                    p {
                                        font-size: 13px;
                                    }
                                    .easy-donation-icon img {
                                        width: 120px;
                                    }
                                }
                            </style>
                        
                        </head>
                        
                        <body style="background: #f4f4f4; padding-top: 57px; padding-bottom: 57px;">
                            <table class="main" border="0" cellspacing="0" cellpadding="0" width="600px" align="center" style="border: 1px solid #e6e6e6; background:#fff; ">
                                <tbody>
                                    <tr>
                                        <td style="padding: 30px 30px 10px 30px;" class="review-content">
                                            <p class="text-align:left;"><img src="' . $logo . '" alt=""></p>
                                            <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px; line-height: 25px; margin-top: 0px;"><b>Hi ' . $shopi_info->shop->shop_owner . '</b>,</p>
                                            <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Thanks for Installing Zestard Application ' . $app_name . '</p>
                                            <p style="font-family: \'Helvetica\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We appreciate your kin interest for choosing our application and hope that you have a wonderful experience.</p>
                                            <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Please don\'t feel hesitate to reach us in case of any queries or questions at <a href="mailto:support@zestard.com" style="text-decoration: none;color: #1f98ea;font-weight: 600;">support@zestard.com</a>.</p>
                                            <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We also do have live chat support services for quick response and resolution of queries.</p>
                                            <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">(Please Note: Support services are available according to the IST Time Zone(i.e GMT 5:30+) as we reside in India. Timings are from 10:00am to 7:00pm)</p>
                        
                                        </td>
                                    </tr>
                        
                                    <tr>
                                        <td style="padding: 20px 30px 30px 30px;">
                        
                                            <br>
                                            <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 26px; margin-bottom:0px;">Thanks,<br>Zestard Support</p>
                                        </td>
                                    </tr>
                        
                                </tbody>
                            </table>
                        </body>';

                    $receiver = $shopi_info->shop->email;
                    try {
                        Mail::raw([], function ($message) use($sender,$sender_name,$receiver,$subject,$installation_follow_up_msg) {
                          $message->from($sender,$sender_name);
                          $message->to($receiver)->subject($subject);
                          $message->setBody($installation_follow_up_msg, 'text/html');
                        });
                    } catch (\Exception $ex) {
                        echo "error";
                        //dd($ex);
                    }

                    //mail for the installation of app
                    $msg = '<table>
                                <tr>
                                    <th>Shop Name</th>
                                    <td>' . $shopi_info->shop->name . '</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>' . $shopi_info->shop->email . '</td>
                                </tr>
                                <tr>
                                    <th>Domain</th>
                                    <td>' . $shopi_info->shop->domain . '</td>
                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td>' . $shopi_info->shop->phone . '</td>
                                </tr>
                                <tr>
                                    <th>Shop Owner</th>
                                    <td>' . $shopi_info->shop->shop_owner . '</td>
                                </tr>
                                <tr>
                                    <th>Country</th>
                                    <td>' . $shopi_info->shop->country_name . '</td>
                                </tr>
                                <tr>
                                    <th>Plan</th>
                                    <td>' . $shopi_info->shop->plan_name . '</td>
                                </tr>
                            </table>';
                    $store_details = DevelopmentStores::where('dev_store_name', $shop)->first();

                    if (count($store_details) <= 0) {
                        mail("support@zestard.com", "Easy Donation App Installed", $msg, $this->headers);
                        mail("chandraprakash.zestard@gmail.com", "Easy Donation App Installed", $msg, $this->headers);
                        //mail("vijay.zestard@gmail.com", "Easy Donation App Installed", $msg, $this->headers);
                    }

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } else {
                    // Issue with data
                }
            } catch (Exception $e) {
                echo '<pre>Error: ' . $e->getMessage() . '</pre>';
            }
        }
    }

    public function dashboard(Request $request) {
        $shop = session('shop');
        if (empty($shop)) {
            $shop = $request->shop;
            session(['shop' => $shop]);
        }
        $select_store = ShopModel::where('store_name', $shop)->first();
        $exist_data = DonationSettings::where('shop_id', $select_store->id)->first();
        if ($select_store->app_version == 1 && $select_store->type_of_app == 1) {
            // for basic verison
            $add_donation_route = 'basic_donation_create_edit';
            
            if (count($exist_data)) {
                $donation_data = unserialize(base64_decode($exist_data->donation_data));
                if(!empty($exist_data->donation_data)){
                    $product_id = $donation_data[0]->product_id;
                }else{
                    $product_id = $select_store->product_id;
                }
                
            } else {
                $product_id = '';
            }
        } else {
            // for advance verison
            if (count($exist_data)) {
                $add_donation_route = 'datatable_listing_pro';
            } else {
                $add_donation_route = 'advance_donation_create_edit';
            }
            $product_id = '';
        }
        $intro_status = $select_store->new_install;
        return view('dashboard', compact(['shop', 'add_donation_route', 'product_id', 'intro_status']));
    }

    public function help() {
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $_GET['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        return view('help', array('shop' => $shop));
    }

    public function helppro() {
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $_GET['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        return view('helppro', array('shop' => $shop));
    }

    public function payment_method(Request $request) {
        $shop = session('shop');
        $app_settings = AppSettingModel::where('id', 1)->first();
        $select_store = ShopModel::where('store_name', $shop)->first();
        if (count($select_store) > 0) {
            $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
            $charge_id = $select_store->charge_id;
            $url = 'admin/recurring_application_charges/' . $charge_id . '.json';
            $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
            if (count($charge) > 0) {
                if ($charge->recurring_application_charge->status == "pending") {
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "declined" || $charge->recurring_application_charge->status == "expired") {
                    //creating the new Recuring charge after declined app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'recurring_application_charge' => array(
                                'name' => 'Easy Donation',
                                'price' => 4.99,
                                'return_url' => url('payment_success'),
                                'capped_amount' => 20,
                                'terms' => 'Easy donation Advance feature also avaliable',
                            //'test' => true
                            )
                        )
                            ], false);
                    $create_charge = ShopModel::where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);
                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "accepted") {
                    $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
                    $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
                    $Activatecharge_array = get_object_vars($Activate_charge);
                    $active_status = $Activatecharge_array['recurring_application_charge']->status;
                    $update_charge_status = ShopModel::where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
                    
                    if ($select_store->app_version > 0 && $select_store->type_of_app > 0) {
                        session(['shop' => $shop]);
                        return redirect()->route('dashboard', ['shop' => $shop]);
                    } else {

                        return redirect()->route('app_version', ['shop' => $shop]);
                    }
                }
            }
        }
    }

    public function payment_compelete(Request $request) {
        $app_settings = AppSettingModel::where('id', 1)->first();
        $shop = session('shop');
        $select_store = ShopModel::where('store_name', $shop)->first();              
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
        $charge_id = $_GET['charge_id'];
        $url = 'admin/recurring_application_charges/#{' . $charge_id . '}.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET',]);
        $status = $charge->recurring_application_charges[0]->status;
        $update_charge_status = ShopModel::where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $status]);
        if ($status == "accepted") {
            $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
            $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
            $Activatecharge_array = get_object_vars($Activate_charge);
            $active_status = $Activatecharge_array['recurring_application_charge']->status;
            $trial_start = $Activatecharge_array['recurring_application_charge']->activated_on;
            $trial_end = $Activatecharge_array['recurring_application_charge']->trial_ends_on;
            $trial_days = $Activatecharge_array['recurring_application_charge']->trial_days;
            $update_charge_status = ShopModel::where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status, 'activated_on' => $trial_start, 'trial_ends_on' => $trial_end]);
            //return redirect()->route('dashboardpro');
            //check if any trial info is exists or not
            if ($trial_days > 0) {
                $check_trial = TrialInfo::where('store_name', $shop)->first();
                if (count($check_trial) > 0) {
                    TrialInfo::where('store_name', $shop)->update(['trial_days' => $trial_days, 'activated_on' => $trial_start, 'trial_ends_on' => $trial_end]);
                } else {
                    TrialInfo::insert([
                        'store_name' => $shop,
                        'trial_days' => $trial_days,
                        'activated_on' => $trial_start,
                        'trial_ends_on' => $trial_end
                    ]);
                }
            }


            if ($select_store->app_version > 0) {
                session(['shop' => $shop]);
                return redirect()->route('dashboard', ['shop' => $shop]);
            } else {
                return redirect()->route('app_version', ['shop' => $shop]);
            }
        } elseif ($status == "declined") {
            echo '<script>window.top.location.href="https://' . $shop . '/admin/apps"</script>';
        }
    }

    public function app_version(Request $request) {
        $shop = session('shop');
        return view('app-version', ['shop' => $shop]);
    }

    public function usage_charge(Request $request, $plan_id) {
        //for creating usage-charges for version
        $shop = session('shop');
        if (!isset($shop)) {
            $shop = $request->input('shop');
        }

        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettingModel::where('id', 1)->first();
        $accessToken = $shop_find->access_token;
        $shop_id = $shop_find->id;
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $id = $shop_find->charge_id;
        $url = 'admin/recurring_application_charges/' . $id . '.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
        $recuring_id = (string) $charge->recurring_application_charge->id;
        $chargearray = (array) $charge->recurring_application_charge;
        if ($plan_id == 1) {
            $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['usage_price' => '0.00', 'app_version' => 1, 'type_of_app' => 1]);
            $product_argument = [
                'product' => [
                    'title' => 'Donate',
                    'body_html' => 'Donation product!',
                    'vendor' => 'zestard-easy-donation',
                    'product_type' => 'Donation',
                    'images' => array(
                        '0' => array(
                            'src' => config('app.url') . 'public/image/product/Donate.png'
                        )
                    ),
                    'variants' => array(
                        '0' => array(
                            'option1' => 'Donate',
                            'price' => '00.00',
                            'taxable' => 'false',
                            'requires_shipping' => 'false'
                        )
                    )
                ]
            ];
            //api call for product
            $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
            $product_id = $product->product->id;
            $variant_id = $product->product->variants[0]->id;
            //DB::statement("UPDATE usersettings SET product_id = $product_id WHERE id = $shop_find->id");            
            ShopModel::where('id', $shop_find->id)->update(['product_id' => $product_id]);
            //webhook for checkout
            $url = 'https://' . $shop . '/admin/webhooks.json';
            $webhookData = [
                'webhook' => [
                    'topic' => 'orders/create',
                    'address' => config('app.url') . 'checkout.php',
                    'format' => 'json'
                ]
            ];
            $checkout = $sh->appUninstallHook($accessToken, $url, $webhookData);
            $email_content = '<html><head><meta charset="utf-8"/>
                                    <style>.receipt-css {border: 1px solid black; padding: 20px; display: inline-block;}.title-left{float:left;text-align:left;}</style>
                                    <title>Easy Donation</title>
                                    </head>
                                    <body class="receipt-css"><div class="receipt-css">
                                    <header><h1 class="heading">Donation Receipt</h1></header>                                                
                                        <table class="donation-receipt">
                                            <tr><th class="title-left">First Name:</th><td>{{first_name}}</td></tr>
                                            <tr><th class="title-left">Last Name:</th><td>{{last_name}}</td></tr>
                                            <tr><th class="title-left">Email:</th><td>{{email}}</td></tr>
                                            <tr><th class="title-left">Date:</th><td>{{date}}</td></tr>
                                            <tr><th class="title-left">Donation Name:</th><td>{{donation_name}}</td></tr>
                                            <tr><th class="title-left">Donate Price:</th><td>{{price}}</td></tr>
                                            <tr><th class="title-left">Currency:</th><td>{{currency}}</td></tr>
                                        </table>
                                        <h1>Thanks For Donating!</h1>
                                    </div></body>
                                </html>';
            //api call for get store info
            $store_details = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
            $adminEmail = new EmailConfig;
            $adminEmail->html_content = $email_content;
            $adminEmail->shop_id = $shop_id;
            $adminEmail->admin_email = $store_details->shop->email;
            $adminEmail->cc_email = $store_details->shop->email;
            $adminEmail->save();
            return redirect()->route('dashboard', ['shop' => $shop]);
        } else if ($plan_id == 2) {            
            $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
            if ($shop == "zankar-test.myshopify.com" || $shop == "all-free-theme-test.myshopify.com" || $shop == "order-additional-fields.myshopify.com" || $shop == "mobile-app-demo.myshopify.com" || $shop == "article-additional-fields.myshopify.com" || $shop == "collection-additional-fields.myshopify.com" || $shop == "broadcast-bar.myshopify.com" || $shop == "ishita-test.myshopify.com" || $shop == "vijay-test.myshopify.com" || $shop == "free-theme-test.myshopify.com" || $shop == "zankar-test.myshopify.com" || $shop == "find-test.myshopify.com" || $shop == "dipal-test.myshopify.com" || $shop == "new-easy-donation-demo.myshopify.com") {                
                $plan_argument = [
                    'usage_charge' => [
                        'description' => 'Easy Donation Pro',
                        'price' => 0.01,
                        'test' => true
                    ]
                ];
            } else {
                $plan_argument = [
                    'usage_charge' => [
                        'description' => 'Easy Donation Pro',
                        'price' => 3.00,
                        //'test' => true
                    ]
                ];
            }
            $plan2 = $sh->call(['URL' => $url, 'METHOD' => 'POST', 'DATA' => $plan_argument]);
            $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['usage_charge_id' => $plan2->usage_charge->id, 'usage_price' => $plan2->usage_charge->price, 'app_version' => 2, 'type_of_app' => 2]);
            //webhook for checkout
            $url = 'https://' . $shop . '/admin/webhooks.json';
            $webhookData = [
                'webhook' => [
                    'topic' => 'orders/create',
                    'address' => config('app.url') . 'checkoutpro.php',
                    'format' => 'json'
                ]
            ];
            $checkout = $sh->appUninstallHook($accessToken, $url, $webhookData);
            $email_content = '<html><head><meta charset="utf-8"/>
                                    <title>Easy Donation</title>
                                    <style type="text/css">
                                    h1,h2,h3,h4,h5,h6,p,body{margin: 0;}
                                        a{font-weight: 600;font-size: 14px;}                                               
                                    </style>
                                    </head>
                                    <body style="font-family: Open Sans, sans-serif; font-weight: 700;">
                                        <div style="padding: 15px;background-color:#cdcdcd;float:left;margin-left: 20px;margin-top: 20px;">
                                            <div style="padding: 30px;background-color: #fff;" >
                                            <div><h1 style="font-size: 36px;color: #373634;">Donation Receipt</h1></div>                                                
                                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td style="font-size: 14px;text-transform: capitalize;color: #515150;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;">First Name:</td>
                                                            <td style="color: #515150;font-weight: 600;font-size: 14px;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;"><span>{{first_name}}<span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 14px;text-transform: capitalize;color: #515150;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;">Last Name:</td>
                                                            <td style="color: #515150;font-weight: 600;font-size: 14px;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;"><span>{{last_name}}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 14px;text-transform: capitalize;color: #515150;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;">Email:</td>
                                                            <td style="color: #515150;font-weight: 600;font-size: 14px;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;">{{email}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 14px;text-transform: capitalize;color: #515150;padding-bottom: 5px;padding-top: 20px; border-bottom: 1px solid #b5b5b5;">Date:</td>
                                                            <td style="color: #515150;font-weight: 600;font-size: 14px;padding-bottom: 5px;padding-top: 20px;border-bottom: 1px solid #b5b5b5;"><span>{{date}}</span></td>
                                                        </tr>
                                                    </tbody>                                                
                                                </table>
                                                {{donation_details}}
                                                <div><h1 style="font-size: 32px;color: #373634;">Thanks For Donating!</h1></div>
                                            </div>
                                        </div>
                                    </body>
                                </html>';

            //api call for get store info
            $store_details = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
            $adminEmail = new EmailConfig;
            $adminEmail->html_content = $email_content;
            $adminEmail->shop_id = $shop_id;
            $adminEmail->admin_email = $store_details->shop->email;
            $adminEmail->cc_email = $store_details->shop->email;
            $adminEmail->save();
            return redirect()->route('dashboard', ['shop' => $shop]);            
        } else {
            
        }
    }

    public function update_modal_status(Request $request) {
        $shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_find->new_install = 'N';
        $shop_find->save();
    }

    public function basic(Request $request) {
        return view('plan_confirm', ['plan' => $request->input('one'), 'shop' => $request->input('shop')]);
    }

    public function advance(Request $request) {
        return view('plan_confirm', ['plan' => $request->input('two'), 'shop' => $request->input('shop')]);
    }

}
