<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopModel;
use App\Symbol;
use App\DonationSettings;
use App;
use DB;
use App\AppSettingModel;
use App\EmailConfig;
use App\TrialInfo;
use App\DevelopmentStores;

class webhookController extends Controller {

    public function webhook(Request $request) {        
        $shop_name = session('shop');
        $shop_name = "hta-atlanta.myshopify.com";
        $shop_find = ShopModel::where('store_name', $shop_name)->first();
        $product_id = $shop_find->product_id;
        $app_settings = AppSettingModel::where('id', 1)->first();
        $shop_id = $shop_find->id;
        $donation_encrypt = crypt($shop_id, "ze");
        $finaly_encrypt = str_replace(['/', '.'], "Z", $donation_encrypt);
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $shop_find->access_token]);
        dd($sh->call(['URL' => '/admin/recurring_application_charges.json', 'METHOD' => 'GET']));
        dd($sh->call(['URL' => '/admin/script_tags.json', 'METHOD' => 'GET']));
        $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
        foreach ($theme->themes as $themeData) {
            if ($themeData->role == 'main') {
                $snippets_arguments = ['id' => $finaly_encrypt];
                $theme_id = $themeData->id;
                $view = (string) View('snippets_new', $snippets_arguments);
                //api call for creating snippets
                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/donation.liquid', 'value' => $view]]]);

                $hide_product = (string) View('hide_product', $snippets_arguments);
                //api call for creating snippets
                dd($call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/hide_product.liquid', 'value' => $hide_product]]]));
            }
        }
        $orders = $sh->call(['URL' => '/admin/orders.json?status=any&', 'METHOD' => 'GET']);
        $orderCollections = array();
        $count = $sh->call(['URL' => '/admin/orders/count.json?status=any', 'METHOD' => 'GET']);
        $products_count = (array) $count;
        $product_count = $products_count['count'];

        $pages = ceil($product_count / 250);
        $limit = 250;
        for ($i = 1; $i <= $pages; $i++) {
            $orders = (array) $sh->call(['URL' => '/admin/orders.json?status=any&limit=' . $limit . '&page=' . $i, 'METHOD' => 'GET']);
            array_push($orderCollections, $orders);
        }
        $i = 0;
        foreach ($orderCollections as $all_orders) {

            foreach ($all_orders['orders'] as $orderData) {

                foreach ($orderData->line_items as $productData) {
                    if (strpos($productData->name, "Donate To Sloan Kettering - Cancer Research And Treatment Center") !== false) {
                        echo $i++ . " " . $orderData->name . " <br> ";
                    }
                }
            }
        }
        die;
        $charge_data = [
            "application_charge" => [
                "name" => "Super Duper Expensive action",
                "price" => 90.00,
                "return_url" => url('payment_success'),
                "test" => true
            ]
        ];
        $application_charge = $sh->call(['URL' => '/admin/application_charges.json', 'METHOD' => 'POST', 'DATA' => $charge_data]);
        echo '<script>window.top.location.href="' . $application_charge->application_charge->confirmation_url . '"</script>';
    }

    public function create_order(Request $request) {
        $app_settings = AppSettingModel::where('id', 1)->first();
        $shop = session('shop');
        if (empty($shop)) {
            $shop = $_GET['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $donation_products = array();
        $temp_donation_product = array();
        $dropdown_donation_amounts = $request->input('dropdown_donation_amounts');
        $textfield_donation_amounts = $request->input('textfield_donation_amounts');
        $pricebar_donation_amounts = $request->input('pricebar_donation_amounts');
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $textfield_donation = $request->input('textfield_donations');
        $pricebar_donation = $request->input('pricebar_donations');
        if (count($textfield_donation_amounts) > 0) {
            $i = 0;
            foreach ($textfield_donation_amounts as $textfield_amount) {
                $product_id = $textfield_donation[$i];
                $amount = $textfield_amount;
                $product_variant = $sh->call(['URL' => '/admin/products/' . $product_id . '/variants.json', 'METHOD' => 'GET']);
                $product_variant_id = $product_variant->variants[0]->id;

                $variant_argument = [
                    'variant' => [
                        'price' => $amount
                    ]
                ];
                $variant = $sh->call(['URL' => '/admin/variants/' . $product_variant_id . '.json', 'METHOD' => 'PUT', 'DATA' => $variant_argument]);

                $temp_donation_product['variant_id'] = $product_variant_id;
                $temp_donation_product['quantity'] = 1;
                array_push($donation_products, $temp_donation_product);
            }
        }
        if (count($pricebar_donation_amounts) > 0) {
            $i = 0;
            foreach ($pricebar_donation_amounts as $pricebar_amount) {
                $product_id = $pricebar_donation[$i];
                $amount = $pricebar_amount;
                $product_variant = $sh->call(['URL' => '/admin/products/' . $product_id . '/variants.json', 'METHOD' => 'GET']);
                $product_variant_id = $product_variant->variants[0]->id;

                $variant_argument = [
                    'variant' => [
                        'price' => $amount
                    ]
                ];
                $variant = $sh->call(['URL' => '/admin/variants/' . $product_variant_id . '.json', 'METHOD' => 'PUT', 'DATA' => $variant_argument]);

                $temp_donation_product['variant_id'] = $product_variant_id;
                $temp_donation_product['quantity'] = 1;
                array_push($donation_products, $temp_donation_product);
            }
        }
        if (count($dropdown_donation_amounts) > 0) {
            foreach ($dropdown_donation_amounts as $dropdown_amount) {
                $temp_donation_product['variant_id'] = $dropdown_amount;
                $temp_donation_product['quantity'] = 1;
                array_push($donation_products, $temp_donation_product);
            }
        }
        $draft_order = [
            "draft_order" => [
                "line_items" => $donation_products
            ]
        ];
        dd($sh->call(["URL" => "/admin/draft_orders.json", "METHOD" => "POST", "DATA" => $draft_order]));
    }

    public function draft_order(Request $request) {
        $shop = session('shop');
        if (empty($shop)) {
            $shop = $_GET['shop'];
        }
        $shopData = ShopModel::select('store_encrypt', 'id', 'app_version', 'store_name', 'access_token', 'product_id')->where('store_name', $shop)->first();
        $shop = (array) $shopData;
        $shop_id = $shop['id'];
        $id = $shop['store_encrypt'];
        $statusdata = DonationSettings::select('status')->where('shop_id', $shop_id)->first();
        $status = (array) $statusdata;
        //If settings exists for particular store then return view
        if (count($status) > 0) {
            $app_settings = AppSettingModel::where('id', 1)->first();
            $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shopData->store_name, 'ACCESS_TOKEN' => $shopData->access_token]);
            $shop_api = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
            $currency = Symbol::where('currency_code', $shop_api->shop->currency)->first();
            $shop_id = $shopData->id;
            $donation_config = DonationSettings::where('shop_id', $shop_id)->first();
            $donation_config->shop_currency = $currency->symbol_html;
            $donation_config->save();
            if ($shop['app_version'] == 2) {
                $donation_settings_array = array();
                $donation_settings_array['status'] = $donation_config->status;
                $donation_settings_array['title'] = $donation_config->title;
                $donation_settings_array['donation_button_text'] = $donation_config->donation_button_text;
                $donation_settings_array['additional_css'] = $donation_config->additional_css;
                $donation_settings_array['variant_ids'] = $donation_config->variant_ids;
                $donation_settings_array['donation_data'] = unserialize(base64_decode($donation_config->donation_data));
                $donation_settings_array['shop_currency'] = ($donation_config->shop_currency);
                $donation_data_length = count($donation_settings_array['donation_data']);
                $temp_donation_data = ($donation_settings_array['donation_data']);
                $select_page = array();
                foreach ($temp_donation_data as $donation_data) {
                    if (isset($donation_data->select_page) || $donation_data->select_page != null)
                        array_push($select_page, $donation_data->select_page);
                    else
                        array_push($select_page, 1);
                }
                $donationData[] = $donation_settings_array['donation_data'];
                $product_image_url = [];
                $product_ids = [];
                $i = 0;
                foreach ($donationData as $donationCollection) {
                    foreach ($donationCollection as $donation) {
                        array_push($product_ids, $donation->product_id);
                    }
                }
                $products = $sh->call(['URL' => '/admin/products.json?ids=' . implode(",", $product_ids), 'METHOD' => 'GET']);
                foreach ($products as $product) {
                    foreach ($product as $attributes) {
                        $product_verify_id = $attributes->variants[0]->product_id;
                        if (empty($attributes->images)) {
                            $product_image_url[$product_verify_id] = "";
                        } else {
                            $img_src = isset($attributes->images[0]->src) ? $attributes->images[0]->src : "";
                            $product_image_url[$product_verify_id] = $img_src;
                        }
                    }
                }
                return view('draft_order', ['id' => $id, 'images_json' => json_encode($product_image_url), 'donation_settings' => json_encode($donation_settings_array), 'temp_product_ids' => $product_ids]);
            } else {
                //Notify
            }
            return view('draft_order');
        }
    }

}
