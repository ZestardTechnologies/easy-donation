<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'DonationSave','DonationproSave','frontend/dropdown/save','frontend/dropdown/savepro','product/update','track_donation','DonationproProductId','getshopid','getshopidbasic','basic','advance','usage_charge/*','update_email', 'update_emailpro', 'save_html', 'save_htmlpro'
    ];
}
