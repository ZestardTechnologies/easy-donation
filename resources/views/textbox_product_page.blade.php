var front_card = '<div id="textbox_preview_' + i + '"><form name="textboxform" id="textboxform' + i + '">' +
					'<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"/><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
					'<div><label class="ed_minimum_donation minimum_donation_' + i + '" for="minimum_donation" style="disply:none;"></label><input type="number" name="textbox_amount" id="textbox_amount_' + i + '" class="textbox_amount" step="any" min="1" required>' +
					'<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
					'<img src="' + base_path_easydonation + 'image/loader.svg"/></div></div></div></div></form></div>';
					$zestard_easy('#preview_product_container').append(front_card);
					$zestard_easy('#dropdown_preview_' + i).remove();
					$zestard_easy('#pricebar_preview_' + i).remove();
					$zestard_easy('#textbox_preview_' + i).css('display', 'block');

					var text_value = parseInt(text_amount);
					if (product_id) {
						$zestard_easy('.product_id_' + i).val(product_id);
						product_verify_id = $zestard_easy('.product_id_' + i).val();
					}
					if (donation_name) {
						$zestard_easy('.display_title_' + i).text(donation_name);
					} else {
						$zestard_easy('.display_title_' + i).css('display', 'none');
					}
					if (donation_description) {
						$zestard_easy('.display_description_' + i).html(donation_description);
					} else {
						$zestard_easy('.display_description_' + i).css('display', 'none');
					}
					if (add_min_amount >= "1") {						
						$zestard_easy(".minimum_donation_" + i).css('display', 'block');
						$zestard_easy(".minimum_donation_" + i).html("Minimum Donation: " + shop_currency + add_min_amount);
						$zestard_easy("#textbox_amount_" + i).attr("min", add_min_amount);
					} else {
						$zestard_easy(".minimum_donation_" + i).css('display', 'none');
					}
					$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
					$zestard_easy('#textbox_amount_' + i).val(text_value);
					submitTextbox(i);