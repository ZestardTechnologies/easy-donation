@extends('headerpro')
@section('content')


<main class="full-width">
    <header>
        <div class="container">
            <div class="adjust-margin toc-block">
                <h1 class="toc-title">Plan Selection</h1>
                <p class="toc-description"></p>
            </div>
        </div>	
    </header>
    <section>
        <div class="full-width">
            <article>
                @if($plan == 1)
                <div class="column twelve card plan-selection">    
                                
                    <ul>
                        <li>Dear Customer,</li>
                        <li>You Have Selected Basic Plan.</li>
                        <li>Total Payable Amount is $4.99 (Basic Plan)</li>
                    </ul>
                    <hr />
                    <div class="mt-20">                        
                        <div class="columns six">
                            <a href="{{ url('app_version') }}"><button type="button" class="btn btn-primary submit-loader-goback btnBasicGoback"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon-go-back" style="display:none;"></i>Go Back</button></a>
                        </div>
                        <div class="columns six">                            
                            <form action="{{ url('usage_charge/1') }}" method="post" id="BasicformId">				
                                <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                                <input name="shop" type="hidden" value="{{ $shop }}" />
                                <?php /* <input type="submit" value="Go to Dashboard" class="btn btn-info clickPay btnBasic" /> */ ?>
                                 <button class="btn btn-primary submit-loader-dashboard btnBasicDashborad" type="submit" name="save"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon-dashboard" style="display:none;"></i>Go to Dashboard</button>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div id="donation-chart"></div> 
                    </div>
                </div>
                @endif

                @if($plan == 2)
                <div class="column twelve card  plan-selection"> 
                     <ul>
                        <li>Dear Customer,</li>
                        <li>You Have Selected Advance Plan.</li>
                        <li>Total Payable Amount is $7.99 (Basic - $4.99 + Advance - $3.00)</li>
                    </ul>                   
                    
                    <hr />
                    <div class=" mt-20">                        
                        <div class="columns six">                            
                            <a href="{{ url('app_version') }}"><button type="button" class="btn btn-primary submit-loader-goback btnBasicGoback"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon-go-back" style="display:none;"></i>Go Back</button></a>
                        </div>
                        <div class="columns six">
                            <form action="{{ url('usage_charge/2') }}" method="post" id="AdvanceformId">								
                                <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                                <input name="shop" type="hidden" value="{{ $shop }}" />
                                <?php /* <input type="submit" value="Click Here to Pay" class="btn btn-info clickPay btnAdvance" /> */ ?>
                                <button class="btn btn-primary submit-loader-dashboard btnAdvanceDashborad" type="submit" name="save"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon-dashboard" style="display:none;"></i>Go to Dashboard</button>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div id="donation-chart"></div> 
                    </div>
                </div>
                @endif



            </article>	
        </div>
    </section>
    <footer></footer>
</main>

<script>
    $().ready(function() {
        
        $( ".btnBasicGoback" ).click(function() {            
            $(".submit-loader-goback").attr("disabled", "disabled");
            $(".btn-loader-icon-go-back").css({"display": "block", "float": "left", "margin": "3px 7px 0 0"});            
        });
        
        
        $( ".btnBasicDashborad" ).click(function() {           
            $(".submit-loader-dashboard").attr("disabled", "disabled");
            $(".btn-loader-icon-dashboard").css({"display": "block", "float": "left", "margin": "3px 7px 0 0"});
            //return false;
            $("#BasicformId").submit();            
        });
        
        $( ".btnAdvanceDashborad" ).click(function() {                
            $(".submit-loader-dashboard").attr("disabled", "disabled");
            $(".btn-loader-icon-dashboard").css({"display": "block", "float": "left", "margin": "3px 7px 0 0"}); 
            //return false;
            $("#AdvanceformId").submit();            
        });
                
    
    });
</script>

@endsection
