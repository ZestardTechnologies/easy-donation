var front_card = '<div id="dropdown_preview_' + i + '"><form name="dropdownform" id="dropdownform_' + i + '">' +
					'<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
					'<div><select name="donation_amount" id="donation_amount_' + i + '" class="donationamount"></select><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/>' +
					'<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
					'<img src="' + base_path_easydonation + 'image/loader.svg"/></div></div></div></div></form></div>';
					$zestard_easy('#preview_product_container').append(front_card);
					$zestard_easy('#pricebar_preview_' + i).remove();
					$zestard_easy('#textbox_preview_' + i).remove();
					$zestard_easy('#dropdown_preview_' + i).css('display', 'block');
					$zestard_easy('#dropdown_range_' + i).css('display', 'block');
					var option_array = JSON.parse(drop_down);							
					if (product_id) {
						$zestard_easy('.product_id_' + i).val(product_id);
						product_verify_id = $zestard_easy('.product_id_' + i).val();
					}
					if (donation_name) {
						$zestard_easy('.display_title_' + i).text(donation_name);
					} else {
						$zestard_easy('.display_title_' + i).css('display', 'none');
					}
					if (donation_description) {
						$zestard_easy('.display_description_' + i).html(donation_description);
					} else {
						$zestard_easy('.display_description_' + i).css('display', 'none');
					}
					$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
					var j = 0;
					if (setdata.variant_ids)
					{
						product_variant_ids = JSON.parse(setdata.variant_ids);
						variant_ids = product_variant_ids[product_id];
						$zestard_easy.map(variant_ids, function (value, index) {
							$zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
								value: value,
								text: option_array[index]
							}));
							j++;
						});
						if(dropdown_other == 1){
							$zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
							   id: "click_drop_down",
							   value: "other",
							   text: text_dropdown_other
						   }));
						}
					} 
					else
					{
						$zestard_easy.each(option_array, function (j) {
							$zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
								value: option_array[j],
								text: option_array[j]
							}));
							j++;
						});
						if(dropdown_other == 1){
							$zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
							   id: "click_drop_down",
							   value: "other",
							   text: text_dropdown_other
						   }));
						}
					}
					submitDropdown(i);