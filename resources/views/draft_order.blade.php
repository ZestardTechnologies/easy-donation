<?php
$donation_settings_array = json_decode($donation_settings, true);
$donation_settings_object = ($donation_settings_array['donation_data']);
$count_donation  = 0;
?>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="{{ asset('js/latest_jquery.min.js') }}"></script>
	<script src="{{ asset('js/latest_jquery-ui.js') }}" type="text/javascript"></script>
	<link href="{{ asset('css/latest_jquery-ui.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/frontpreviewpro.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>	
	<form action="create-order" method="POST">	
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div id="preview_container" class="fontpreview" style="">
			<div class="donation_title" style=""><h3 class="easy_donation_title"></h3></div>
		</div>
		<div class="easy_donation_additional_css"></div>
		<input type="hidden" id="count"/>
		<input type="submit" id="submit"/>			
	</form>
</body>	
<script>	
	var donation_min, donation_max;
	var base_path_easydonation = "https://zestardshop.com/shopifyapp/easy_donation/public/";
    var product_variant_ids;
    var shop_name = "{{ session('shop') }}";
    var shop_id = "{{ $id }}";			
	var temp_product_id = [];     
	function accordion() {
		var acc = document.getElementsByClassName("accordion");
		var i;
		for (i = 0; i < acc.length; i++) {
			acc[i].addEventListener("click", function () {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.display === "block") {
					panel.style.display = "none";
				} else {
					panel.style.display = "block";
				}
			});
		}
	}
	function pricebarAmount(i) 
	{
        $('#pricebar_slider_' + i).change(function () {
            var amounttext = $(this).val();
            var parent = $(this).parents('#pricebarform_' + i);
            var child = parent.find('#amount-label_' + i);
            child.html(setdata.shop_currency + amounttext);            
        });
    }
    function submitDropdown(i) 
	{
        $("#dropdownform_" + i).submit(function (e) {            
        var data = $("#donation_amount_" + i).val();
        if(data != "other")
		{
            /* $.ajax({
                url: '/cart/add.js',
                data: {quantity: 1, id: data},
                async: false,
				success: function()
				{
						$(".loaderDiv_" + i).hide();
						$(".overlay").css({
							'display': 'inline-block',
							'visibility': 'visible',
							'opacity': '1'
						});
						$(".donate-close").click(function () {
							$(".overlay").css({
								'display': 'none'
							});
							reloadcart()
						});
				}
				
            }); */
            //location.reload();           
            e.preventDefault();
        }
		else
		{
            var price = $("#dropdown_textbox_" + i).val();
            var product_id = $(".product_id_" + i).val();
            $.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/savepro') }}",
                data: {
                    'price': price,
                    'product_id': product_id,
                    'id': shop_id
                },
                beforeSend: function () {
                    $('.loaderDiv_' + i).css('display', 'inline-block');
                },
                success: function (productId) {					
					/* $.ajax({
                        url: '/cart/change.js',
                        data: {quantity: 0, id: parseInt(productId)},
                        async: false
                    }); */
                    $(".loaderDiv_" + i).hide();
                    $(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });					
                    $(".donate-close").click(function () {
                        $(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });					 
                    //$.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
                }
            });
            e.preventDefault();
        }
        });
    }
    function reloadcart() {
        location.reload();
        //window.location.href = "/cart";
    }
    function submitPricesbar(i) 
	{
        $("#pricebarform_" + i).submit(function (e) {
            var price = $("#pricebar_slider_" + i).val();
            var product_id = $(".product_id_" + i).val();
            $.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/savepro') }}",
                data: {
                    'price': price,
                    'product_id': product_id,
                    'id': shop_id
                },
                beforeSend: function () {
                    $('.loaderDiv_' + i).css('display', 'inline-block');
                },
                success: function (productId) {
					/* $.ajax({
                        url: '/cart/change.js',
                        data: {quantity: 0, id: parseInt(productId)},
                        async: false
                    }); */
                    $(".loaderDiv_" + i).hide();
                    $(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $(".donate-close").click(function () {
                        $(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart();
                    });
                    //$.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
                }
            });
            e.preventDefault();
        });
    }
    function submitTextbox(i) 
	{
        $("#textboxform" + i).submit(function (e) {
            var price = $("#textbox_amount_" + i).val();
            var product_id = $(".product_id_" + i).val();
            $.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/savepro') }}",
                data: {
                    'price': price,
                    'product_id': product_id,
                    'id': shop_id
                },
                beforeSend: function () {
                    $('.loaderDiv_' + i).css('display', 'inline-block');
                },
                success: function (productId) {
					/* $.ajax({
                        url: '/cart/change.js',
                        data: {quantity: 0, id: parseInt(productId)},
                        async: false
                    }); */
                    $(".loaderDiv_" + i).hide();
                    $(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $(".donate-close").click(function () {
                        $(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                    //$.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
                }
            });
            e.preventDefault();
        });
    }
</script>
<div id="donate-popup" class="overlay" style="display: none" align = "center">
    <div class="popup">
        <h2>Thanks For Donating!</h2>
        <a class="donate-close" href="#">&times;</a>        
    </div>
</div>
<script>     		
	setdata = <?php echo $donation_settings ?>;	
	if(setdata) 
	{                 
		var shop = setdata.shop_id;
		{
			product_image = <?php echo $images_json ?>;
		}                    
	}	
	var i = 0;
	var countCard = setdata.donation_data.length;
	donation_settings_object = setdata.donation_data;
	$(document).ready(function(){
		$('.donationamount').on('change', function(){  		
			var dropdown_value = $(this).val();
			if(dropdown_value == "other"){                                
				$(this).next().css('display', 'inline-block');
			}
			else
			{
				$(this).next().css('display', 'none');
			}
		});			
	});
	
	for(var i = 0; i < countCard; i++) 
	{					
		var donation_title = setdata.title;
		var donation_button_text = setdata.donation_button_text;
		var additional_css = setdata.additional_css;
		var donation_name = setdata.donation_data[i].donation_name;
		var donation_description = setdata.donation_data[i].donation_description;
		var field_option = setdata.donation_data[i].field_option;
		var drop_down = setdata.donation_data[i].drop_down;
		var dropdown_other = setdata.donation_data[i].dropdown_other;
		var text_dropdown_other = setdata.donation_data[i].text_dropdown_other;
		var text_amount = setdata.donation_data[i].text_amount;
		var add_min_amount = setdata.donation_data[i].add_min_amount;			
		donation_min = setdata.donation_data[i].donation_min;
		donation_max = setdata.donation_data[i].donation_max;	
		var product_id = setdata.donation_data[i].product_id;		
		var shop_currency = setdata.shop_currency;
		var product_verify_id = "";   					
		
		$('.easy_donation_additional_css').html("<style>" + additional_css + "</style>");																				
			$('.donationamount').on('change', function(){  
				var dropdown_value = $(this).val();
				if(dropdown_value == "other"){                                
					$(this).next().css('display', 'inline-block');
				}
				else
				{
					$(this).next().css('display', 'none');
				}
			});
			@if(($donation_settings_array) > 0) 
			{																						
				if(donation_title) 
				{
					$('#preview_container .easy_donation_title').html("<b>" + donation_title + "</b>");
					$('#preview_container .donation_title').css('display', 'block');                            				
				}
				if(donation_button_text) 
				{
					$(".frontbutton").attr("value", setdata.donation_button_text);
				} 
				else 
				{
					$(".frontbutton").attr("value", "Donate");
				}				
				if(donation_settings_object[i].field_option == "P")
				{											
					@include('range_input_cart_page')
				} 				
				else if(donation_settings_object[i].field_option == "D")	
				{		
					@include('dropdown_cart_page')					
				} 								
				else if(donation_settings_object[i].field_option == "T")						
				{																			
					@include('textbox_cart_page')										
				} 
				else 
				{
					$('#bar_range_' + i).css('display', 'none');
					$('#text_range_' + i).css('display', 'none');
					$('#dropdown_range_' + i).css('display', 'none');
				}
				$('#default_preview_' + i).css('display', 'none');
				$('#data_preview_' + i).css('display', 'block');
			} 
			@else 
			{				
				$('#bar_range_' + i).css('display', 'none');
				$('#text_range_' + i).css('display', 'none');
				$('#dropdown_range_' + i).css('display', 'none');
				$('#default_preview_' + i).css('display', 'block');
				$('#data_preview_' + i).css('display', 'none');
			}	
			@endif	
		}												
	accordion();	
</script>
</html>