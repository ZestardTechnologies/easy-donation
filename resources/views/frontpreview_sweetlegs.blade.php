<!--script type="text/javascript" src="{{ asset('js/latest_jquery.min.js') }}"></script-->
<script src="{{ asset('js/latest_jquery-ui.js') }}" type="text/javascript"></script>
<link href="{{ asset('css/latest_jquery-ui.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/frontpreview_sweetlegs.css') }}" rel="stylesheet" type="text/css" />
<script>
var base_path_easydonation = "https://shopifydev.anujdalal.com/easy_donation_optimization/public/";
var variant_ids, product_id;
var shop_name = Shopify.Checkout.apiHost;
var shop_id = "{{ $id }}";
if(shop_name == undefined || typeof shop_name == undefined)
{
	
}
console.log(shop_name);
$(".fontpreview").css("box-shadow", "none");
$("#donation_amount").css("webkit-appearance", "menulist");
$("#donation_amount").css("border", "1px solid #d5d5d5");
$("#dropdown_textbox").css("border", "1px solid #d5d5d5");
$("#textbox_amount").css("border", "1px solid #d5d5d5");
$("#pricebar_slider").css("webkit-appearance", "slider-horizontal");
$zestard_easy(document).ready(function () {
    var shop_id = '<?php echo $id; ?>';
    var page = '<?php echo $page; ?>';
    var additional_css = "";
    $zestard_easy.ajax({
        type: "GET",
        url: base_path_easydonation + "front_preview",
        data: {
            id: shop_id,
            shop_name: shop_name
        },
        success: function (data) {            
            setdata = JSON.parse(data);
            if (setdata) {
                var text_dropdown_other = setdata.text_dropdown_other
                additional_css = setdata.additional_css;
                $zestard_easy('.easy_donation_additional_css').html("<style>" + additional_css + "</style>");
                if (setdata.select_page == 2 && page == "product" || setdata.select_page == 3 && page == "product" ) {
/*                     if($("[itemprop=offers]"))
					{
                        $("[itemprop=offers]").css("display", "none");
                    }
					else
					{
                        $("[itemprop=price]").css("display", "none");

                        var button = jQuery('button[type=submit][name=add]');
                        if (button.length == 0)
                        {
                            button = jQuery('input[type=submit][name=add]');

                        }
                        if (button.length == 0)
                        {
                            var f = jQuery('form[action="/cart/add"]');
                            button = jQuery(f).find(':submit');
                        }
                        $(button).css("display", "none");
                    } */
                    
                     if (setdata.field_option == "P") {
                        $zestard_easy('#preview_product_container').css('display', 'inline-block');
                        $zestard_easy("#textbox_product_preview").remove();
                        $zestard_easy("#dropdown_product_preview").remove();
                        $zestard_easy('#pricebar_product_preview').css('display', 'block');
                        
                        var min_val = parseInt(setdata.bar_min);
                        var max_val = parseInt(setdata.bar_max);
                        
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_product_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_product_button").attr("value", "Donate");
                        }
                        $zestard_easy("#product_pricebar_slider").val(min_val);
                        $zestard_easy("#product_pricebar_slider").attr("min", min_val);
                        $zestard_easy("#product_pricebar_slider").attr("max", max_val);
                        $zestard_easy("#amount-product-label").html(setdata.shop_currency + min_val);
                        $zestard_easy("#product_pricebar_slider").change(function () {
                            var amounttext = $zestard_easy("#product_pricebar_slider").val();
                            $zestard_easy("#amount-product-label").html(setdata.shop_currency + amounttext);
                        });
                    } else if (setdata.field_option == "D") {
                        $zestard_easy('#preview_product_container').css('display', 'inline-block');
                        $zestard_easy("#textbox_product_preview").remove();
                        $zestard_easy("#pricebar_product_preview").remove();
                        $zestard_easy('#dropdown_product_preview').css('display', 'block');
                        var option_array = JSON.parse(setdata.dropdown_option);
                        
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_product_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_product_button").attr("value", "Donate");
                        }
                        var j = 0;                        
                        if (setdata.variant_ids)
                        {
                            variant_ids = JSON.parse(setdata.variant_ids);
                            $zestard_easy.each(option_array, function (j) {
                                $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                    value: variant_ids[j],
                                    text: option_array[j]
                                }));
                                j++;
                            });
                            if(setdata.dropdown_other == 1){
                                 $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                    id: "click_drop_down",
                                    value: "other",
                                    text: text_dropdown_other
                                }));
                            }
                        } else
                        {
                            $zestard_easy.each(option_array, function (j) {
                                $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                    value: option_array[j],
                                    text: option_array[j]
                                }));
                                j++;
                            });
                            if(setdata.dropdown_other == 1){
                                 $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                    id: "click_drop_down",
                                    value: "other",
                                    text: text_dropdown_other
                                }));
                            }
                        }
                    } else if (setdata.field_option == "T") {
                        $zestard_easy('#preview_product_container').css('display', 'inline-block');
                        $zestard_easy("#pricebar_product_preview").remove();
                        $zestard_easy("#dropdown_product_preview").remove();
                        $zestard_easy('#textbox_product_preview').css('display', 'block');
                        if(setdata.add_min_amount >= 1){
                            //$zestard_easy("#minimum_product_donation").after(setdata.shop_currency + setdata.add_min_amount);
                            $zestard_easy('#minimum_product_donation').css('display', 'block');
                            $zestard_easy("#minimum_product_donation").html("Minimum Donation: " + setdata.shop_currency + setdata.add_min_amount);
                            $zestard_easy("#textbox_product_amount").attr("min", setdata.add_min_amount);                            
                        }else{
                            $zestard_easy('#minimum_product_donation').css('display', 'none');
                        }
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_product_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_product_button").attr("value", "Donate");
                        }
                    } else {
                    }
                }
                else if(setdata.select_page == 1 && page == "cart" || setdata.select_page == 3 && page == "cart" || page == "checkout") {
                    var shop = setdata.shop_id;
                    //for getting the product image
                    $zestard_easy.ajax({
                        type: "GET",
                        url: "{{ url('productimage') }}",
                        async: false,
                        data: {
                            'id': shop
                        },
                        success: function (product) {
                            //alert(product);
                            product_image = JSON.parse(product);
                            //alert(product_image);
                        }
                    });
                    //console.log(setdata);
                    $zestard_easy('.product-image').html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image}));
                    if (setdata.field_option == "P") {
                        $zestard_easy('#preview_container').css('display', 'inline-block');
                        $zestard_easy("#textbox_preview").remove();
                        $zestard_easy("#dropdown_preview").remove();
                        $zestard_easy('#pricebar_preview').css('display', 'block');
                        
                        var min_val = parseInt(setdata.bar_min);
                        var max_val = parseInt(setdata.bar_max);
                        if (setdata.donation_name) {
                            $zestard_easy(".display_title").text(setdata.donation_name);
                        } else {
                            $zestard_easy(".display_title").css('display', 'none');
                        }
                        if (setdata.description) {
                            $zestard_easy(".display_description").html(setdata.description);
                        } else {
                            $zestard_easy(".display_description").css('display', 'none');
                        }
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_button").attr("value", "Donate");
                        }
                        $zestard_easy("#pricebar_slider").val(min_val);
                        $zestard_easy("#pricebar_slider").attr("min", min_val);
                        $zestard_easy("#pricebar_slider").attr("max", max_val);
                        $zestard_easy("#amount-label").html(setdata.shop_currency + min_val);
                        $zestard_easy("#pricebar_slider").change(function () {
                            var amounttext = $zestard_easy("#pricebar_slider").val();
                            $zestard_easy("#amount-label").html(setdata.shop_currency + amounttext);
                        });
                    } else if (setdata.field_option == "D") {
                        $zestard_easy('#preview_container').css('display', 'inline-block');
                        $zestard_easy("#textbox_preview").remove();
                        $zestard_easy("#pricebar_preview").remove();
                        $zestard_easy('#dropdown_preview').css('display', 'block');
                        var option_array = JSON.parse(setdata.dropdown_option);
                        if (setdata.donation_name) {
                            $zestard_easy(".display_title").text(setdata.donation_name);
                        } else {
                            $zestard_easy(".display_title").css('display', 'none');
                        }
                        if (setdata.description) {
                            $zestard_easy(".display_description").html(setdata.description);
                        } else {
                            $zestard_easy(".display_description").css('display', 'none');
                        }
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_button").attr("value", "Donate");
                        }
                        var j = 0;
                        if (setdata.variant_ids)
                        {
                            variant_ids = JSON.parse(setdata.variant_ids);
                            $zestard_easy.each(option_array, function (j) {
                                $zestard_easy('#donation_amount').append($zestard_easy('<option>', {
                                    value: variant_ids[j],
                                    text: option_array[j]
                                }));
                                j++;
                            });
                            if(setdata.dropdown_other == 1){
                                 $zestard_easy('#donation_amount').append($zestard_easy('<option>', {
                                    id: "click_drop_down",
                                    value: "other",
                                    text: text_dropdown_other
                                }));
                            }
                        } else
                        {
                            $zestard_easy.each(option_array, function (j) {
                                $zestard_easy('#donation_amount').append($zestard_easy('<option>', {
                                    value: option_array[j],
                                    text: option_array[j]
                                }));
                                j++;                                
                            });
                            if(setdata.dropdown_other == 1){
                                 $zestard_easy('#donation_amount').append($zestard_easy('<option>', {
                                    id: "click_drop_down",
                                    value: "other",
                                    text: text_dropdown_other
                                }));
                            }
                        }
                    } else if (setdata.field_option == "T") {
                        $zestard_easy('#preview_container').css('display', 'inline-block');
                        $zestard_easy("#pricebar_preview").remove();
                        $zestard_easy("#dropdown_preview").remove();
                        $zestard_easy('#textbox_preview').css('display', 'block');

                        if (setdata.donation_name) {
                            $zestard_easy(".display_title").text(setdata.donation_name);
                        } else {
                            $zestard_easy(".display_title").css('display', 'none');
                        }
                        if (setdata.description) {
                            $zestard_easy(".display_description").html(setdata.description);
                        } else {
                            $zestard_easy(".display_description").css('display', 'none');
                        }                        
                        if(setdata.add_min_amount >= 1){
                            //$zestard_easy("#minimum_donation").after(setdata.shop_currency + setdata.add_min_amount);
                            $zestard_easy("#minimum_donation").css('display', 'block');
                            $zestard_easy("#minimum_donation").html("Minimum Donation: " + setdata.shop_currency + setdata.add_min_amount);
                            $zestard_easy("#textbox_amount").attr("min", setdata.add_min_amount);                            
                        }else{
                             $zestard_easy("#minimum_donation").css('display', 'none');
                        }
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_button").attr("value", "Donate");
                        }
                    } else {
                    }
                }
            } 
        }
    });
    $zestard_easy("#donation_amount").on('change', function(){
        var dropdown_value = $(this).val();
        if(dropdown_value == "other"){
            $zestard_easy('#dropdown_textbox').css('display', 'inline-block');
        }else{
            $zestard_easy('#dropdown_textbox').css('display', 'none');
        }
    });
    $zestard_easy("#donation_product_amount").on('change', function(){
        var dropdown_value = $(this).val();
        if(dropdown_value == "other"){
            $zestard_easy('#dropdown_product_textbox').css('display', 'inline-block');
        }else{
            $zestard_easy('#dropdown_product_textbox').css('display', 'none');
        }
    });
});

function show_loader() {
    $zestard_easy('.loaderDiv').css('display', 'inline-block');
}

function reloadcart() {
    window.location.href = "";
}
$zestard_easy("#product_dropdownform").submit(function (e) {
    var data = $zestard_easy("#donation_product_amount").val();
    if(data != "other"){
        if(variant_ids)
        {
            $zestard_easy.ajax({
                url: '/cart/add.js',
                data: {quantity: 1, id: data},
                async: false
            });
            location.reload();
        } 
		else
        {
            show_loader();
            $zestard_easy.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/save') }}",
                async: false,
                data: {
                    data: data,
                    'id': shop_id
                },
                beforeSend: function () {
                    show_loader();
                },
                success: function (productId)
                {
                    product_id = productId;
                    $zestard_easy.ajax({
                        url: '/cart/change.js',
                        data: {quantity: 0, id: product_id},
                        async: false
                    });  
					$zestard_easy.ajax({
                        url: '/cart/add.js',
                        data: {quantity: 1, id: product_id},
                        async: false
                    });
                }
            });
            location.reload();
        }
    }
	else
	{
        data = $zestard_easy("#dropdown_product_textbox").val();
        show_loader();
            $zestard_easy.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/save') }}",
                async: false,
                data: {
                    data: data,
                    'id': shop_id
                },
                beforeSend: function () {
                    show_loader();
                },
                success: function (productId)
                {
                    product_id = productId;
					$zestard_easy.ajax({
                        url: '/cart/update.js',
                        data: {quantity: 0, id: product_id},
						success:function(result){},
                        async: false
                    });
                    $zestard_easy.ajax({
                        url: '/cart/add.js',
                        data: {quantity: 1, id: product_id},
                        async: false
                    });
                }
            });
            location.reload();
    }
    
    e.preventDefault();
});

$zestard_easy("#product_textbox_form").submit(function (e) {
    var data = $zestard_easy("#textbox_product_amount").val();
    $zestard_easy.ajax({
        type: "POST",
        url: "{{ url('frontend/dropdown/save') }}",
        data: {
            data: data,
            'id': shop_id
        },
        beforeSend: function () {
            $zestard_easy('.loaderDiv').css('display', 'inline-block');
        },
        success: function (productId) {
            $zestard_easy(".loaderDiv").hide();
            $zestard_easy(".overlay").css({
                'display': 'inline-block',
                'visibility': 'visible',
                'opacity': '1'
            });
            $zestard_easy(".donate-close").click(function () {
                $zestard_easy(".overlay").css({
                    'display': 'none'
                });
                reloadcart();
            });
            $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
        }
    });
    e.preventDefault();
});

$zestard_easy("#product_pricebarform").submit(function (e) {
    var data = $zestard_easy("#product_pricebar_slider").val();
    $zestard_easy.ajax({
        type: "POST",
        url: "{{ url('frontend/dropdown/save') }}",
        data: {
            data: data,
            'id': shop_id
        },
        beforeSend: function () {
            $zestard_easy('.loaderDiv').css('display', 'inline-block');
        },
        success: function (productId) {
            $zestard_easy(".loaderDiv").hide();
            $zestard_easy(".overlay").css({
                'display': 'inline-block',
                'visibility': 'visible',
                'opacity': '1'
            });
            $zestard_easy(".donate-close").click(function () {
                $zestard_easy(".overlay").css({
                    'display': 'none'
                });
                reloadcart();
                //location.reload();
            });
            $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
        }
    });
    e.preventDefault();
});

$zestard_easy("#dropdownform").submit(function (e) {
    var data = $zestard_easy("#donation_amount").val();
    if(data != "other"){
        if (variant_ids)
        {
            $zestard_easy.ajax({
                url: '/cart/add.js',
                data: {quantity: 1, id: data},
                async: false
            });
            location.reload();
        } else
        {
            show_loader();
            $zestard_easy.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/save') }}",
                async: false,
                data: {
                    data: data,
                    'id': shop_id
                },
                beforeSend: function () {
                    show_loader();
                },
                success: function (productId)
                {
                    product_id = productId;
					 
                    $zestard_easy.ajax({
                        url: '/cart/add.js',
                        data: {quantity: 1, id: product_id},
                        async: false
                    });
                }
            });
            location.reload();
        }
    }else{
        data = $zestard_easy("#dropdown_textbox").val();
        show_loader();
        $zestard_easy.ajax({
            type: "POST",
            url: "{{ url('frontend/dropdown/save') }}",
            async: false,
            data: {
                data: data,
                'id': shop_id
            },
            beforeSend: function () {
                show_loader();
            },
            success: function (productId)
            {
                product_id = parseInt(productId);							
				$zestard_easy.ajax({
					url: '/cart/change.js',
					data: {quantity: 0, id: product_id},
					async: false
				}); 
				$zestard_easy.ajax({
					url: '/cart/add.js',
					data: {quantity: 1, id: product_id},
					async: false
				});									
            }
        });
        location.reload();
    }    
    e.preventDefault();
});

$zestard_easy("#textbox_preview").submit(function (e) {
    var data = $zestard_easy("#textbox_amount").val();
    $zestard_easy.ajax({
        type: "POST",
        url: "{{ url('frontend/dropdown/save') }}",
        data: {
            data: data,
            'id': shop_id
        },
        beforeSend: function () {
            $zestard_easy('.loaderDiv').css('display', 'inline-block');
        },
        success: function (productId) {
            $zestard_easy(".loaderDiv").hide();
            $zestard_easy(".overlay").css({
                'display': 'inline-block',
                'visibility': 'visible',
                'opacity': '1'
            });
            $zestard_easy(".donate-close").click(function () {
                $zestard_easy(".overlay").css({
                    'display': 'none'
                });
                reloadcart();
            });
            $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
        }
    });
    e.preventDefault();
});

$zestard_easy("#pricebarform").submit(function (e) {
    var data = $zestard_easy("#pricebar_slider").val();
    $zestard_easy.ajax({
        type: "POST",
        url: "{{ url('frontend/dropdown/save') }}",
        data: {
            data: data,
            'id': shop_id
        },
        beforeSend: function () {
            $zestard_easy('.loaderDiv').css('display', 'inline-block');
        },
        success: function (productId) {
            $zestard_easy(".loaderDiv").hide();
            $zestard_easy(".overlay").css({
                'display': 'inline-block',
                'visibility': 'visible',
                'opacity': '1'
            });
            $zestard_easy(".donate-close").click(function () {
                $zestard_easy(".overlay").css({
                    'display': 'none'
                });
                reloadcart();
                //location.reload();
            });
            $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
        }
    });
    e.preventDefault();
});
</script>
<div id="donate-popup" class="overlay" style="display: none" align = "center">
    <div class="popup">
        <h2>Thanks For Donating!</h2>
        <a class="donate-close" href="#">&times;</a>        
    </div>
</div>
<div class="easy_donation_additional_css"></div>
<div id="preview_product_container" class="product_fontpreview" style="display: none">
    <div id="dropdown_product_preview" style="display: none">
        <form name="product_dropdownform" id="product_dropdownform">
            <div class="product-price-slider">                
                <select name="donation_product_amount" id="donation_product_amount" class="donation_product_amount field__input"></select>  
                <input class="field__input" type="number" name="dropdown_product_textbox" id="dropdown_product_textbox" step="any" min="1" style="display:none;"/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_product_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>
        </form>
    </div>
    
    <div id="textbox_product_preview" style="display: none">
        <form name="product_textbox_form" id="product_textbox_form">
            <div class="product-price-slider">  
                <label id="minimum_product_donation" for="minimum_product_donation" style="display:none;"></label>
                <input type="number" class="field__input" name="textbox_product_amount" id="textbox_product_amount" step="any" min="1" required/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_product_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>          
        </form>
    </div>
    
    <div id="pricebar_product_preview" style="display: none">
        <div class="product-price-box">
            <form name="product_pricebarform" id="product_pricebarform">
                <div class="product-price-slider">                    
                    <input class="field__input" type="range" id="product_pricebar_slider" />          
                </div>
                <div class="product-price-form">
                    <label class="amountsize" for="amount">Amount: </label> 
                    <input type="hidden" id="amount">
                    <p class="price lead" id="amount-product-label"></p>
                    <p class="price">.00</p>
                </div>
                <div class="price-slider_button">
                    <button type="submit" class="btn button donation_product_button">Donate</button>            
                    <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="preview_container" class="fontpreview" style="display: none">
    <div id="dropdown_preview" style="display: none">
        <form name="dropdownform" id="dropdownform">
            <div class="price-slider">
                <h3 class="display_title"></h3>
                <div class="donation_content">
                    <p class="product-image"></p>
                    <p class="display_description"></p>
                </div>
                <select name="donation_amount" id="donation_amount" class="donationamount field__input"></select> 
                <input class="field__input" type="number" name="dropdown_textbox" id="dropdown_textbox" step="any" min="1" style="display:none;"/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>
        </form>
    </div>
    
    <div id="textbox_preview" style="display: none">
        <form name="textbox_form" id="textbox_form">
            <div class="price-slider">
                <h3 class="display_title"></h3>
                <div class="donation_content">
                    <p class="product-image"></p>
                    <p class="display_description"></p>
                </div>
                <label id="minimum_donation" for="minimum_donation" style="display:none;"></label>
                <input class="field__input" type="number" name="textbox_amount" id="textbox_amount" step="any" min="1" required/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>          
        </form>
    </div>
    
    <div id="pricebar_preview" style="display: none">
        <div class="price-box">
            <form name="pricebarform" id="pricebarform">
                <div class="price-slider">
                    <h3 class="display_title"></h3>
                    <div class="donation_content">
                        <p class="product-image"></p>
                        <p class="display_description"></p>
                    </div>
                    <input class="field__input" type="range" id="pricebar_slider" />          
                </div>
                <div class="price-form">
                    <label class="amountsize" for="amount">Amount: </label> 
                    <input type="hidden" id="amount">
                    <p class="price lead" id="amount-label"></p>
                    <p class="price">.00</p>
                </div>
                <div class="price-slider_button">
                    <button type="submit" class="btn button donation_button">Donate</button>            
                    <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
                </div>
            </form>
        </div>
    </div>
</div>