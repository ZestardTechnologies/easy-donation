@extends('headerpro')
@section('content')
<?php $shop_name = str_replace(array('http://','https://'), array('','') , session('shop'));?>
<style>
        /* Outer */
        .popup {
            width:100%;
            height:100%;
            display:none;
            position:fixed;
            top:0px;
            left:0px;
            background:rgba(0,0,0,0.50);
        }
    
        /* Inner */
        .popup-inner {
            max-width:500px;
            width:90%;
            padding:40px;
            position:absolute;
            top:50%;
            left:50%;
            -webkit-transform:translate(-50%, -50%);
            transform:translate(-50%, -50%);
            box-shadow:0px 2px 6px rgba(0,0,0,1);
            border-radius:3px;
            background:#fff;
        }
    
        /* Close Button */
        .popup-close {
            width:30px;
            height:30px;
            padding-top:4px;
            display:inline-block;
            position:absolute;
            top:0px;
            right:0px;
            transition:ease 0.25s all;
            -webkit-transform:translate(50%, -50%);
            transform:translate(50%, -50%);
            border-radius:1000px;
            background:rgba(0,0,0,0.8);
            font-family:Arial, Sans-Serif;
            font-size:20px;
            text-align:center;
            line-height:100%;
            color:#fff;
        }

        .popup-close:hover {
            -webkit-transform:translate(50%, -50%) rotate(180deg);
            transform:translate(50%, -50%) rotate(180deg);
            background:rgba(0,0,0,1);
            text-decoration:none;
            color: #fff;
        } 
        div#pop-up {
            z-index: 999;
        }
    </style>
<?php //$shop = 'https://' . session('shop'); ?>

<?php
        if (!session('shop')) {
            $shop = session('shop');
        } else if(isset($_REQUEST['shop'])) {
            $shop = $_REQUEST['shop'];
        }else{
            $shop = "";
        }        
        ?>

<script type="text/javascript">
    ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
    title: 'Dashboard',
            buttons: {
            primary: {
                    label: 'Dashboard Settings Demo',
                    callback: function(){ introJs().start(); }
            },
            secondary: [
            {
            label: 'Donations',
                    href : '{{route($add_donation_route,$product_id)}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'Track Donation',
                    href : '{{route("track_donationpro")}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'General Settings',
                    href : '{{route("email_configurationpro")}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'Help',
                    href : '{{route("help_pro")}}?shop=<?php echo $shop; ?>',
                    loading: true
            }
            ]
            }
    });
    });</script>    
<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script src="https://zestardshop.com/shopifyapp/easy_donation/public/js/ckeditor.js"></script>
<script>
    $( document ).ready(function() {        

        if("{{ $intro_status }}")
        {
            //alert('{{ $intro_status }}');
            var new_install = "{{ $intro_status }}";	
            if(new_install == "Y")
            {
                $('#new_note').modal('show');
            }
        }

        $("#take-tour").on("click", function(){
            var myintro = introJs();
            myintro.start();
            myintro.oncomplete(function() {
                window.top.location.href="{{ url("update_demo_status") }}";
            }).onexit(function() {
                window.top.location.href="{{ url("update_demo_status") }}";
            });
        });
        $(".close-tour").on("click", function(){
            $.ajax({
				url:'update_demo_status',
				data:{},
				async:false,					
				success:function(result)
				{
                    //location.reload();
				}
			});	
        });
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

            e.preventDefault();
        });
        
    });
    
</script>
<main class="full-width easy-donation-main">
    <header>
        <div class="container">            
            <div class="adjust-margin toc-block">
                <h1 class="toc-title">Dashboard</h1>
                <p class="toc-description">With Easy Donation App - Create a Charity of your own choice from dashboard - View donation section in cart or product page - Let your customer choose and donate some amount - Generate & Send Receipts - Track Donations Be a part of Good Cause.</p>
            </div>
        </div>	
    </header>
    <section>          
        <div class="full-width"> 
            <article>
                <div class="column twelve card img-padding es-image" style="background-image: url('{{ asset('image/donation_banner_image.svg') }}');" data-step="1" data-intro="To initiate configurations please fill the information for the donation section.Click on 'Add Donation' Button to add/create the donation.">
                    <div class="empty-section">
                        <div class="details-wrapper">
                            <div class="text-wrapper">
                                <h2 class="heading-text">Setup Donations</h2>
                                <div class="sub-text">
                                    <p>Let's get started by donations</p>
                                </div>
                            </div>
                            <?php 
                                if($shop_find->app_version == 1){
                                    
                                    if($product_id == ""){
                                        $buttonName = "Add Donation";
                                    }else{
                                        $buttonName = "Manage Donation";
                                    }
                                    
                                }else{
                                    $buttonName = "Add Donation";
                                }

                            ?>
                            <div class="action-wrapper adddonation">
                            <a class="btn btn-primary button" type="submit" name="save" href="{{route($add_donation_route,$product_id)}}?shop=<?php echo $shop_name; ?>">{{ $buttonName }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </article>	
            <article>	
                <div class="columns six card secondary" data-step="2" data-intro="Once donation gets create, please click on 'Configure' button to make general settings. It will allow to enable/disable application and to enter other general information for the donation box.">
                    <h3>General Settings</h3>
                    <div class="row">
                        <div class="columns three ">
                            <img src="{{ asset('image/email_configuration.svg') }}" width="80px">
                        </div>
                        <div class="columns nine">
                            <p>Allows you to set Title, CSS, Message and look and feel of the donation section which will be visible in the front side.</p>
                            <a href="{{route('email_configurationpro')}}?shop=<?php echo $shop; ?>"><button class="btn btn-primary" type="submit" name="save">Configure</button></a>
                        </div>
                    </div>
                </div>
                <div class="columns six card secondary" data-step="3" data-intro="This section will provide information for the donations made throughout the year against each month. Use 'Track' button to know the overall statistics information of the donations.">
                    <h3>Track Donations</h3>
                    <div class="row">
                        <div class="columns three">
                            <img src="{{ asset('image/track_donation.svg') }}" width="80px">
                        </div>
                        <div class="columns nine ">
                            <p>Allows you to view and download full statistics of the amount donated by the Customers against a particular year.</p>
                            <a href="{{route('track_donationpro')}}?shop=<?php echo $shop; ?>"><button class="btn btn-primary" type="submit" name="save">Track</button></a>
                        </div>
                    </div>
                </div>
            </article>
            <!-- Documentation Start -->
            <article>
                <div class="column twelve card" data-step="4" data-intro="Please refer to the section 'Where to paste Shortcode?' if you would like to insert manually shortcode into your shopify theme's cart file and product file. <br> Or else if you want automatic insertion of the short-code then select specific page from the drop-down and click 'Put Shortcode in template'. That's it and it will be good to go.">
                    <div class="empty-section">
                        <div class="details-wrapper">
                            <div class="text-wrapper">
                                <div class="success-copied"></div>
                                <h2 class="heading-text">Shortcode</h2>
                                <div class="cus-row">
                                    <div class="c-7">
                                        <textarea id="ticker-shortcode" rows="1" class="form-control short-code"  readonly="">{% include 'donation' %}</textarea>
                                    </div>
                                    <div class="c-5">
                                        <button type="button" onclick="copyToClipboard('#ticker-shortcode')" class="btn tooltipped tooltipped-s copyMe copyclippro" accesskey="" style="display: block;"><i class="fa fa-check"></i> &nbsp; Copy to clipboard</button>                                
                                    </div>
                                </div>

                                <h2 class="heading-text">Where to paste Shortcode?</h2>
                                <p><b>Cart Page</b></p>
                                <ul class="dashboard-shortcode">
                                    <li>Copy the Shortcode from above and paste it in <a href="<?php echo $shop; ?>/admin/themes/current?key=sections/cart-template.liquid" target="_blank"><b>cart-template.liquid</b></a></li>
                                    <li>If your theme does not have cart-template.liquid file then paste the short-code in <a href="<?php echo $shop; ?>/admin/themes/current?key=templates/cart.liquid" target="_blank"><b>cart.liquid</b></a></li>
                                </ul>
                                <p><b>Product Page</b></p>
                                <ul class="dashboard-shortcode">
                                    <li>Copy the Shortcode from above and paste it in <a href="<?php echo $shop; ?>/admin/themes/current?key=sections/product-template.liquid" target="_blank"><b>product-template.liquid</b></a>
                                    </li>
                                    <li>If your theme does not have product-template.liquid file then paste the short-code in <a href="<?php echo $shop; ?>/admin/themes/current?key=templates/product.liquid" target="_blank"><b>product.liquid</b></a></li>
                                </ul>
                              
                                <div class="alert notice">
                                    <dl>
                                        <dt><b>Note</b> : </dt>
                                        <dd>Do not paste the above code multiple times in the same liquid file.</dd>
                                        <dd>Do not delete products which are created by Easy Donation app under Products menu at the backend.</dd>
                                    </dl>
                                </div>
                                <h2 class="heading-text">Do you want us to paste the Shortcode?</h2>
                                <div class="adjust-margin">
                                    @if(session()->has('success'))
                                    <div class="alert success alertHide" id="scrolltobottom">
                                        <a class="close btnClose"></a>
                                        <dl>
                                            <dt>Success</dt>
                                            <dd>{{ Session('success') }}</dd>
                                        </dl>
                                    </div>
                                    @endif
                                    @if(session()->has('error'))
                                    <div class="alert error alertHide" id="scrolltobottom" style="display: block">
                                        <a class="close btnClose"></a>
                                        <dl>
                                            <dt>Error</dt>
                                            <dd>{{ Session('error') }}</dd>
                                        </dl>
                                    </div>
                                    @endif                
                                </div>
                                                        
                                    <div class="has-sections  fields-group parent-automatic-shortcode row ">
                                        <div class="automatic-shortcode col-sm-8">
                                            <select class="validation form-control select_page cur_point select_template_page" name="select_template_page" id="select_template_page">
                                                <option value="">Select Template Page</option>
                                                <option value="1">Cart Page</option>
                                                <option value="2">Product Page</option>
                                            </select>
                                            <div id="errors_select_template_page" style="color: red;display: none">Please select template page</div>
                                            <p><strong>Note : </strong>Select the page from the option where you want to paste the code then click 'Put Shortcode' and will get your job done.</p>
                                            

                                        
                                        </div>                                        
                                        <div class="col-sm-2">                                        
                                            <input type="submit" class="btn btn-primary submit-loader" id="shortcode_pase_template" name="BtnPutShortcode" value="Put Shortcode in Template">
                                        </div>                                        
                                    </div>                                    
                                                           

                                <div style="display: none;">
                                    <p><b>Paste Shortcode in Cart Template</b></p>
                                   <div class="">
                                       {!! Form::open(['route' => ['snippet_create_cart', 'shop' => $shop],'id' => 'RemoveCartVariants', 'method'=> 'post','class' =>'submitForm']) !!}
                                       <button class="btn btn-primary" type="submit" name="snippet_cart">Paste Shortcode in Cart</button>
                                       {!! Form::close() !!}
                                   </div>

                                   <p><b>Paste Shortcode in Product Template</b></p>
                                   <div class="">
                                       {!! Form::open(['route' => ['snippet_create_product','shop' => $shop],'id' => 'RemoveProductVariants', 'method'=> 'post','class' =>'submitForm']) !!}
                                       <button class="btn btn-primary" type="submit" name="snippet_product">Paste Shortcode in Product</button>
                                       {!! Form::close() !!}
                                   </div>
                                   <br>
                                </div>                                
                                <p class="donation-note note_delete"><b></b></p>
                            </div>
                            <div class="action-wrapper">
                            </div>
                        </div>                        
                    </div>
                </div>
            </article>
            <!-- Documentation End -->
        </div>
    </section>
    <footer></footer>
    <div class="container">        
            <div id="pop-up">                
                    <div class="popup" data-popup="popup-1" style="display: {{ ($intro_status == "Y") ? 'block' : 'none' }}">                    
                    <div class="popup-inner">
                        <h2>Welcome to Updated version of Easy Donation !</h2>
                        <?php /* <p>We have updated our app with new look & features and if you face any problem just drop us an email at <a href="mailto:support@zestard.com">support@zestard.com</a> and will get back to you.</p> */ ?>
                        <p>As we have changed our app look, we advise you to have a quick brush up to look up for all modification we did by taking this tour. To start the tour please click on TOUR BUTTON.</p>                         
                        <button id="take-tour" class="btn btn-primary submit-loader" data-popup-close="popup-1" type="button" name="Take Toure"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon" style="display:none;"></i>Start Tour</button>
                        <a class="popup-close close-tour" data-popup-close="popup-1" href="#">x</a>
                    </div>                    
                </div>                
            </div>
            
    </div>
    
</main>
<script src="{{ asset('js/commonJS.js') }}"></script>
<script>
       var errorMsg = $('.alertHide').attr('id');
       if(errorMsg == "scrolltobottom") {
            $("html, body").animate({ scrollTop: $(document).height() }, 2000);
       }

    $("#shortcode_pase_template").click(function () { 
        var template_value = $('#select_template_page').val();        
        if(template_value == '1'){
            $( "#RemoveCartVariants" ).submit();
            
            $('#errors_select_template_page').hide();
            $('#select_template_page').css('border', '');            
            $(".submit-loader").attr("disabled", "disabled");
            $(".submit-loader").css("padding-left", "25px");
            
        }else if(template_value == '2'){
            $( "#RemoveProductVariants" ).submit();
            
            $('#errors_select_template_page').hide();
            $('#select_template_page').css('border', '');             
            $(".submit-loader").attr("disabled", "disabled");
            $(".submit-loader").css("padding-left", "25px");
        }else{
            $('#errors_select_template_page').show();
            $('#select_template_page').css('border', '1px solid red'); 
            return false;
        }
    });    
   
</script>
@endsection