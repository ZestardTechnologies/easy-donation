@extends('headerpro')
@section('content')
<?php
        if (!session('shop')) {
            $shop = session('shop');
        } else if(isset($_REQUEST['shop'])) {
            $shop = $_REQUEST['shop'];
        } else{
            $shop = "";
        }        
        ?>

<script type="text/javascript">
    ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
    buttons: {
    primary: {
            label: 'Add Donation Settings Demo',
            callback: function(){ introJs().start(); }
    },
    secondary: [
    {
    label: 'Dashboard',
            href : '{{route("dashboard")}}?shop=<?php echo $shop; ?>',
            loading: true
    },
    {
    label: 'Donations',
            href : '{{route($add_donation_route,$product_id)}}?shop=<?php echo $shop; ?>',
            loading: true
    },
    {
    label: 'Track Donation',
            href : '{{route("track_donationpro")}}?shop=<?php echo $shop; ?>',
            loading: true
    },
    {
    label: 'General Settings',
            href : '{{route("email_configurationpro")}}?shop=<?php echo $shop; ?>',
            loading: true
    },
    {
    label: 'Help',
            href : '{{route("help_pro")}}?shop=<?php echo $shop; ?>',
            loading: true
    }
    ]
    }
    });
    });</script>  
<script>
    $().ready(function() {
    var error_count = 0;
    $("#DonationAddForm").validate({
    submitHandler: function(form) {
    var option_selected = $("input[name='ui_select']:checked").val();
    //For Dropdown Validation
    if (option_selected == "D"){
    $('.dropdown_option').each(function() {
    if ($(this).val() == ''){
    $(this).parent().next(".error_dropval").html('Please enter amount');
    $(this).css("border", "red solid 1px");
    error_count = error_count + 1;
    } else{
    $(this).parent().next(".error_dropval").html('');
    $(this).css("border", "1px solid #c4cdd5");
    return true;
    }
    });
    }
    //For Pricebar Validation
    if (option_selected == "P"){
    var price_min = parseInt($(".donation_min").val());
    var price_max = parseInt($(".donation_max").val());
    var price_min_length = $(".donation_min").val().length;
    var price_max_length = $(".donation_max").val().length;
    if ((price_min_length <= 0) || (price_max_length <= 0)){
    error_count = error_count + 1;
    $(".donation_max").css("border", "red solid 1px");
    $(".donation_min").css("border", "red solid 1px");
    $("#error_bar_range").html('Can not be null.');
    }

    if (price_max <= price_min){
    error_count = error_count + 1;
    $(".donation_max").css("border", "red solid 1px");
    $(".donation_min").css("border", "red solid 1px");
    $("#error_bar_range").html('Mininum amount should be less then Maximum amount');
    } 
//    else{
//    $(".donation_max").css("border", "1px solid #c4cdd5");
//    $(".donation_min").css("border", "1px solid #c4cdd5");
//    $("#error_bar_range").html('');
//    }
    }
    //For TextBox Validation
    if (option_selected == "T"){
    var text_val = $('#add_min_amount').val().length;
    if (text_val <= 0) {
    error_count = error_count + 1;
    $(".add_min_amount").css("border", "red solid 1px");
    $("#error_textoption").html('Can not be null.');
    } else{
    $("#error_textoption").html('');
    $(".add_min_amount").css("border", "1px solid #c4cdd5");
    }
    }
    if (error_count > 0){
    error_count = 0;
    return false;
    }
    else{
    $("#addDonationProBtn").attr("disabled", "disabled");
    $("#addDonationProBtn .btn-loader-icon-submit").css("display", "block");
    $("#addDonationProBtn .submit-loader").css("padding-left", "30px");
    $(".overlay").show();
    return true;
    }
    },
            messages: {
            'ui_select': {
            required: "You must check at least 1 box",
            },
                    'donation_name':{
                    required: "Donation name field is required",
                    }
            },
            errorPlacement: function ($error, $element) {
            console.log($error);
            var name = $element.attr("name");
            $("#errors-" + name).append($error);
            }
    });
    });</script>
<main class="full-width basic-donation">
    <header>
        <div class="container">
            <div class="adjust-margin">
                @if(session()->has('success'))
                <div class="alert success alertHide">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Success</dt>
                        <dd>{{ Session('success') }}</dd>
                    </dl>
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert danger alertHide">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Error</dt>
                        <dd>{{ Session('error') }}</dd>
                    </dl>
                </div>
                @endif
                @if($errors->has('upload_donation_image'))
                <div class="alert error alertHide" style="display: block;">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Error</dt>
                        <dd>{{ $errors->first('upload_donation_image') }}</dd>
                    </dl>
                </div>
                @endif
            </div>
        </div>
        <div class="container">
            <div class="adjust-margin toc-block">
                <h1 class="toc-title">Add Donation</h1>
                <p class="toc-description">This section will allow you to add new donation.</p>
            </div>
        </div>        
    </header> 
    <section>
        <div class="overlay"></div> 
        {!! Form::open(['route' => ['basic_donation_save', 'shop' => $shop],'id' => 'DonationAddForm', 'enctype'=> 'multipart/form-data','class' =>'submitForm']) !!}
        <article>
            <div class="card has-sections columns eight fields-group">
                <div class="card-section">
                    <div class="mb-3" data-step="1" data-intro="Here you have to add donation name.It can be anything relevant to the cause or charity for which you are collecting donations.">
                        <label for="donation_name">Donation Name</label>
                        <input name="donation_name" id="donation_name" type="text" class="validation form-control donation_name" required>
                        <div id="errors-donation_name"></div>
                    </div>
                    <div class="mb-3" data-step="2" data-intro="This section can be used to provide a brief idea to the customers about the purpose and motive behind collecting donations.">
                        <label for="donation_Description">Donation Description</label>
                        <textarea name="donation_Description" id="donation_Description" class="validation form-control donation_Description" rows="4" required></textarea>
                        @ckeditor('donation_Description')
                    </div>
                    <div class="mb-3" data-step="3" data-intro="Please select the either of the page or both where you would like to display the donation option.">
                        <label for="select_page">Select Page</label>
                        <select class="validation form-control select_page cur_point" name="select_page" id="select_page" required>
                            <option value="1">Cart Page</option>
                            <option value="2">Product Page</option>
                            <option value="3">Both</option>
                        </select>
                    </div>
                    <div class="mb-3" data-step="4" data-intro="This section will depicts different types of field options that can be selected and allowed to show to customers to make donations.">
                        <label>Field Option</label>
                        <div class="innerdiv">
                            <label for="dropdown"><input class="validation with-gap drop range-field" name="ui_select" type="radio" id="dropdown" value="D" required />
                                DropDown</label>
                        </div>
                        <div class="innerdiv">
                            <label for="range_option"><input class="with-gap range_option range-field" name="ui_select" type="radio" id="range_option" value="P"  />
                                Price Bar</label>
                        </div>
                        <div class="innerdiv field-range-check">
                            <label for="custom_text"><input class="with-gap custom_text range-field" name="ui_select" type="radio" id="custom_text" value="T"  />
                                Text Box</label>
                        </div>
                        <div id="errors-ui_select"></div>
                    </div>
                    <div class="form-group field-option" id="text_range" style="display: none;">
                        <div class="columns twelve range-inner-div">
                            <div class="add-min-amount">
                                <label class="amount" for="add_min_amount">Add Minimum Amount</label>
                                <div>
                                    <input id="add_min_amount" name="add_min_amount" type="number" class="validate form-control add_min_amount" min="0">
                                </div>
                            </div>
                            <div class="columns twelve form-group inner-div">                         
                                <p class="donation-note"><b>Note:</b> You can set here the minimum value and at donation page the customer can enter the value in Text Box. An Alert/Validation is set to make sure that customer do not enter the value less then Minimum amount set here.</p>
                            </div>                          
                        </div>
                    </div>
                    <div class="form-group field-option" id="bar_range" style="display: none; overflow: hidden;">
                        <div class="range-inner-div">
                            <div class="columns six form-group">
                                <label for="donation_min">Minimum Amount</label>
                                <input name="donation_min" id="donation_min" type="number" class="validate form-control donation_min" min="0" >
                            </div>
                            <div class="columns six form-group">
                                <label for="donation_max">Maximum Amount</label>
                                <input name="donation_max" id="donation_max" type="number" class="validate form-control donation_max" min="0" >
                            </div>
                            <div id="error_bar_range" style="color:red;"></div>
                        </div>
                    </div>
                    <div class=" form-group field-option" id="dropdown_range" style="display: none;"> 
                        <div class="column twelve dropdown-wrapper">
                            <div class="form-group range-inner-div dropdown_add">
                                <div class="leftpadding">
                                    <label class="amount" for="dropdown_add">Amount</label>

                                    <div class="cus-row">
                                        <div class="col-nine">
                                            <input name="dropdown_option[]" type="text" class="validate option_value form-control dropdown_option">
                                        </div>                                         
                                    </div>

<?php /* <div>
  <input name="dropdown_option[]" type="text" class="validate option_value form-control dropdown_option">
  </div> */ ?>

                                    <div class="error_dropval" style="color:red;"></div>
                                </div>
                            </div>
                        </div>
                        <a class="rowpointer addrow"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add Amount</a>
                        <div class=" row-dropdown-other">
                            <div class="columns twelve">
                                <label for="dropdown_other">
                                    <input type="checkbox" id="dropdown_other" name="dropdown_other" value="checked" class="dropdown_other">
                                    Let the donor choose what they want to pay
                                </label>
                            </div>    
                        </div>
                        <label for="text_dropdown_other">Option Text</label>
                        <div class="other_option">
                            <input id="text_dropdown_other" name="text_dropdown_other" type="text" class="validate form-control text_dropdown_other" placeholder="Other..">
                        </div>                                
                    </div>
                </div>
            </div>
            <div class="columns four">
                <div class="alert custom-alert">
                    <dl>
                        <dt>App creates donation option in form of product which you can access from the Shopify Products section.It is highly recommended not to delete this default app product from anywhere for the proper functioning of the application.</dt>
                    </dl>
                </div>
                <div class="card" data-step="5" data-intro="Use this section to add or update the donation product image which you would like to display.">
                    <div class=" action-area">
                        <div class="pl-4" id="check_imagediv">
                            <div class="columns twelve">                                
                                <div class="chooseimage">                                    
                                    <input type="file" id="upload_donation_image"  name="upload_donation_image" style="display:none" />
                                    <img src="{{ asset('image/Donate.png') }}" id="upload_image">
                                    <u><label id="uploadImage">Upload Image</label></u>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>    
        </article>

    </section>  
    <hr class="hr-custom"/>
    <section class="edit-btn" data-step="6" data-intro="Click on Add Donation button">
        <article>
            <a onclick='return confirm("Are you sure you want to go with listing donation page?");' href="{{route($add_donation_route,$product_id)}}"><button class="btn btn-primary secondary" type="button" name="cancel">Cancel</button></a> &nbsp;&nbsp;&nbsp;&nbsp;
            <button id="addDonationProBtn" class="btn btn-primary submit-loader" type="submit" name="save"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon btn-loader-icon-submit" style="display:none;"></i>Save Donation</button>                

        </article>
    </section>
    {!! Form::close() !!}
</main>
<script src="{{ asset('js/commonJS.js') }}"></script>
<script type="text/javascript">
                function readURL(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                $('#upload_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                }
                }

                $("#upload_donation_image").change(function(){
                readURL(this);
                });
</script>
@endsection
