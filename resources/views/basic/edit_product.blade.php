@extends('headerpro')
@section('content')

<?php
        if (!session('shop')) {
            $shop = session('shop');
        } else if(isset($_REQUEST['shop'])) {
            $shop = $_REQUEST['shop'];
        }else{
            $shop = $shop;
        }        
        ?>

<script type="text/javascript">
    ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
    buttons: {
    secondary: [
    {
    label: 'Dashboard',
            href : '{{route("dashboard")}}?shop=<?php echo $shop; ?>',
            loading: true
    },
    {
    label: 'Donations',
            href : '{{route($add_donation_route,$product_id)}}?shop=<?php echo $shop; ?>',
            loading: true
    },
    {
    label: 'Track Donation',
            href : '{{route("track_donationpro")}}?shop=<?php echo $shop; ?>',
            loading: true
    },
    {
    label: 'General Settings',
            href : '{{route("email_configurationpro")}}?shop=<?php echo $shop; ?>',
            loading: true
    },
    {
    label: 'Help',
            href : '{{route("help_pro")}}?shop=<?php echo $shop; ?>',
            loading: true
    }
    ]
    }
    });
    });</script>
<script>
    $().ready(function() {
    var error_count = 0;
    $("#DonationEditForm").validate({
    submitHandler: function(form) {
    var option_selected = $("input[name='ui_select']:checked").val();
    //For Dropdown Validation
    if (option_selected == "D"){
    $('.dropdown_option').each(function() {
    if ($(this).val() == ''){
    console.log($(this).parent().next(".error_dropval"));
    $(this).parent().next(".error_dropval").html('Please enter amount');
    $(this).css("border", "red solid 1px");
    error_count = error_count + 1;
    } else{
    $(this).parent().next(".error_dropval").html('');
    $(this).css("border", "1px solid #c4cdd5");
    return true;
    }
    });
    }
    //For Pricebar Validation
    if (option_selected == "P"){
    var price_min = parseInt($(".donation_min").val());
    var price_max = parseInt($(".donation_max").val());
    var price_min_length = $(".donation_min").val().length;
    var price_max_length = $(".donation_max").val().length;
    if ((price_min_length <= 0) || (price_max_length <= 0)){
    error_count = error_count + 1;
    $(".donation_max").css("border", "red solid 1px");
    $(".donation_min").css("border", "red solid 1px");
    $("#error_bar_range").html('Can not be null.');
    }
    if (price_max <= price_min){
    error_count = error_count + 1;
    $(".donation_max").css("border", "red solid 1px");
    $(".donation_min").css("border", "red solid 1px");
    $("#error_bar_range").html('Mininum amount should be less then Maximum amount');
    }
//    else{
//    $(".donation_max").css("border", "1px solid black");
//    $(".donation_min").css("border", "1px solid black");
//    $("#error_bar_range").html('');
//    }
    }
    //For TextBox Validation
    if (option_selected == "T"){
    var text_val = $('#add_min_amount').val().length;
    if (text_val <= 0) {
    error_count = error_count + 1;
    $(".add_min_amount").css("border", "red solid 1px");
    $("#error_textoption").html('Can not be null.');
    } else{
    $("#error_textoption").html('');
    $(".add_min_amount").css("border", "1px solid #c4cdd5");
    }
    }
    if (error_count > 0){
    error_count = 0;
    return false;
    }
    else{
    $("#addDonationProBtn").attr("disabled", "disabled");
    $("#addDonationProBtn .btn-loader-icon-submit").css("display", "block");
    $("#addDonationProBtn .submit-loader").css("padding-left", "30px !important");
    $(".overlay").show();
    return true;
    }
    },
            messages: {
            'ui_select': {
            required: "You must check at least 1 box",
            },
                    'donation_name':{
                    required: "Donation name field is required",
                    }
            },
            errorPlacement: function ($error, $element) {
            //console.log($error);
            var name = $element.attr("name");
            $("#errors-" + name).append($error);
            }
    });
    });</script>
<main class="full-width basic-donation">
    <header>
        <div class="container">
            <div class="adjust-margin">                 
                @if(session()->has('success'))
                <div class="alert success alertHide">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Success</dt>
                        <dd>{{ Session('success') }}</dd>
                    </dl>
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert danger alertHide">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Error</dt>
                        <dd>{{ Session('error') }}</dd>
                    </dl>
                </div>
                @endif
                @if($errors->has('upload_donation_image'))
                <div class="alert error alertHide" style="display: block;">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Error</dt>
                        <dd>{{ $errors->first('upload_donation_image') }}</dd>
                    </dl>
                </div>
                @endif
            </div>
        </div>
        <div class="container">
            <div class="adjust-margin toc-block">
                <h1 class="toc-title">Manage Donation</h1>
                <p class="toc-description">This section will allow you to update the existing information for the donations.</p>
            </div>
        </div>

    </header>
    {!! Form::open(['route' => ['basic_donation_save', 'shop' => $shop],'id' => 'DonationEditForm', 'enctype'=> 'multipart/form-data','class' =>'submitForm']) !!}    
          {!! csrf_field() !!}
            <input type="hidden" name="shop" value="{{ $shop }}">
    <section>
       
        <article>
            <div class="card has-sections columns eight fields-group">
                <div class="card-section">
                    <input type="hidden" name="product_id" id="product_id" value="{{ $product->product->id }}">
                    <div class="mb-3">
                        <label for="donation_name">Donation Name</label>
                        <input name="donation_name" id="donation_name" type="text" class="validate form-control donation_name" required value="{{ $product->product->title }}">
                        <div id="errors-donation_name"></div>
                    </div>
                    <div class="mb-3">
                        <label for="donation_Description">Donation Description</label>
                        <textarea name="donation_Description" id="donation_Description" class="form-control donation_Description" rows="4" required>{{ $product->product->body_html }}</textarea>
                        @ckeditor('donation_Description')
                    </div>
                    <div class="mb-3">
                        <label for="select_page">Select Page</label>
                        <select class="form-control select_page cur_point" name="select_page" required>
                            <option value="">Select Page</option>
                            <option value="1" @if($donation_config->select_page == 1) selected="selected" @endif >Cart Page</option>
                            <option value="2" @if($donation_config->select_page == 2) selected="selected" @endif>Product Page</option>
                            <option value="3" @if($donation_config->select_page == 3) selected="selected" @endif>Both</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <?php //echo '<pre>';print_r($selected_product); exit;?>
                        <label>Field Option</label>                        
                        <div class="innerdiv">
                            <label for="dropdown"><input class="with-gap drop range-field" name="ui_select" type="radio" id="dropdown" value="D" @if($selected_product->field_option == "D") checked="checked" @endif />DropDown</label>
                        </div>
                        <div class="innerdiv">
                            <label for="range_option"><input class="with-gap range_option range-field" name="ui_select" type="radio" id="range_option" value="P" @if($selected_product->field_option == "P") checked="checked" @endif  />Price Bar</label>
                        </div>
                        <div class="innerdiv field-range-check">
                            <label for="custom_text"><input class="with-gap custom_text range-field" name="ui_select" type="radio" id="custom_text" value="T" @if($selected_product->field_option == "T") checked="checked" @endif />Text Box</label>
                        </div>
                        <div id="errors-ui_select"></div>
                    </div> 
                    <div class=" form-group field-option" id="text_range" style="display: @if($selected_product->field_option == 'T') block; @else none; @endif ">
                        <div class="columns twelve range-inner-div">
                            <div class="add-min-amount">
                                <label class="amount" for="add_min_amount">Add Minimum Amount</label>
                                <div>
                                    <input id="add_min_amount" name="add_min_amount" type="number" value="{{ isset($selected_product->add_min_amount) ? $selected_product->add_min_amount : '' }}" class="validate form-control add_min_amount" min="0">
                                </div>
                                <div id="error_textoption" style="color:red;"></div>
                            </div>
                            <div class="form-group inner-div">                         
                                <p class="donation-note"><b>Note:</b> You can set here the minimum value and at donation page the customer can enter the value in Text Box. An Alert/Validation is set to make sure that customer do not enter the value less then Minimum amount set here.</p>
                            </div>                          
                        </div>
                    </div>
                    <div class=" form-group field-option" id="bar_range" style="overflow: hidden; display: @if($selected_product->field_option == 'P') block; @else none; @endif">
                        <div class="range-inner-div">
                            <div class="columns six form-group">
                                <label for="donation_min">Minimum Amount</label>
                                <input name="donation_min" type="number" class="validate form-control donation_min" min="0" value="{{ isset($selected_product->donation_min) ? $selected_product->donation_min : ''  }}" >
                            </div>
                            <div class="columns six form-group">
                                <label for="donation_max">Maximum Amount</label>
                                <input name="donation_max" type="number" class="validate form-control donation_max" min="0" value="{{ isset($selected_product->donation_max) ? $selected_product->donation_max : ''  }}">
                            </div>
                            <div id="error_bar_range" style="color:red;"></div>
                        </div>
                    </div>
                    <?php
                    $dropdown_option = json_decode($selected_product->drop_down);
                    $donation_count = count($dropdown_option);
                    ?>
                    @if($donation_count > 0)
                    <div class=" form-group field-option" id="dropdown_range" style="display: @if($selected_product->field_option == 'D') block; @else none; @endif"> 
                        @if(!empty($dropdown_option))
                        <div class="column twelve dropdown-wrapper">
                            @foreach($dropdown_option as $key => $data)
                            <div class="range-inner-div dropdown_edit" id="edit_{{ $key }}">
                                <div class="leftpadding">
                                    <label class="amount" for="dropdown_add">Amount</label>

                                    <div class="cus-row">
                                        <div class="col-nine">
                                            <input name="dropdown_option[]" type="text" value="{{ $data }}" class="validate option_value form-control dropdown_option">
                                        </div> 
                                        <div class="col-one">
                                            <a class="editremoverow existremove" data-id="{{ $key }}"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </div>
                                    </div>

                                    <?php /* <div>
                                      <input name="dropdown_option[]" type="text" value="{{ $data }}" class="validate option_value form-control dropdown_option">
                                      </div>
                                      <a class="editremoverow existremove" data-id="{{ $key }}"><i class="fa fa-times" aria-hidden="true"></i></a> */ ?>


                                    <div class="error_dropval" style="color:red;"></div>
                                </div>
                            </div>

                            @endforeach
                        </div>
                        @endif
                        <a class="rowpointer editrow"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add Amount</a>

                        <div class=" row-dropdown-other">
                            <div class="columns twelve">
                                <label for="dropdown_other">
                                    <input type="checkbox" id="dropdown_other" name="dropdown_other" value="checked" <?php
                                    if (isset($selected_product->dropdown_other)) {
                                        if ($selected_product->dropdown_other == "1") {
                                            echo "checked='checked'";
                                        }
                                    }
                                    ?> class="dropdown_other">
                                    Let the donor choose what they want to pay
                                </label>
                            </div>    
                        </div>


                        <label for="text_dropdown_other">Option Text</label>
                        <div class="other_option">
                            <input id="text_dropdown_other" name="text_dropdown_other" type="text" value="{{ isset($selected_product->text_dropdown_other) ? $selected_product->text_dropdown_other : '' }}" class="validate form-control text_dropdown_other" placeholder="Other..">
                        </div>                                
                    </div>
                    @else
                    <div class=" form-group field-option" id="dropdown_range" style="display: none;"> 
                        <div class="column twelve dropdown-wrapper">
                            <div class="form-group range-inner-div dropdown_edit" >
                                <div class="leftpadding">
                                    <label class="amount" for="dropdown_add">Amount</label>
                                    <div>
                                        <input name="dropdown_option[]" type="text" class="validate option_value form-control dropdown_option">
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <a class="rowpointer editrow"><i class="fa fa-plus" aria-hidden="true"></i>Add Amount</a>
                        <input type="checkbox" id="dropdown_other" name="dropdown_other" value="checked" class="dropdown_other"><label for="dropdown_other">Let the donor choose what they want to pay</label>
                        <label for="text_dropdown_other">Option Text</label>
                        <div class="other_option">
                            <input id="text_dropdown_other" name="text_dropdown_other" type="text" class="validate form-control text_dropdown_other" placeholder="Other..">
                        </div>                                
                    </div> 
                    @endif
                </div>
            </div>
            <div class="columns four">
                <div class="alert custom-alert">
                    <dl>
                        <dt>App creates donation option in form of product which you can access from the Shopify Products section.It is highly recommended not to delete this default app product from anywhere for the proper functioning of the application.</dt>
                    </dl>
                </div>
                <div class="card">
                    <div class="action-area">
                        <div class="pl-4"  id="check_imagediv">
                            <div class="columns twelve">                                
                                <div class="chooseimage">     
                                    <input type="file" id="upload_donation_image"  name="upload_donation_image" style="display:none" />                                    
                                    @if(!empty($product->product->image))
                                    <img src="{{ $product->product->image->src }}" id="upload_image">
                                    @else
                                    <img src="{{ asset('image/Donate.png') }}" id="upload_image">
                                    @endif
                                    <u><label id="uploadImage">Change Image</label></u>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </article>    

    </section>
    <hr class="hr-custom"/>
    <section class="edit-btn">
        <article>
            <button id="addDonationProBtn" class="btn btn-primary submit-loader" type="submit" name="save"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon btn-loader-icon-submit" style="display:none;"></i>Edit Donation</button>
<?php /* <a onclick='return confirm("Are you sure you want to go with listing donation page?");' href="{{route('datatable_listing_pro')}}"><button class="btn btn-primary" type="button" name="cancel">Cancel</button></a> */ ?>
        </article>
    </section>
    {!! Form::close() !!}

</main>

<script src="{{ asset('js/commonJS.js') }}"></script>
<script type="text/javascript">
    /*
     * Add More
     */
    $(document).ready(function() {
    $(".editrow").on('click', function () {

<?php /* var new_add = '<div class="range-inner-div dropdown_edit">' +
  '<label class="amount" for="dropdown_option">Amount</label>' +
  '<div><input name="dropdown_option[]" type="text" class="validate option_value form-control dropdown_option"></div>' +
  '<a class="editremoverow"><i class="fa fa-times" aria-hidden="true"></i></a>' +
  '</div><div class="error_dropval" style="color:red;"></div>'; */ ?>

    var new_add = '<div class="range-inner-div dropdown_edit"><label class="amount" for="dropdown_option">Amount</label><div class="cus-row"><div class="col-nine"><input name="dropdown_option[]" type="text" class="validate option_value form-control dropdown_option"></div><div class="col-one"><a class="editremoverow"><i class="fa fa-times" aria-hidden="true"></i></a></div></div><div class="error_dropval" style="color:red;"></div></div>';
    var parent = $(this).parents("#dropdown_range");
    parent.children('.dropdown-wrapper').append(new_add);
    });
    /*
     * Remove Add More
     */
    $(document).on('click', '.editremoverow', function () {
    $(this).parent().parent().parent('.dropdown_edit').remove();
    });
    /*
     * Exist Remove Row while edit
     */
    $(document).on('click', '.existremove', function () {
    var data_id = $(this).attr("data-id");
    $('#edit_' + data_id).remove();
    });
    });</script>
<script type="text/javascript">
    function readURL(input) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
    $('#upload_image').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    }
    }

    $("#upload_donation_image").change(function(){
    readURL(this);
    });
</script>
@endsection
