@extends('header')
@section('content')
<script>
    $(document).ready(function () {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('.pricing_table_div').offset().top
        }, 'slow');
    });
</script>
<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script src="https://zestardshop.com/shopifyapp/easy_donation/public/js/ckeditor.js"></script>

<link rel="stylesheet" href="{{ asset('css/custom.css') }}"/>
<div class="container formcolor">
    <div class="pricing_table_block">
        <div class="pricing_table_div">            
            <div class="pricing_table_item pricing_middle_item margin_right">
                <div class="ribbon ribbon-small text-white">
                    <div class="ribbon-content bg_pink text-uppercase text_white">We Recommend</div>
                </div>
                <h1 class="pricing_table_title">Advance</h1>
                <div class="pricing_table_price_div">
                    <span class="doller_icon">$</span><h2>7.99</h2><span class="month_text"> / m</span>
                </div>
                <div class="pricing_table_list">
                    <ul>
                        <li>Multiple Donation option</li>	
                        <li>Multiple Donation image change Feature</li>
                        <li>User will get donation receipt for all the donation through email</li>
                        <li>Per month Track Donation functionality as per donation</li>	
                    </ul>
                </div>
                <div class="select_btn">				
                    <!--form action="advance" method="post"-->
                        {{ csrf_field() }}
                        <input name="two" type="hidden" value="2">					
                        </input>
						<input name="shop" type="hidden" value="{{ $shop }}">					
                        </input>
                        <button data-version="two" class="plan_select">Select</button>
                    <!--/form-->
                </div>
            </div>		
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Note</h4>
			</div>
			<div class="modal-body">
				<p><b></b></p>
			</div>
			<div class="modal-footer">
				<div class="text-left">
					<input type="checkbox" name="confirm_usage_charge" id="confirm_usage_charge"></input> Check this box if you agree to proceed.
				</div>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script>
	var trial_days = "{{$trial_days}}";
    console.log(trial_days);
    var url;
	$(".plan_select").click(function(){		
		var version = $(this).data("version");
		if(version == "two")
		{
			$(".modal-body").html("User charges will get apply for advance version during trial period according to the shopify terms and structure of charges. Develpement Merchants do not have any rights or hold on this Methodology.");
			url = "advance?two=2";
		}		
		if(parseInt(trial_days) > 0)
		{
			$('#myModal').modal('show');
			$("#confirm_usage_charge").change(function(){
				if($(this).prop("checked"))
				{
					$.ajax({
						url:"send-confirm-email",
						async:false,
						data:{shop:"{{$shop}}"},
						success:function(result){

						}
					});		
					window.location.href = url;		
				}
			});							
		}
		else
		{
			window.location.href = url;	
		}
	});
</script>
<style>
	input#confirm_usage_charge {
    	margin-right: 4px;
	}
	#confirm_usage_charge {
    	visibility: initial !important;
	}
</style>
@endsection
