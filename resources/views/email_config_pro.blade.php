@extends('headerpro')
@section('content')
<style>
    /* Outer */
    .popup {
        width:100%;
        height:100%;
        display:none;
        position:fixed;
        top:0px;
        left:0px;
        background:rgba(0,0,0,0.50);
    }

    /* Inner */
    .popup-inner {
        max-width:500px;
        width:90%;
        padding:40px;
        position:absolute;
        top:50%;
        left:50%;
        -webkit-transform:translate(-50%, -50%);
        transform:translate(-50%, -50%);
        box-shadow:0px 2px 6px rgba(0,0,0,1);
        border-radius:3px;
        background:#fff;
    }

    /* Close Button */
    .popup-close {
        width:30px;
        height:30px;
        padding-top:4px;
        display:inline-block;
        position:absolute;
        top:0px;
        right:0px;
        transition:ease 0.25s all;
        -webkit-transform:translate(50%, -50%);
        transform:translate(50%, -50%);
        border-radius:1000px;
        background:rgba(0,0,0,0.8);
        font-family:Arial, Sans-Serif;
        font-size:20px;
        text-align:center;
        line-height:100%;
        color:#fff;
    }

    .popup-close:hover {
        -webkit-transform:translate(50%, -50%) rotate(180deg);
        transform:translate(50%, -50%) rotate(180deg);
        background:rgba(0,0,0,1);
        text-decoration:none;
    } 
    div#pop-up {
        z-index: 999;
    }
</style>  
<?php
if (!session('shop')) {
    $shop = session('shop');
} else if(isset($_REQUEST['shop'])) {
    $shop = $_REQUEST['shop'];
}else{
    $shop = $shop;
}        
?>

<script type="text/javascript">
    ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
    title: 'General Settings',
            buttons: {
            secondary: [
            {
            label: 'Dashboard',
                    href : '{{route('dashboard')}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'Donations',
                    href : '{{route($add_donation_route,$product_id)}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'Track Donation',
                    href : '{{route('track_donationpro')}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'Help',
                    href : '{{route('help_pro')}}?shop=<?php echo $shop; ?>',
                    loading: true
            }
            ]
            }
    });
    });</script>
<script>
   $().ready(function() {

    $("#emailSubmit").validate({
            ignore: [],
            debug: false,
            rules: {
            email_content:{
                required: function() 
                {
                     if($("#send_email").is(':checked')){
                        CKEDITOR.instances.email_content.updateElement();
                     } else {
                         return false;
                     }
                },
                    minlength:10
                },     
            },
            submitHandler: function(form) {
                $(".submit-loader").attr("disabled", "disabled");
                $(".btn-loader-icon").css("display", "block");
                $(".submit-loader").css("padding-left", "25px");
                return true;
            },
                messages: {
                    'title': {
                            required: " Donation Title field is required",
                    },
                    'email_content': {
                            required: "Email Content field is required",
                            minlength:"Please enter 10 characters"
                    },
            },
            errorPlacement: function ($error, $element) {
            var name = $element.attr("name");
            $("#errors-" + name).append($error);
            }
    });
    $(".btnClose").click(function(){
    $(".alertHide").hide();
    });
    });
</script>    
<main class="full-width general-setting">
    <header>
        <div class="container">
            <div class="adjust-margin">
                @if(session()->has('success'))
                <div class="alert success alertHide">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Success</dt>
                        <dd>{{ Session('success') }}</dd>
                    </dl>
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert error alertHide">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Error</dt>
                        <dd>{{ Session('error') }}</dd>
                    </dl>
                </div>
                @endif
            </div>
        </div>     
        
        <div class="container">
            
            <div id="pop-up">                
                <div class="popup" data-popup="popup-1" style="display: {{ (!$setting) ? 'block' : 'none' }}">
                    <div class="popup-inner">
                        <h2>Alert</h2>
                        <p>You have to Add Donation First. After that you can access the General Settings.</p>                        
                        <a href="{{route($add_donation_route,$product_id)}}"><button class="btn btn-primary submit-loader" type="submit" name="save"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon" style="display:none;"></i>Add Donation</button></a>
                    </div>                    
                </div>                
            </div>

            <div class="adjust-margin toc-block">
                <h1 class="toc-title">General Settings</h1>
                <p class="toc-description">Set Title, CSS, Message and look and feel of the donation section which will be visible in the front side. You can change this according to your theme and requirement.</p>
            </div>
        </div>	
    </header>
    {!! Form::open(['route' => ['email_config_pro_save', 'shop' => $shop],'id' => 'emailSubmit', 'method'=> 'post','class' =>'submitForm']) !!}

    <!-- Settings Start -->
    <section>
        <aside>
            <!--<h2>General Settings</h2>-->
        </aside>
        <article>
            <div class="card">
                <div class="mb-3">
                    <label>Enable App?</label>       
                    <select name="status" id="status">
                        <option value="1" {{ (isset($setting->status) && $setting->status == 1)?'selected="selected"':'' }} >Enable</option>
                        <option value="0" {{ (isset($setting->status) && $setting->status == 0)?'selected="selected"':'' }} >Disable</option>
                    </select>
                </div>

                <div class="mb-3">
                    <label>Title for Donation Section</label>                    
                    <input type="text" name="title" id="title" placeholder="Donation" value="{{ isset($setting->title) ? $setting->title : '' }}" maxlength="200"/>
                    <div id="errors-title"></div>
                    <p>Maximum 200 characters allowed.</p>
                </div>

                <div class="mb-3">
                    <label>Donation Button Text</label>                    
                    <input type="text" name="donation_button_text" id="donation_button_text" value="{{ isset($setting->donation_button_text) ? $setting->donation_button_text : 'Donate' }}" maxlength="200"/>
                    <p>Maximum 200 characters allowed.</p>
                </div>

                <div class="mb-3">
                    <label>Additional CSS</label>               
                    <textarea name="additional_css" id="additional_css" rows="3">{{ isset($setting->additional_css) ? $setting->additional_css : '' }}</textarea>
                    <p><strong>Note:</strong>The donate button will by default use store theme as CSS, but you can add extra CSS For Example: .body {margin:0px;}. from here.
                        <br>If you want us to help you out here just drop us an email at <a href = "mailto:support@zestard.com">support@zestard.com</a></p>                    
                </div>

                <div class="mb-3">                    
                    <label><input type="checkbox" name="send_email" id="send_email" value="1" {{ (isset($setting->send_email) && $setting->send_email == 1)?'checked="checked"':'' }}>Do you want to receive e-mail notification?</label>
                </div>

                <div class="mb-3">                    
                    <label><input type="checkbox" name="show_popup" id="show_popup" value="1" {{ (isset($setting->show_popup) && $setting->show_popup == 1)?'checked="checked"':'' }}>Do you want to show a 'Thank you' note/message to customers who donate?</label>
                </div>

                <div class="mb-3">                    
                    <input type="text" name="popup_message" id="popup_message" placeholder="Thanks For Donating!" value="{{ isset($setting->popup_message) ? $setting->popup_message : 'Thanks For Donating!' }}"/>
                    <p>Write your message above and it will be displayed whenever the customer donates.</p>
                </div>

            </div>
        </article>               
    </section>
    <!-- Settings End -->
    <hr class="hr-custom"/>
    <div class="container">
        <div class="adjust-margin toc-block">
            <h1 class="toc-title">Email Configurations</h1>
            <p class="toc-description">Please fill up the information asked below to configure the donation receipt functionality which gets sent in form of emails.</p>
        </div>
    </div>
    <section>
        <aside>
            <?php /* <h2>To</h2>It indicates address of the customer who makes donation and to whom mail is sent.
              <h2>From</h2>Sets source person email address who is sending mail.
              <h2>Cc</h2>Provides the information of the person with whom copy of email is shared. */ ?>
        </aside> 
        <article>
            <div class="card">
                <div class="mb-3">
                    <label>To:</label>                    
                    <p>This field will consider email address of customer to whom donation receipt is supposed to be sent.</p>
                </div>

                <div class="mb-3">
                    <label>From:</label>                    
                    <input type="email" name="from_email" id="from_email" value="{{ isset($data->admin_email) ? $data->admin_email : '' }}" required=""/>
                    <p>It indicates the information of the source person's email address. In default it will consider address of store owner which can be replaced if it is required to sent email from different email address.</p>
                </div>

                <div class="mb-3">
                    <label>CC:</label>                    
                    <input type="email" name="cc_email" id="cc_email" value="{{ isset($data->cc_email) ? $data->cc_email : '' }}"/>
                    <p>Please use this field to input the email address of the person to whom you would like to sent copy of the donation receipt.</p>
                </div>
            </div>
        </article>        
    </section>
    <section class="border-none">
        <aside>
            <h2 class="section-aside-h2">Subject</h2>
            <p>It provides information for the purpose of sending email.</p>
        </aside>
        <article>
            <div class="card">
                <label>Subject</label>
                <div class="mb-3">                    
                    <input type="text" name="email_subject" id="email_subject" value="{{ isset($data->email_subject) ? $data->email_subject : '' }}" required="" />
                </div>                
            </div>
        </article>
    </section>
    <section class="border-none">
        <aside>
            <h2 class="section-aside-h2">Email Template</h2>
            <?php /* <p>Donation receipt has been sent to customers on making donation in the store.A format of donation receipt has been showed here.</p> */ ?>
            <p>Receipt Format</p>
        </aside>
        <article>
            <div class="card">
                <label>Email Template</label>
                <div class="mb-3">                    
                    <textarea id="email_content" name="email_content" required>{!! isset($data->html_content) ? $data->html_content : '' !!}</textarea>
                    @ckeditor('email_content')
                    <div id="errors-email_content"></div>
                </div>
                <button class="btn btn-primary submit-loader-email-default" type="button" name="DefaultEmail" id="DefaultEmail"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon-email" style="display:none;"></i>Default Email</button>
            </div>
        </article>                
    </section>  
    <hr class="hr-custom"/>
    <section class="border-none">

        <article class="save-btn">
            <button class="btn btn-primary submit-loader" type="submit" name="save"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon" style="display:none;"></i>Save</button>
        </article>
    </section>
    {!! Form::close() !!}
    <footer></footer>
</main>

<script type="text/javascript">
    $("#DefaultEmail").click(function () {        
        $(".submit-loader-email-default").attr("disabled", "disabled");
        $(".btn-loader-icon-email").css({"display": "block", "float": "left", "margin": "3px 7px 0 0"});
        $.ajax({
                url: '{{ route('set_default_email') }}',
                type: "POST",
                data: {_token: '{{ csrf_token() }}'},
                success: function (data) {
                    location.reload();                     
                }
        });        
    });
</script>

@endsection