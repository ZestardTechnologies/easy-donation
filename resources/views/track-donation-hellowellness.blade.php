@extends('header')

@section('content')
<script type="text/javascript">
	ShopifyApp.ready(function(e){
		ShopifyApp.Bar.initialize({
			title: 'Track Donation',
				buttons: {
				secondary: [{
					label: 'Dashboard',
					href : '{{url('/dashboard')}}',	
					/* href : 'dashboard?shop={{ $shop }}', */
					loading: true
				},
				{
					label: 'Email Configuration',
					href : '{{url('/email_configuration')}}',	
					/* href : 'email_configuration?shop={{ $shop }}', */
					loading: true
				}, 
				{
					label: 'Help',
					href : '{{url('/help')}}',	
					/* href : 'help?shop={{ $shop }}', */
					loading: true
				}]
			}
		});
	});
</script>
<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script src="https://zestardshop.com/shopifyapp/easy_donation/public/js/ckeditor.js"></script>

<?php //echo "<pre/>"; print_r($orderCollection->orders[0]->id);exit;  ?>

<div class="dashboard">

    <div class="donation-container">    

        <div class="col-md-12 left-content-setting">

            <div class="subdiv-content settings trackset">

                <h2 class="sub-heading">Track Number of Donation</h2>

                <form action="" id="track-submit" method="get">
				<input type="hidden" name="shop" value="{{ $shop }}"/>
                    <div class="col-md-12 form-group">

                        <label for="track">Select Year</label>

                        <select class="form-control trackfield" id="track" name="track">	          

                            @for ($i = 2017; $i <= 2025; $i++)
                            
                            <option value="{{$i}}" <?php echo ($trackNumber == $i) ? 'selected' : ''; ?>>{{$i}}</option>            

                            @endfor

                        </select>

                        <button class="btn btn-primary" type="submit" name="save">Submit</button>



                        @if($trackNumber == "")

                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto; display:none;"></div></div>

                    @else

                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div></div>

            @endif

        </div>	       

        </form>	    



        <?php
        $i = 0;

        //$monthValue = "0,0,0,0,0,0,0,0,0,0,0,0";
        // if($shop == "hellowellness-com.myshopify.com")
        // {       
        //     $total = 0;         
        //     foreach ($orderCollection as $all_orders) {    
        //         foreach ($all_orders['orders'] as $orderData) {                
        //         foreach ($orderData->line_items as $productData) {            
        //             if(strpos($productData->name, "Donate To Sloan Kettering - Cancer Research And Treatment Center") !== false)
        //             {
        //                 $date = $orderData->created_at;                                        
        //                 $explDate = explode('T', $date, -1);
        //                 echo $implDate = implode(" ", $explDate);
        //                 $dateValue = strtotime($implDate);
        //                 $month = date("M", $dateValue);
        //                 echo " <br> ";

        //                 // if($productData->product_id == "1230056816692")
        //                 // {
        //                 //     $total = $total + $productData->price;
        //                 // }                        
        //             }
        //         }
        //     }
        // }
        //     // echo $total;
        //     //die;
        // }                

        $newOrderCollection = array();

        $monthArray = array("Jan" => 0, "Feb" => 0, "Mar" => 0, "Apr" => 0, "May" => 0, "Jun" => 0, "Jul" => 0, "Aug" => 0, "Sep" => 0, "Oct" => 0, "Nov" => 0, "Dec" => 0);

        $matchMonth = (array_keys($monthArray));

        //echo "<pre/>"; print_r($monthArray);
        
        
        
        foreach($orderCollection as $all_orders) {

            foreach ($all_orders['orders'] as $orderData) {
    
            
            foreach ($orderData->line_items as $productData) {
                
                $donate_id = $productData->product_id;
                
                

                $date = $orderData->created_at;
                
                //$date = substr($date,0,10);

                $explDate = explode('T', $date, -1);

                $implDate = implode(" ", $explDate);

                $dateValue = strtotime($implDate);

                $year = date("Y", $dateValue);

                $month = date("M", $dateValue);
  

                if ($donate_id == $donate_product_id && $year == $trackNumber) {

                    $newOrderCollection[$i]['order_id'] = $orderData->id;

                    $newOrderCollection[$i]['date'] = $implDate;

                    $newOrderCollection[$i]['product_id'] = $donate_id;

                    $newOrderCollection[$i]['amount'] = $productData->price * $productData->quantity;

                    

                    for ($j = 0; $j < 12; $j++) {
                        
                        if ($matchMonth[$j] == $month) {

                            $monthArray[$month] = $monthArray[$month] + $newOrderCollection[$i]['amount'];

                            //echo "<pre/>"; print_r($monthArray);                        		
                            //echo "<pre/>"; print_r($monthValue);
                        }
                    }

                    $i++;
                }
            }
        }
    }
        $monthValue = implode(",", $monthArray);
       
        ?>







    </div>

</div>

</div>

</div>





<script type="text/javascript">

    jQuery('#track-submit').submit(function(event) {

    var trackNo = jQuery('#track', this).val();
    //console.log(val);

    //jQuery('.result').html(trackNo);



    });</script>



<script type="text/javascript">

    Highcharts.chart('container', {

    chart: {

    type: 'line'

    },
            title: {

            text: 'Track Doantion January, <?php echo $trackNumber; ?> to December, <?php echo $trackNumber; ?>'

            },
            xAxis: {

            categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

            },
            yAxis: {

            title: {

            text: 'Amount of Donation'

            }

            },
            plotOptions: {

            line: {

            dataLabels: {

            enabled: true

            },
                    enableMouseTracking: false

            }

            },
            series: [{

            name: 'Total amount of Donation in every month of <?php echo $trackNumber; ?>',
                    data: [<?php echo $monthValue; ?>]

            }, ]

            });

</script>

@endsection



