<div id="preview_product_container" class="product_fontpreview" style="display: none">
	<div id="dropdown_product_preview" style="display: none">
		<form name="product_dropdownform" id="product_dropdownform">
			<div class="product-price-slider">                
				<select name="donation_product_amount" id="donation_product_amount" class="donation_product_amount"></select>  
				<input type="number" name="dropdown_product_textbox" id="dropdown_product_textbox" step="any" min="1" style="display:none;"/>
				<input type="submit" name="donate" value="Donate" class="btn button donation_product_button"/>          
				<div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
			</div>
		</form>
	</div>	
	<div id="textbox_product_preview" style="display: none">
		<form name="product_textbox_form" id="product_textbox_form">
			<div class="product-price-slider">  
				<label id="minimum_product_donation" for="minimum_product_donation" style="display:none;"></label>
				<input type="number" name="textbox_product_amount" id="textbox_product_amount" step="any" min="1" required/>
				<input type="submit" name="donate" value="Donate" class="btn button donation_product_button"/>          
				<div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
			</div>          
		</form>
	</div>
	<div id="pricebar_product_preview" style="display: none">
		<div class="product-price-box">
			<form name="product_pricebarform" id="product_pricebarform">
				<div class="product-price-slider">                    
					<input type="range" id="product_pricebar_slider" />          
				</div>
				<div class="product-price-form">
					<label class="amountsize" for="amount">Amount: </label> 
					<input type="hidden" id="amount">
					<p class="price lead" id="amount-product-label"></p>
					<p class="price">.00</p>
				</div>
				<div class="price-slider_button">
					<button type="submit" class="btn button donation_product_button">Donate</button>            
					<div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
				</div>
			</form>
		</div>
	</div>
</div>
<div id="preview_container" class="fontpreview" style="display: none">
    <div id="dropdown_preview" style="display: none">
        <form name="dropdownform" id="dropdownform">
            <div class="price-slider">
                <h3 class="display_title"></h3>
                <div class="donation_content">
                    <p class="product-image"></p>
                    <p class="display_description"></p>
                </div>
                <select name="donation_amount" id="donation_amount" class="donationamount"></select> 
                <input type="number" name="dropdown_textbox" id="dropdown_textbox" step="any" min="1" style="display:none;"/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>
        </form>
    </div>
    
    <div id="textbox_preview" style="display: none">
        <form name="textbox_form" id="textbox_form">
            <div class="price-slider">
                <h3 class="display_title"></h3>
                <div class="donation_content">
                    <p class="product-image"></p>
                    <p class="display_description"></p>
                </div>
                <label id="minimum_donation" for="minimum_donation" style="display:none;"></label>
                <input type="number" name="textbox_amount" id="textbox_amount" step="any" min="1" required/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>          
        </form>
    </div>
    
    <div id="pricebar_preview" style="display: none">
        <div class="price-box">
            <form name="pricebarform" id="pricebarform">
                <div class="price-slider">
                    <h3 class="display_title"></h3>
                    <div class="donation_content">
                        <p class="product-image"></p>
                        <p class="display_description"></p>
                    </div>
                    <input type="range" id="pricebar_slider" />          
                </div>
                <div class="price-form">
                    <label class="amountsize" for="amount">Amount: </label> 
                    <input type="hidden" id="amount">
                    <p class="price lead" id="amount-label"></p>
                    <p class="price">.00</p>
                </div>
                <div class="price-slider_button">
                    <button type="submit" class="btn button donation_button">Donate</button>            
                    <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
                </div>
            </form>
        </div>
    </div>
</div>