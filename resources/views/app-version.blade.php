@extends('headerpro')
@section('content')
<script>
    $(document).ready(function () {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('.pricing_table_div').offset().top
        }, 'slow');
    });
</script>
<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script src="https://zestardshop.com/shopifyapp/easy_donation/public/js/ckeditor.js"></script>

<link rel="stylesheet" href="{{ asset('css/custom.css') }}"/>


<main class="full-width">

    <header>        
        <div class="container">
            <div class="adjust-margin toc-block">
                <h1 class="toc-title">Select Package</h1>
                <p class="toc-description">Package Description</p>
            </div>
        </div>	
    </header>

    <div class="container formcolor">
        <div class="pricing_table_block">
            <div class="pricing_table_div">
                <div class="pricing_table_item margin_right">
                    <h1 class="pricing_table_title">Basic</h1>
                    <div class="pricing_table_price_div">
                        <span class="doller_icon">$</span><h2>4.99</h2><span class="month_text"> / m</span>
                    </div>

                    <div class="pricing_table_list text-height">
                        <ul>
                            <li>Single Donation option.</li>
                            <li>Option to modify Look & Feel of the donation section.</li>
                            <li>Ability to format & design Donation Receipt Email Template.</li>
                            <li>Donation Receipt Emails are sent to customers.</li>
                            <li>Merchants can receive the Copy of Donation Receipt.</li>
                            <li>Ability to Track the donations year/month wise. </li>
                            <li>Facility to Download/Print the donation collected in different formats. </li>                                                        
                        </ul>
                    </div>

                    <div class="select_btn">				
                        <form action="basic" method="post" id="basicPlan">
                            {{ csrf_field() }}
                            <input name="one" type="hidden" value="1">					
                            </input>
                            <input name="shop" type="hidden" value="{{ $shop }}">					
                            </input>                            
                            <button class="plan_select submit-loader-goback basicPlan" type="submit"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon-go-back" style="display:none;"></i>Select</button>
                        </form>
                    </div>
                </div>

                <div class="pricing_table_item pricing_middle_item margin_right">
                    <div class="ribbon ribbon-small text-white">
                        <div class="ribbon-content bg_pink text-uppercase text_white">We Recommend</div>
                    </div>
                    <h1 class="pricing_table_title">Advance</h1>
                    <div class="pricing_table_price_div">
                        <span class="doller_icon">$</span><h2>7.99</h2><span class="month_text"> / m</span>
                    </div>

                    <div class="pricing_table_list">
                        <ul>
                            <li>Create Multiple donations.</li>
                            <li>Option to modify Look & Feel of the donation section.</li>
                            <li>Ability to format and design Donation Receipt Email Template.</li>
                            <li>Add details like descriptions and images seperately for each donations. </li>
                            <li>Customers can keep "Thank You" message of their own choice. </li>
                            <li>Donation Receipt Emails are sent to customers.</li>
                            <li>Merchants can receive the Copy of Donation Receipt.</li>
                            <li>Ability to Track donations based on particular organization. </li>
                            <li>Facility to Download/Print the donations collected in different formats based on organization.</li>                            
                        </ul>
                        <p class="usage_note"><b>Note:</b><small> Usage charges will apply during free trial for this version.</small></p>
                    </div>

                    <div class="select_btn">				
                        <form action="advance" method="post" id="advancePlan">
                            {{ csrf_field() }}
                            <input name="two" type="hidden" value="2">					
                            </input>
                            <input name="shop" type="hidden" value="{{ $shop }}">					
                            </input>
                            <button class="plan_select submit-loader-advanceback advancePlan" type="submit"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon-advanceback" style="display:none;"></i>Select</button>
                        </form>
                    </div>
                </div>		
            </div>
        </div>
    </div>
</main>
<script>
    $().ready(function () {
        $(".basicPlan").click(function () {
            $(".submit-loader-goback").attr("disabled", "disabled");
            $(".btn-loader-icon-go-back").css({"display": "block", "float": "left", "margin": "3px 7px 0 0"});           
            //return false;  
            $("#basicPlan").submit();   
        });
        
        $(".advancePlan").click(function () {
            $(".submit-loader-advanceback").attr("disabled", "disabled");
            $(".btn-loader-icon-advanceback").css({"display": "block", "float": "left", "margin": "3px 7px 0 0"});           
            //return false;  
            $("#advancePlan").submit();   
        });
    });
</script>
@endsection
