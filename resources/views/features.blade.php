@extends('header')
@section('content')
<style>
    .top-marquee{
        display:none;
    }
</style>
<script type="text/javascript">
    ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
    buttons: {
    secondary: [{
    label: 'Dashboard',
            href : '{{ url(' / dashboard') }}',
            loading: true
    }, {
    label: 'Email Configuration',
            href : '{{ url('email_configuration') }}',
            loading: true
    }, {
    label: 'Track Donation Amount',
            href : '{{ url('track_donation') }}',
            loading: true
    }, {
    label: 'Help',
            href : '{{ url(' / help') }}',
            loading: true
    }]
    }
    });
    });
    $(document).ready(function () {
    // Handler for .ready() called.
    $('html, body').animate({
    scrollTop: $('.pricing_table_div').offset().top
    }, 'slow');
    });</script>
<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script src="https://zestardshop.com/shopifyapp/easy_donation/public/js/ckeditor.js"></script>

<link rel="stylesheet" href="{{ asset('css/custom.css') }}"/>
<div class="container formcolor">
    <div class="pricing_table_block">
        <div class="pricing_table_div">
            <div class="pricing_table_item margin_right">
                <h1 class="pricing_table_title">Basic</h1>
                <div class="pricing_table_price_div">
                    <span class="doller_icon">$</span><h2>2.99</h2><span class="month_text"> / m</span>
                </div>
                <div class="pricing_table_list">
                    <ul>
                        <li>Single Donation option</li>
                        <li>Donation image change Feature</li>
                        <li>User will get donation recipet through email</li>
                        <li>Per month Track Donation functionality</li>
                    </ul>
                </div>
                <div class="select_btn">				
                    <form action="basic" method="post">
                        {{ csrf_field() }}
                        <input name="one" type="hidden" value="1">					
                        </input>
                        <button class="plan_select">Select</button>
                    </form>
                </div>
            </div>
            <div class="pricing_table_item pricing_middle_item margin_right">
                <div class="ribbon ribbon-small text-white">
                    <div class="ribbon-content bg_pink text-uppercase text_white">We Recommend</div>
                </div>
                <h1 class="pricing_table_title">Advance</h1>
                <div class="pricing_table_price_div">
                    <span class="doller_icon">$</span><h2>4.99</h2><span class="month_text"> / m</span>
                </div>
                <div class="pricing_table_list">
                    <ul>
                        <li>Multiple Donation option</li>	
                        <li>Donation image change Feature</li>
                        <li>User will get donation recipet through email</li>
                        <li>Per month Track Donation functionality per donation</li>	
                    </ul>
                </div>
                <div class="select_btn">				
                    <form action="advance" method="post">
                        {{ csrf_field() }}
                        <input name="two" type="hidden" value="2">					
                        </input>
                        <button class="plan_select">Select</button>
                    </form>
                </div>
            </div>		
        </div>
    </div>
</div>
</div>
@endsection
