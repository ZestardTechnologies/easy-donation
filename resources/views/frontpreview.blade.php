<!--script type="text/javascript" src="{{ asset('js/latest_jquery.min.js') }}"></script-->
<script src="{{ asset('js/latest_jquery-ui.js') }}" type="text/javascript"></script>
<link href="{{ asset('css/latest_jquery-ui.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/frontpreview.css') }}" rel="stylesheet" type="text/css" />
<script>
var base_path_easydonation = "https://zestardshop.com/shopifyapp/easy_donation/public/";
var variant_ids, product_id;
var shop_name = Shopify.shop, popup_message;
var shop_id = "{{ $id }}";
$zestard_easy = window.jQuery;
$zestard_easy(document).ready(function () {
	if(shop_name == "help-get-fun-size-and-jesters-to-usa.myshopify.com")
	{
		$zestard_easy(".fontpreview").css("padding","75px");
		$zestard_easy(".fontpreview").css("border","1px solid #e9e9e9");
		$zestard_easy(".fontpreview").css("box-shadow","none");
		$zestard_easy(".display_description p").css("font-size","13px");		
	}
    var shop_id = '<?php echo $id; ?>';
    var page = '<?php echo $page; ?>';
    var additional_css = "";	
    var product_image = " <?php echo $images_json ?> "; 
    setdata = <?php echo $donation_settings; ?>;
    popup_message = setdata.popup_message;
    show_popup = setdata.show_popup;   
	$(".donation_loader").hide();	
    /*
		$zestard_easy.ajax({
        type: "GET",
        url: base_path_easydonation + "front_preview",
        data: {
            id: shop_id,
            shop_name: shop_name
        },
        success: function (data){ 
            setdata = JSON.parse(data);                        
	*/            
            if (setdata) {
                var text_dropdown_other = setdata.text_dropdown_other
                additional_css = setdata.additional_css;
                $zestard_easy('.easy_donation_additional_css').html("<style>" + additional_css + "</style>");   
                $zestard_easy('#thank_you_message').html(popup_message);             
                if (setdata.select_page == 2 && page == "product" || setdata.select_page == 3 && page == "product" ) {
					/* if(shop_name == "dedend.myshopify.com")
					{						
						if($("[itemprop=offers]"))
						{
							$("[itemprop=offers]").css("display", "none");
						}
						else
						{
							$("[itemprop=price]").css("display", "none");

							var button = jQuery('button[type=submit][name=add]');
							if (button.length == 0)
							{
								button = jQuery('input[type=submit][name=add]');

							}
							if (button.length == 0)
							{
								var f = jQuery('form[action="/cart/add"]');
								button = jQuery(f).find(':submit');
							}
							$(button).css("display", "none");
						} 	 
						*/		                    		
                    if(shop_name == "pandere-shoes.myshopify.com")
                    {
                        $(".product-form").css("display", "none");
                    }
                    if(shop_name == "serajeymonastery.myshopify.com")
                    {
                        if(vendor == "zestard-easy-donation")
                        {
                            $("[itemprop=offers]").css("display", "none");
                        }
                    }
                    else
                    {
                        if($("[itemprop=offers]"))
                        {
                            $("[itemprop=offers]").css("display", "none");
                        }  
                    }
                    if (setdata.field_option == "P") {
                        $zestard_easy('#preview_product_container').css('display', 'inline-block');
                        $zestard_easy("#textbox_product_preview").remove();
                        $zestard_easy("#dropdown_product_preview").remove();
                        $zestard_easy('#pricebar_product_preview').css('display', 'block');
                        
                        var min_val = parseInt(setdata.bar_min);
                        var max_val = parseInt(setdata.bar_max);                        
                        
                        if(setdata.title){
                            $zestard_easy(".easy_donation_title").text(setdata.title);
                        } else {
                            $zestard_easy(".easy_donation_title").text("Donation");
                        }
                        
                        if (setdata.donation_button_text) {                            
                            $zestard_easy(".donation_product_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_product_button").attr("value", "Donate");
                        }
                        $zestard_easy("#product_pricebar_slider").val(min_val);
                        $zestard_easy("#product_pricebar_slider").attr("min", min_val);
                        $zestard_easy("#product_pricebar_slider").attr("max", max_val);
                        $zestard_easy("#amount-product-label").html(setdata.shop_currency + min_val);
                        $zestard_easy("#product_pricebar_slider").change(function () {
                            var amounttext = $zestard_easy("#product_pricebar_slider").val();
                            $zestard_easy("#amount-product-label").html(setdata.shop_currency + amounttext);
                        });
						if(shop_name == "free-theme-test.myshopify.com")
						{
							var price_tip = '<div id="price_map" style="display: block;width: 160px;margin: 10px 50px;position: relative;">'+
							'<span style="position: absolute;left: 0;">$' + min_val + '</span>'+
							'<span style="position: absolute;left: 40%;">$' + Math.floor((parseInt(min_val) + parseInt(max_val)) / 2) + '</span>'+
							'<span style="position: absolute;right: 0;">$' + max_val + '</span>'+
							'</div>';   
							$(".price-slider").find("input[type='range']").after(price_tip);
							$(".price-slider").find("input[type='range']").val(Math.floor((parseInt(min_val) + parseInt(max_val)) / 2));
						}
                    } 
					else if (setdata.field_option == "D") 
					{
						$zestard_easy('#preview_product_container').css('display', 'inline-block');
                        $zestard_easy("#textbox_product_preview").remove();
                        $zestard_easy("#pricebar_product_preview").remove();
                        $zestard_easy('#dropdown_product_preview').css('display', 'block');
                        var option_array = JSON.parse(setdata.dropdown_option);
                        
                        
                        if(setdata.title){
                            $zestard_easy(".easy_donation_title").text(setdata.title);
                        } else {
                            $zestard_easy(".easy_donation_title").text("Donation");
                        }
                        
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_product_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_product_button").attr("value", "Donate");
                        }
                        var j = 0;                        
                        if (setdata.variant_ids)
                        {
                            variant_ids = JSON.parse(setdata.variant_ids);
                            if(shop_name == "pandere-shoes.myshopify.com")
                            {
                                $zestard_easy.each(option_array, function (j) {
                                    $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                        value: variant_ids[j],
                                        text: "Donate " + option_array[j] + " to LE&RN"
                                    }));
                                    j++;
                                });
                            }
                            else
                            {
                                $zestard_easy.each(option_array, function (j) {
                                    $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                        value: variant_ids[j],
                                        text: option_array[j]
                                    }));
                                    j++;
                                });
                            }        
                            // Donate $1 to LE&RN
                            if(setdata.dropdown_other == 1){
                                 $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                    id: "click_drop_down",
                                    value: "other",
                                    text: text_dropdown_other
                                }));
                            }
                        } 
                        else
                        {
                            if(shop_name == "pandere-shoes.myshopify.com")
                            {
                                $zestard_easy.each(option_array, function (j) {
                                    $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                        value: option_array[j],
                                        text: "Donate" + option_array[j] + "to LE&RN"
                                    }));
                                    j++;
                                });
                            }
                            else
                            {
                                $zestard_easy.each(option_array, function (j) {
                                    $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                        value: option_array[j],
                                        text: option_array[j]
                                    }));
                                    j++;
                                });
                            }
                            if(setdata.dropdown_other == 1){
                                 $zestard_easy('#donation_product_amount').append($zestard_easy('<option>', {
                                    id: "click_drop_down",
                                    value: "other",
                                    text: text_dropdown_other
                                }));
                            }
                        }
                    } else if (setdata.field_option == "T") {
                           
                        $zestard_easy('#preview_product_container').css('display', 'inline-block');
                        $zestard_easy("#pricebar_product_preview").remove();
                        $zestard_easy("#dropdown_product_preview").remove();
                        $zestard_easy('#textbox_product_preview').css('display', 'block');
                        if(setdata.add_min_amount >= 1){
                            //$zestard_easy("#minimum_product_donation").after(setdata.shop_currency + setdata.add_min_amount);
                            $zestard_easy('#minimum_product_donation').css('display', 'block');
                            $zestard_easy("#minimum_product_donation").html("Minimum Donation: " + setdata.shop_currency + setdata.add_min_amount);
                            $zestard_easy("#textbox_product_amount").attr("min", setdata.add_min_amount);                            
                        }else{
                            $zestard_easy('#minimum_product_donation').css('display', 'none');
                        }
                        
                        if(setdata.title){
                            $zestard_easy(".easy_donation_title").text(setdata.title);
                        } else {
                            $zestard_easy(".easy_donation_title").text("Donation");
                        }
                        
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_product_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_product_button").attr("value", "Donate");
                        }
                    } else {
                    }
                }
                else if(setdata.select_page == 1 && page == "cart" || setdata.select_page == 3 && page == "cart") {
                    
                    //for getting the product image
                    /* var shop = setdata.shop_id;
					$zestard_easy.ajax({
                        type: "GET",
                        url: "{{ url('productimage') }}",
                        async: false,
                        data: {
                            'id': shop
                        },
                        success: function (product) {                            
							$(".donation_loader").hide();
                            product_image = JSON.parse(product);                            
                        }
                    }); */                    
					if(product_image)
					{
						$zestard_easy('.product-image').html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image, width: 100}));
					}
					else
					{
						$zestard_easy('.product-image').remove();
					}
                    if (setdata.field_option == "P") {
                        $zestard_easy('#preview_container').css('display', 'inline-block');
                        $zestard_easy("#textbox_preview").remove();
                        $zestard_easy("#dropdown_preview").remove();
                        $zestard_easy('#pricebar_preview').css('display', 'block');
                        
                        var min_val = parseInt(setdata.bar_min);
                        var max_val = parseInt(setdata.bar_max);
                        
                        
                        if(setdata.title){
                            $zestard_easy(".easy_donation_title").text(setdata.title);
                        } else {
                            $zestard_easy(".easy_donation_title").text("Donation");
                        }
                        
                        if (setdata.donation_name) {
                            $zestard_easy(".display_title").text(setdata.donation_name);
                        } else {
                            $zestard_easy(".display_title").css('display', 'none');
                        }
                        if (setdata.description) {
                            $zestard_easy(".display_description").html(setdata.description);
                        } else {
                            $zestard_easy(".display_description").css('display', 'none');
                        }
                        if (setdata.donation_button_text) {                            
                            $zestard_easy(".donation_button").text(setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_button").attr("value", "Donate");
                        }
                        $zestard_easy("#pricebar_slider").val(min_val);
                        $zestard_easy("#pricebar_slider").attr("min", min_val);
                        $zestard_easy("#pricebar_slider").attr("max", max_val);
                        $zestard_easy("#amount-label").html(setdata.shop_currency + min_val);
                        $zestard_easy("#pricebar_slider").change(function () {
                            var amounttext = $zestard_easy("#pricebar_slider").val();
                            $zestard_easy("#amount-label").html(setdata.shop_currency + amounttext);
                        });
                    } else if (setdata.field_option == "D") {
                        $zestard_easy('#preview_container').css('display', 'inline-block');
                        $zestard_easy("#textbox_preview").remove();
                        $zestard_easy("#pricebar_preview").remove();
                        $zestard_easy('#dropdown_preview').css('display', 'block');
                        var option_array = JSON.parse(setdata.dropdown_option);
                        
                        if(setdata.title){
                            $zestard_easy(".easy_donation_title").text(setdata.title);
                        } else {
                            $zestard_easy(".easy_donation_title").text("Donation");
                        }
                        
                        if (setdata.donation_name) {
                            $zestard_easy(".display_title").text(setdata.donation_name);
                        } else {
                            $zestard_easy(".display_title").css('display', 'none');
                        }
                        if (setdata.description) {
                            $zestard_easy(".display_description").html(setdata.description);
                        } else {
                            $zestard_easy(".display_description").css('display', 'none');
                        }
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_button").attr("value", "Donate");
                        }
                        var j = 0;
                        if (setdata.variant_ids)
                        {
                            variant_ids = JSON.parse(setdata.variant_ids);
                            $zestard_easy.each(option_array, function (j) {
                                $zestard_easy('#donation_amount').append($zestard_easy('<option>', {
                                    value: variant_ids[j],
                                    text: option_array[j]
                                }));
                                j++;
                            });
                            if(setdata.dropdown_other == 1){
                                 $zestard_easy('#donation_amount').append($zestard_easy('<option>', {
                                    id: "click_drop_down",
                                    value: "other",
                                    text: text_dropdown_other
                                }));
                            }
                        } else
                        {
                            $zestard_easy.each(option_array, function (j) {
                                $zestard_easy('#donation_amount').append($zestard_easy('<option>', {
                                    value: option_array[j],
                                    text: option_array[j]
                                }));
                                j++;                                
                            });
                            if(setdata.dropdown_other == 1){
                                 $zestard_easy('#donation_amount').append($zestard_easy('<option>', {
                                    id: "click_drop_down",
                                    value: "other",
                                    text: text_dropdown_other
                                }));
                            }
                        }
                    } else if (setdata.field_option == "T") {
                        $zestard_easy('#preview_container').css('display', 'inline-block');
                        $zestard_easy("#pricebar_preview").remove();
                        $zestard_easy("#dropdown_preview").remove();
                        $zestard_easy('#textbox_preview').css('display', 'block');
                        
                        if(setdata.title){
                            $zestard_easy(".easy_donation_title").text(setdata.title);
                        } else {
                            $zestard_easy(".easy_donation_title").text("Donation");
                        }
                        
                        if (setdata.donation_name) {
                            $zestard_easy(".display_title").text(setdata.donation_name);
                        } else {
                            $zestard_easy(".display_title").css('display', 'none');
                        }
                        if (setdata.description) {
                            $zestard_easy(".display_description").html(setdata.description);
                        } else {
                            $zestard_easy(".display_description").css('display', 'none');
                        }                        
                        if(setdata.add_min_amount >= 1){
                            //$zestard_easy("#minimum_donation").after(setdata.shop_currency + setdata.add_min_amount);
                            $zestard_easy("#minimum_donation").css('display', 'block');
                            $zestard_easy("#minimum_donation").html("Minimum Donation: " + setdata.shop_currency + setdata.add_min_amount);
                            $zestard_easy("#textbox_amount").attr("min", setdata.add_min_amount);                            
                        }else{
                             $zestard_easy("#minimum_donation").css('display', 'none');
                        }
                        if (setdata.donation_button_text) {
                            $zestard_easy(".donation_button").attr("value", setdata.donation_button_text);
                        } else {
                            $zestard_easy(".donation_button").attr("value", "Donate");
                        }
                    } else {
                    }
                }
            } 
        /* }
    }); */
    $zestard_easy("#donation_amount").on('change', function(){
        var dropdown_value = $(this).val();
        if(dropdown_value == "other"){
            $zestard_easy('#dropdown_textbox').css('display', 'inline-block');
        }else{
            $zestard_easy('#dropdown_textbox').css('display', 'none');
        }
    });
    $zestard_easy("#donation_product_amount").on('change', function(){
        var dropdown_value = $(this).val();
        if(dropdown_value == "other"){
            $zestard_easy('#dropdown_product_textbox').css('display', 'inline-block');
        }else{
            $zestard_easy('#dropdown_product_textbox').css('display', 'none');
        }
    });
});

function show_loader() {
    $zestard_easy('.loaderDiv').css('display', 'inline-block');
}

function reloadcart() {
    if(shop_name == "dolan-group.myshopify.com"){
        setTimeout(function(){ window.location.reload(true); }, 400);
    }
    else{
        window.location.href = "/cart";
    }
}
$zestard_easy("#product_dropdownform").submit(function (e) {
    
    var data = $zestard_easy("#donation_product_amount").val();
    if(data != "other"){
        if(variant_ids)
        {
            $zestard_easy.ajax({
                url: '/cart/add.js',
                data: {quantity: 1, id: data},
                async: false				
            });
            if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                }
                else
                {    
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }            
        } 
		else
        {
            show_loader();
            $zestard_easy.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/save') }}",
                async: false,
                data: {
                    data: data,
                    'id': shop_id
                },
                beforeSend: function () {
                    show_loader();
                },
                success: function (productId)
                {
                    product_id = productId;
                    $zestard_easy.ajax({
                        url: '/cart/change.js',
                        data: {quantity: 0, id: product_id},
                        async: false
                    });  
					$zestard_easy.ajax({
                        url: '/cart/add.js',
                        data: {quantity: 1, id: product_id},
                        async: false
                    });
                    if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                }
                else
                {    
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }
                }
            });
            /* setTimeout(function(){ window.location.reload(true); }, 400); */
        }
    }
	else
	{
        data = $zestard_easy("#dropdown_product_textbox").val();
        show_loader();
            $zestard_easy.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/save') }}",
                async: false,
                data: {
                    data: data,
                    'id': shop_id
                },
                beforeSend: function () {
                    show_loader();
                },
                success: function (productId)
                {
                    product_id = productId;
					$zestard_easy.ajax({
                        url: '/cart/change.js',
                        data: {quantity: 0, id: product_id},
                        async: false
                    });  
                    $zestard_easy.ajax({
                        url: '/cart/add.js',
                        data: {quantity: 1, id: product_id},
                        async: false
                    });
                    $zestard_easy('.loaderDiv').css('display', 'none');
                    if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                }
                else
                {    
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }
                }
            });
            /* setTimeout(function(){ window.location.reload(true); }, 400); */
    }
    
    e.preventDefault();
});

$zestard_easy("#product_textbox_form").submit(function (e) {
    var data = $zestard_easy("#textbox_product_amount").val();
    var productid = $('.donation').attr('productid');
    $zestard_easy.ajax({
        type: "POST",
        url: "{{ url('frontend/dropdown/save') }}",
        data: {
            data: data,
            productid: productid,
            'id': shop_id
        },
        beforeSend: function () {
            $zestard_easy('.loaderDiv').css('display', 'inline-block');
        },
        success: function (productId) {
            $zestard_easy(".loaderDiv").hide();
            if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                }
                else
                {    
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }
			$zestard_easy.ajax({ url: '/cart/change.js',data: {quantity: 0, id: productId},async: false });  
            $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
        }
    });
    e.preventDefault();
});

$zestard_easy("#product_pricebarform").submit(function (e) {
    var data = $zestard_easy("#product_pricebar_slider").val();
    $zestard_easy.ajax({
        type: "POST",
        url: "{{ url('frontend/dropdown/save') }}",
        data: {
            data: data,
            'id': shop_id
        },
        beforeSend: function () {
            $zestard_easy('.loaderDiv').css('display', 'inline-block');
        },
        success: function (productId) {
            $zestard_easy(".loaderDiv").hide();
            if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                }
                else
                {    
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }
			$zestard_easy.ajax({ url: '/cart/change.js',data: {quantity: 0, id: productId},async: false });  
            $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
        }
    });
    e.preventDefault();
});

$zestard_easy("#dropdownform").submit(function(e){
    var data = $zestard_easy("#donation_amount").val();
    if(data != "other"){
        if (variant_ids)
        {
            $zestard_easy.ajax({
                url: '/cart/add.js',
                data: {quantity: 1, id: data},
                async: false
            });
            //setTimeout(function(){ window.location.reload(true); }, 400);

            //for redirect to checkout
            if(shop_name == "fckcancr-org.myshopify.com"){
                window.location.href = "/checkout";
            }
            else if(shop_name == "brentwood-home.myshopify.com"){             
                window.location.href = "/cart";            
            }
            else{
                if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                }
                else
                {   
                    //location.reload();
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }
            }
        } 
		else
        {
            show_loader();
            $zestard_easy.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/save') }}",
                async: false,
                data: {
                    data: data,
                    'id': shop_id
                },
                beforeSend: function () {
                    show_loader();
                },
                success: function (productId)
                {
                    product_id = productId;					 
                    $zestard_easy.ajax({
                        url: '/cart/add.js',
                        data: {quantity: 1, id: product_id},
                        async: false
                    });
                }
            });
            //setTimeout(function(){ window.location.reload(true); }, 400);
            //for redirect to checkout
            if(shop_name == "fckcancr-org.myshopify.com"){
                window.location.href = "/checkout";
            }
            else{
                reloadcart();
            }
        }
    }else{
        data = $zestard_easy("#dropdown_textbox").val();
        show_loader();
        $zestard_easy.ajax({
            type: "POST",
            url: "{{ url('frontend/dropdown/save') }}",
            async: false,
            data: {
                data: data,
                'id': shop_id
            },
            beforeSend: function () {
                show_loader();
            },
            success: function (productId)
            {				
				show_loader(0);	
                product_id = parseInt(productId);							
				$zestard_easy.ajax({
					url: '/cart/change.js',
					data: {quantity: 0, id: product_id},
					async: false
				}); 
				$zestard_easy.ajax({
					url: '/cart/add.js',
					data: {quantity: 1, id: product_id},
					async: false
				});									
            }
        });
        //setTimeout(function(){ window.location.reload(true); }, 400);

        //for redirect to checkout
        if(shop_name == "fckcancr-org.myshopify.com"){
                window.location.href = "/checkout";
        }
        else if(shop_name == "brentwood-home.myshopify.com"){             
            window.location.href = "/cart";            
        }
        else{
            if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                }
                else
                {    
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }
        }
    }    
    e.preventDefault();
});

$zestard_easy("#textbox_preview").submit(function (e) {
    var data = $zestard_easy("#textbox_amount").val();
    $zestard_easy.ajax({
        type: "POST",
        url: "{{ url('frontend/dropdown/save') }}",
		async:false,
        data: {
            data: data,
            'id': shop_id
        },
        beforeSend: function () {
            $zestard_easy('.loaderDiv').css('display', 'inline-block');
        },
        success: function (productId) {            
            $zestard_easy(".loaderDiv").hide();            
			if(shop_name != "oak-charity-golf-classic.myshopify.com" && shop_name != "brentwood-home.myshopify.com")
			{
                if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                }
                else
                {    
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }
			}
				
			$zestard_easy.ajax({ url: '/cart/change.js',data: {quantity: 0, id: productId},async: false });  
			$zestard_easy.ajax({ url: '/cart/add.js',data: {quantity: 1, id: productId},async: false });              
        }
    });
	if(shop_name == "oak-charity-golf-classic.myshopify.com" || shop_name == "brentwood-home.myshopify.com")
	{
		reloadcart();
	}
    e.preventDefault();
});

$zestard_easy("#pricebarform").submit(function (e) {
    var data = $zestard_easy("#pricebar_slider").val();
    $zestard_easy.ajax({
        type: "POST",
        url: "{{ url('frontend/dropdown/save') }}",
        data: {
            data: data,
            'id': shop_id
        },
        beforeSend: function () {
            $zestard_easy('.loaderDiv').css('display', 'inline-block');
        },
        success: function (productId) {
            $zestard_easy(".loaderDiv").hide();
            if(shop_name == "brentwood-home.myshopify.com")
            {
                
            }
            else
            {
                
                if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                    if(shop_name == "lamporia-com.myshopify.com"){
                        $zestard_easy('body').click(function(){
                            //alert('test');
                            $zestard_easy(".overlay").css({
                                'display': 'none'
                            });
                            reloadcart();
                        });
                    }
                }
                else
                {    
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }
                
            }			
	    $zestard_easy.ajax({ url: '/cart/change.js',data: {quantity: 0, id: productId},async: false });  
            $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
        }
    });
    e.preventDefault();
});
</script>
<div id="donate-popup" class="overlay" style="display: none" align = "center">
    <div class="popup">
        <h2 class="thank_you_message" id="thank_you_message"></h2>
        <a class="donate-close" href="#">&times;</a>        
    </div>
</div>
<script>
if(popup_message)
{
    $(".thank_you_message").html(popup_message);
}
if(shop_name == "lovesilkpillowcase.myshopify.com")
{
	$(".thank_you_message").html("Thank you for making a difference");
}
if(shop_name == "catawba-cultural-preservation-project.myshopify.com")
{
	$(".thank_you_message").html("Thank You For Donating!");
}
if(shop_name == "ciwfnederland.myshopify.com")
{
	$(".thank_you_message").html("Bedankt voor jouw donatie!");
}
</script>

<div id="preview_product_container" class="product_fontpreview" style="display: none">
    <div id="dropdown_product_preview" style="display: none">
        <form name="product_dropdownform" id="product_dropdownform">
            <div class="product-price-slider">                
                <select name="donation_product_amount" id="donation_product_amount" class="donation_product_amount"></select>  
                <input type="number" name="dropdown_product_textbox" id="dropdown_product_textbox" step="any" min="1" style="display:none;"/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_product_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>
        </form>
    </div>
    
    <div id="textbox_product_preview" style="display: none">
        <form name="product_textbox_form" id="product_textbox_form">
            <div class="product-price-slider">  
                <label id="minimum_product_donation" for="minimum_product_donation" style="display:none;"></label>
                <input type="number" name="textbox_product_amount" id="textbox_product_amount" step="any" min="1" required/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_product_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>          
        </form>
    </div>
    
    <div id="pricebar_product_preview" style="display: none">
        <div class="product-price-box">
            <form name="product_pricebarform" id="product_pricebarform">
                <div class="product-price-slider">                    
                    <input type="range" id="product_pricebar_slider" />          
                </div>
                <div class="product-price-form">
                    <label class="amountsize" for="amount">Amount: </label> 
                    <input type="hidden" id="amount">
                    <p class="price lead" id="amount-product-label"></p>
                    <p class="price">.00</p>
                </div>
                <div class="price-slider_button">
                    <button type="submit" class="btn button donation_product_button">Donate</button>            
                    <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="preview_container" class="fontpreview" style="display: none">
    <div id="dropdown_preview" style="display: none">
        <h3 class="easy_donation_title"></h3>
        <form name="dropdownform" id="dropdownform">
            <div class="price-slider">
                <h3 class="display_title"></h3>
                <div class="donation_content">
                    <p class="product-image"></p>
                    <p class="display_description"></p>
                </div>
                <select name="donation_amount" id="donation_amount" class="donationamount"></select> 
                <input type="number" name="dropdown_textbox" id="dropdown_textbox" step="any" min="1" style="display:none;"/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>
        </form>
    </div>
    
    <div id="textbox_preview" style="display: none">
        <h3 class="easy_donation_title"></h3>
        <form name="textbox_form" id="textbox_form">
            <div class="price-slider">
                <h3 class="display_title"></h3>
                <div class="donation_content">
                    <p class="product-image"></p>
                    <p class="display_description"></p>
                </div>
                <label id="minimum_donation" for="minimum_donation" style="display:none;"></label>
                <input type="number" name="textbox_amount" id="textbox_amount" step="any" min="1" required/>
                <input type="submit" name="donate" value="Donate" class="btn button donation_button"/>          
                <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
            </div>          
        </form>
    </div>
    
    <div id="pricebar_preview" style="display: none">
        <h3 class="easy_donation_title"></h3>
        <div class="price-box">
            <form name="pricebarform" id="pricebarform">
                <div class="price-slider">
                    <h3 class="display_title"></h3>
                    <div class="donation_content">
                        <p class="product-image"></p>
                        <p class="display_description"></p>
                    </div>
                    <input type="range" id="pricebar_slider" />          
                </div>
                <div class="price-form">
                    <label class="amountsize" for="amount">Amount: </label> 
                    <input type="hidden" id="amount">
                    <p class="price lead" id="amount-label"></p>
                    <p class="price">.00</p>
                </div>
                <div class="price-slider_button">
                    <button type="submit" class="btn button donation_button">Donate</button>             
                    <div class="loaderDiv" style="display:none;"><img src="https://zestardshop.com/shopifyapp/easy_donation/public/image/loader.svg"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="easy_donation_additional_css"></div>