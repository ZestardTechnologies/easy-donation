var front_card =    '<div id="pricebar_preview_' + i + '"><form id="pricebarform_' + i + '" class="form-horizontal form-pricing" role="form">' +
					'<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div><input type="range" id="pricebar_slider_' + i + '" />' +
					'<div><div class="price-form"><label class="amountsize" for="amount">Amount: </label><input type="hidden" id="amount"><p class="price lead" id="amount-label_' + i + '"></p><p class="price">.00</p></div>' +
					'<input type="submit" class="btn button frontbutton" value="Donate"><div class="loaderDiv_' + i + '" style="display:none;"><img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></div></form></div>';
					$zestard_easy('#preview_product_container').append(front_card);
					$zestard_easy('#dropdown_preview_' + i).remove();
					$zestard_easy('#textbox_preview_' + i).remove();
					$zestard_easy('#pricebar_preview_' + i).css('display', 'block');

					var min_val = parseInt(donation_min);
					var max_val = parseInt(donation_max);
					if (product_id) {
						$zestard_easy('.product_id_' + i).val(product_id);
						product_verify_id = $zestard_easy('.product_id_' + i).val();
					}
					if (donation_name) {
						$zestard_easy('.display_title_' + i).text(donation_name);
					} else {
						$zestard_easy('.display_title_' + i).css('display', 'none');
					}
					if (donation_description) {
						$zestard_easy('.display_description_' + i).html(donation_description);
					} else {
						$zestard_easy('.display_description_' + i).css('display', 'none');
					}
					$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
					$zestard_easy('#pricebar_slider_' + i).val(min_val);
					$zestard_easy('#pricebar_slider_' + i).attr("min", min_val);
					$zestard_easy('#pricebar_slider_' + i).attr("max", max_val);
					$zestard_easy('#amount-label_' + i).html(shop_currency + min_val);
					$zestard_easy('#pricebar_preview_' + i).css('display', 'block');
					pricebarAmount(i);
					submitPricesbar(i);