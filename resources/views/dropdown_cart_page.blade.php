var front_card = '<div id="dropdown_preview_' + i + '"><input type="hidden" name="dropdown_donations[]" value=' + product_id + ' />' +
						'<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
						'<div><select name="dropdown_donation_amounts[]" id="donation_amount_' + i + '" class="donationamount"></select><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/>' +
						'<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
						'<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></div>';						
						$('#preview_container').append(front_card);
						$('#pricebar_preview_' + i).remove();
						$('#textbox_preview_' + i).remove();
						$('#dropdown_preview_' + i).css('display', 'block');
						$('#dropdown_range_' + i).css('display', 'block');
						var option_array = JSON.parse(drop_down);								
						if (product_id) {
							$('.product_id_' + i).val(product_id);
							product_verify_id = $('.product_id_' + i).val();
						}
						if (donation_name) {
							$('.display_title_' + i).text(donation_name);
						} else {
							$('.display_title_' + i).css('display', 'none');
						}
						if (donation_description) {
							$('.display_description_' + i).html(donation_description);
						} else {
							$('.display_description_' + i).css('display', 'none');
						}
						$('.product-image_' + i).html($('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
						var j = 0;						
						if (setdata.variant_ids)
						{
							product_variant_ids = JSON.parse(setdata.variant_ids);
							variant_ids = product_variant_ids[product_id];
							$.map(variant_ids, function (value, index) {
								$('#donation_amount_' + i).append($('<option>', {
									value: value,
									text: option_array[index]
								}));
								j++;
							});
							if(dropdown_other == 1){
								$('#donation_amount_' + i).append($('<option>', {
								   id: "click_drop_down",
								   value: "other",
								   text: text_dropdown_other
							   }));
							}
						} 
						else
						{
							$.each(option_array, function (j) {
								$('#donation_amount_' + i).append($('<option>', {
									value: option_array[j],
									text: option_array[j]
								}));
								j++;
							});
							if(dropdown_other == 1){
								$('#donation_amount_' + i).append($('<option>', {
								   id: "click_drop_down",
								   value: "other",
								   text: text_dropdown_other
							   }));
							}
						}
						submitDropdown(i);