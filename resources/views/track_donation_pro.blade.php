@extends('headerpro')
@section('content')
<?php
        if (!session('shop')) {
            $shop = session('shop');
        } else if(isset($_REQUEST['shop'])) {
            $shop = $_REQUEST['shop'];
        }else{
            $shop = "";
        }        
        ?>
<script type="text/javascript">
    ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
    title: 'Track Donation',
            buttons: {
            primary: {
            label: 'Track Donation Settings Demo',
                    callback: function(){ introJs().start(); }
            },
                    secondary: [
                    {
                    label: 'Dashboard',
                            href : '{{route('dashboard')}}?shop=<?php echo $shop; ?>',
                            loading: true
                    },
                    {
                    label: 'Donations',
                            href : '{{route($add_donation_route,$product_id)}}?shop=<?php echo $shop; ?>',
                            loading: true
                    },
                    {
                    label: 'General Settings',
                            href : '{{route('email_configurationpro')}}?shop=<?php echo $shop; ?>',
                            loading: true
                    },
                    {
                    label: 'Help',
                            href : '{{route('help_pro')}}?shop=<?php echo $shop; ?>',
                            loading: true
                    }
                    ]
            }
    });
    });</script>
<!-- Do not move in CommonJs File -->
<!-- For Disable button and add loader in button -->
<script>
    $().ready(function() {
    $("#trackSubmit").validate({
    submitHandler: function(form) {
    $(".submit-loader").attr("disabled", "disabled");
    $(".btn-loader-icon").css("display", "block");
    $(".submit-loader").css("padding-left", "25px");
    return true;
    },
            messages: {
            'donation_title': {
            required: " Donation Title field is required",
            },
                    'track':{
                    required: " Year field is required",
                    }
            },
            errorPlacement: function ($error, $element) {
            console.log($error);
            var name = $element.attr("name");
            $("#errors-" + name).append($error);
            }
    });
    });</script>
<main class="full-width">
    <header>
        <div class="container">
            <div class="adjust-margin toc-block">
                <h1 class="toc-title">Track Donation</h1>
                <p class="toc-description">Allows you to view and download full statistics of the amount donated by the Customers against a particular year. Store admin can get the details of the donation for the selected organization and year.</p><!--On the top it shows how much total donation collected this month and overall donation collections received by your store.-->
            </div>
        </div>	
    </header>
    <section>
        <div class="full-width"> 
            <article style="display:none;"> 
                <div class="column six card" <?php /* data-step="4" data-intro="Total donation this month" */ ?>>
                    <h4>Total donation this month<h4>
                            <h2><?php
                                if (isset($currency)) {
                                    echo $currency;
                                } else {
                                    echo '';
                                }
                                ?> <?php
                                if (isset($current_month_donation)) {
                                    echo $current_month_donation;
                                } else {
                                    echo '0';
                                }
                                ?></h2>
                            </div>
                            <div class="column six card" <?php /* data-step="5" data-intro="Total donations collection" */ ?>>
                                <h4>Total donations collection</h4>                                
                                <h2><?php
                                    if (isset($currency)) {
                                        echo $currency;
                                    } else {
                                        echo '';
                                    }
                                    ?> <?php
                                    if (isset($total_collection_donation)) {
                                        echo $total_collection_donation;
                                    } else {
                                        echo '0';
                                    }
                                    ?></h2>
                            </div>
                            </article>
                            <form action="" id="trackSubmit" method="get" id="regForm" class="submitForm">
                                <article>
                                    <div class="column twelve card">
                                        <h2>Track Donations</h2>
                                        <hr />
                                        <div class="mt-20" style="overflow: hidden;">
                                            <div class="columns five" data-step="1" data-intro="This section will allow to select particular donation name from the drop-down whose information you would like to see.">
                                                <input type="hidden" name="shop" value="{{ $shop }}"/>
                                                <label>Select Donation Name</label>       
                                                <?php $selected_value = ''; ?>
                                                <select name="donation_title" id="donation_title" required="">
                                                    <option value="">Select Donation</option>
                                                    @foreach($donation_data as $key => $value)                                                   
                                                    <option value="{{ $key }}" <?php
                                                    if (isset($product_id)) {
                                                        if ($product_id == $key) {
                                                            echo "selected";
                                                            $selected_value = $value;
                                                        }
                                                    }
                                                    ?>>{{ $value }}</option>
                                                    @endforeach
                                                </select> 
                                                <div id="errors-donation_title"></div>
                                            </div>
                                            <div class="columns four" data-step="2" data-intro="This section will allow to select the particular year against selected donation organization to see the information of the donations collected.">
                                                <label>Select Year</label>
                                                <select name="track" id="track" required="">
                                                    <!--<option value="">Year</option>-->
                                                    <?php $current_year = date("Y"); ?>
                                                    @for($i = $current_year; $i >= 2018; $i--)                                                    
                                                    <option value="{{ $i }}" <?php
                                                    if (isset($trackNumber)) {
                                                        if ($trackNumber == $i) {
                                                            echo "selected";
                                                        }
                                                    }
                                                    ?>>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                                <div id="errors-track"></div>
                                            </div>
                                            <div class="columns two" data-step="3" data-intro="Clicking on 'Track' button will show the information of the donation in the graphical form.">
                                                <br/>
                                                <button class="btn btn-primary submit-loader" type="submit" name="save"><i class="fa fa-circle-o-notch fa-spin btn-loader-icon" style="display:none;"></i>Track</button>
                                            </div>
                                        </div>
                                        <div class="mt-3">
                                            <div id="donation-chart" style="display:<?php
                                                 if (isset($trackNumber)) {
                                                     if ($trackNumber == "") {
                                                         echo 'none';
                                                     } else {
                                                         echo 'block';
                                                     }
                                                 } else {
                                                     echo 'none';
                                                 }
                                                 ?>"></div>
                                        </div>                                            
                                    </div>
                                </article>
                            </form>
                            </div>
                            </section>
                            <footer></footer>
                            </main>
                            <?php
                            $i = 0;
                            $newOrderCollection = array();
                            $monthArray = array("Jan" => 0, "Feb" => 0, "Mar" => 0, "Apr" => 0, "May" => 0, "Jun" => 0, "Jul" => 0, "Aug" => 0, "Sep" => 0, "Oct" => 0, "Nov" => 0, "Dec" => 0);
                            $matchMonth = (array_keys($monthArray));
                            $monthValue = '';
                            $amount = 0;
                            if (isset($orderCollections) && $orderCollections != NULL) {
                                foreach ($orderCollections->orders as $orderData) {
                                    foreach ($orderData->line_items as $productData) {
                                        $donate_id = $productData->product_id;
                                        $date = $orderData->created_at;
                                        $explDate = explode('T', $date, -1);
                                        $implDate = implode(" ", $explDate);
                                        $dateValue = strtotime($implDate);
                                        $year = date("Y", $dateValue);
                                        $month = date("M", $dateValue);
                                        if ($donate_id == $product_id && $year == $trackNumber) {
                                            $newOrderCollection[$i]['order_id'] = $orderData->id;
                                            $newOrderCollection[$i]['date'] = $implDate;
                                            $newOrderCollection[$i]['product_id'] = $donate_id;
                                            $newOrderCollection[$i]['amount'] = $productData->price * $productData->quantity;
                                            for ($j = 0; $j < 12; $j++) {
                                                if ($matchMonth[$j] == $month) {
                                                    if ($productData->vendor == 'zestard-easy-donation') {
                                                        $monthArray[$month] = $monthArray[$month] + $newOrderCollection[$i]['amount'];
                                                    }
                                                }
                                            }
                                            $i++;
                                        }
                                    }
                                }
                                $monthValue = implode(",", $monthArray);
                            }
                            ?>
                            <script type="text/javascript">
                                 <?php                                
                                if($selected_value == ''){
                                    $selected_value = 'Track Donation';
                                }
                                ?>
                                jQuery(document).ready(function() {
                                Highcharts.chart('donation-chart', {
                                chart: {
                                type: 'line'
                                },
                                        title: {
                                        text:  '<?php echo $selected_value; ?> ( Jan, <?php echo isset($trackNumber) ? $trackNumber : '' ?> - Dec, <?php echo isset($trackNumber) ? $trackNumber.' )' : '' ?>'
                                        },
                                        xAxis: {
                                        categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                                        },
                                        yAxis: {
                                        title: {
                                        text: 'Amount of Donation'
                                        }
                                        },
                                        plotOptions: {
                                        line: {
                                        dataLabels: {
                                        enabled: true
                                        },
                                                enableMouseTracking: false
                                        }
                                        },
                                        series: [{
                                        name: 'Total amount of Donation in every month of <?php echo isset($trackNumber) ? $trackNumber : '' ?>',
                                                data: [<?php echo isset($monthValue) ? $monthValue : '' ?>]
                                        }, ]
                                });
                                });
                            </script>
                            @endsection
