@extends('headerpro')
@section('content')
<?php
        if (!session('shop')) {
            $shop = session('shop');
        } else if(isset($_REQUEST['shop'])) {
            $shop = $_REQUEST['shop'];
        }else{
            $shop = "";
        }        
        ?>
<script type="text/javascript">
    ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
    title: 'Help',
            buttons: {
            secondary: [
            {
            label: 'Dashboard',
                    href : '{{route('dashboard')}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'Donations',
                    href : '{{route($add_donation_route,$product_id)}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'Track Donation',
                    href : '{{route('track_donationpro')}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'General Settings',
                    href : '{{route('email_configurationpro')}}?shop=<?php echo $shop; ?>',
                    loading: true
            }
            ]
            }
    });
    });</script>
<main class="full-width">
    <header>
        <div class="container">
            <div class="adjust-margin toc-block">
                <h1 class="toc-title">Help</h1>
                <p class="toc-description">Instructions provided below will help customers to understand the application features and how to configure the app.FAQ section will help to resolve customer's query.</p>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="adjust-margin toc-block">
            <h2 class="toc-title">Need Help?</h2>
            <p class="toc-description">To customize any thing within the app or for other work just contact us on below details.</p>
        </div>
    </div>
    <section>
        <article>
            <div class="card">
                <div class="row">
                    <ul class="support-info">
                        <li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
                        <li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
                        <li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
                    </ul>
                </div>
            </div>
        </article> 
    </section>
    <div class="container">
        <div class="adjust-margin toc-block">
            <h2 class="toc-title">How to Configure?</h2>
            <p class="toc-description">Please follow below mentioned instructions for making admin configuration settings for both Basic (Single donation) and Advance (Multiple Donation) versions of our application.</p>
        </div>
    </div>
    <section>
        <article>
            <div class="card has-sections accordion" id="accordion">
                
                <h3 class="accordion-head">Instruction Guide to Setup and Configure Application</h3>
                <div class="accordion-desc">
                    <ul>
                        <li>
                            <b>To read or download the User Manual of the application please click</b>  &nbsp; <a target="_blank" class="btn btn-primary button" type="submit" name="save" href="{{ url('/download/EasyDonationUserManual.pdf') }}">Download PDF</a>
                                                        
                        </li>                        
                    </ul>                    
                </div>
                
                <h3 class="accordion-head">Admin Setting for Multiple Donation</h3>
                <div class="accordion-desc">
                    <ol>
                        <li>
                            <b>Title for Donation Section</b>
                            <br>Title for main donation section, where all donation boxes will be shown.
                        </li>
                        <li>
                            <b>Donation Name</b>
                            <br>
                            Name of the organization/cause for which the customers will donate.
                        </li>
                        <li>
                            <b>Donation Description</b>
                            <br>
                            Description about the organization/cause for which the customers will be donating.
                        </li>
                        <li>
                            <b>Field Option</b>
                            <br>
                            The donation amount can be added using dropdown, price bar or a simple text box.
                        </li>
                        <li>
                            <b>Donation Image</b>
                            <br>
                            Image can also be uploaded for a donation box. When a donation box is added, default image will be set. If you want to change the default image, it can be changed using this option.
                        </li>
                    </ol>
                    <div class ="row easy_donation_screenshot_box">
                        <div>
                            <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/multiple-donation-admin.png') }}">
                                <img class="easy_donation_help_page_image_logo" src="{{ asset('image/multiple-donation-admin.png') }}">
                            </a>
                        </div>
                        <div>
                            <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/Multiple-donation-Front-new.jpg') }}">
                                <img class="easy_donation_help_page_image_logo" src="{{ asset('image/Multiple-donation-Front-new.jpg') }}">
                            </a>
                        </div>
                    </div>
                </div>
                <h3 class="accordion-head">Admin Setting for Single Donation</h3>
                <div class="accordion-desc">
                    <ol>
                        <li>
                            <b>Donation Name</b><br>Name of the organization/cause for which the customers will donate.
                        </li>
                        <li>
                            <b>Donation Description</b><br>Description about the organization/cause for which the customers will be donating.
                        </li>

                        <li>
                            <b>Donation Product Image</b><br>Image can also be uploaded/edited for a donation product.
                        </li>

                        <li>
                            <b>Field Option</b><br>The donation amount can be added using dropdown, price bar or a simple text box(minimum amount can be set for text box option).</li>
                    </ol>
                    <div class ="row easy_donation_screenshot_box">
                        <div> 
                            <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/single-donation-admin.png') }}">
                                <img class="easy_donation_help_page_image_logo" src="{{ asset('image/single-donation-admin.png') }}">
                            </a>
                        </div>
                        <div>
                            <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/Single-Donation-Frontend.jpg') }}">
                                <img class="easy_donation_help_page_image_logo" src="{{ asset('image/Single-Donation-Frontend.jpg') }}">
                            </a>
                        </div>
                    </div>
                </div>
                <h3 class="accordion-head">Manage ShortCode</h3>
                <div class="accordion-desc">
                    <div class="success-copied"></div>
                    <div class="view-shortcode">
                        <h2 class="sub-heading">Shortcode</h2>
                        <div class="cus-row">
                            <div class="c-7">                                
                                <textarea id="ticker-shortcode" rows="1" class="form-control short-code"  readonly="">{% include 'donation' %}</textarea>
                            </div>
                            <div class="c-5">
                                <button type="button" onclick="copyToClipboard('#ticker-shortcode')" class="btn tooltipped tooltipped-s copyMe copyclippro" accesskey="" style="display: block;"><i class="fa fa-check"></i>&nbsp;Copy to clipboard</button>
                            </div>
                        </div>
                    </div><br/>
                    <ul class="ul-help" type="none">    
                        <li>Copy the Shortcode from above and paste it in <a href="https://<?php echo $shop ?>/admin/themes/current?key=sections/cart-template.liquid" target="_blank"><b>cart-template.liquid</b></a>.
                        </li>
                        <a class="screenshot" href="javascript:void(0)" image-src="https://shopifydev.anujdalal.com/easy_donation_optimization/public/image/ED-cart-template-shortcode.png">
                            <img class="easy_donation_help_page_image_logo" src="https://shopifydev.anujdalal.com/easy_donation_optimization/public/image/ED-cart-template-shortcode.png">
                        </a>
                        <br>
                        <br>
                        <div class="row">
                            <li>If your theme does not have cart-template.liquid file then paste the short-code in <a href="https://<?php echo $shop ?>/admin/themes/current?key=templates/cart.liquid" target="_blank"><b>cart.liquid</b></a>.
                            </li>
                            <a class="screenshot" href="javascript:void(0)" image-src="https://shopifydev.anujdalal.com/easy_donation_optimization/public/image/ED-cart-shortcode.png">
                                <img class="easy_donation_help_page_image_logo" src="https://shopifydev.anujdalal.com/easy_donation_optimization/public/image/ED-cart-shortcode.png">
                            </a>
                        </div>
                        <div class="row">
                            <li>Copy the Shortcode from above and paste it in <a href="https://<?php echo $shop ?>/admin/themes/current?key=sections/product-template.liquid" target="_blank"><b>product-template.liquid</b></a>.
                            </li>

                            <a class="screenshot" href="javascript:void(0)" image-src="https://shopifydev.anujdalal.com/easy_donation_optimization/public/image/ED-product-template-shortcode.png">
                                <img class="easy_donation_help_page_image_logo" src="https://shopifydev.anujdalal.com/easy_donation_optimization/public/image/ED-product-template-shortcode.png">
                            </a>
                        </div>
                        <div class="row">
                            <li>If your theme is section theme then paste it in <a href="https://<?php echo $shop ?>/admin/themes/current?key=templates/product.liquid" target="_blank"><b>product.liquid</b></a>.
                            </li>
                            <a class="screenshot" href="javascript:void(0)" image-src="https://shopifydev.anujdalal.com/easy_donation_optimization/public/image/ED-product-shortcode.png">
                                <img class="easy_donation_help_page_image_logo" src="https://shopifydev.anujdalal.com/easy_donation_optimization/public/image/ED-product-shortcode.png">
                            </a>
                        </div>
                    </ul>
                </div>
            </div>
        </article> <hr />			
    </section>
    <div class="container">
        <div class="adjust-margin toc-block">
            <h2 class="toc-title">Frequently Asked Questions - FAQ</h2>
            <p class="toc-description"></p>
        </div>
    </div>
    <section>
        <article>
            <div class="card has-sections accordion" id="accordion">
                <h3 class="accordion-head">How many donations a merchant can create?</h3>
                <div class="accordion-desc">
                    <p>In total our application have two below mentioned versions : </p>
                    <ol class="ul-help">
                      <li><b>Basic Version: </b>Merchant will be allowed to make Single Donation and similarly can associate with single organization. </li>
                      <li><b>Advance Version: </b>Merchant will be allowed to make Multiple Donations(Recommended not to exceed more then 8 for better performance) and with multiple organizations.</li>
                    </ol>
                    <div class="alert notice">
                        <dl>
                            <dt>Note : </dt>
                            <dd>Please make sure not to delete the donation products which gets generated on the installation of the application otherwise it will result in showing the errors. Donation products are always available for the add/update facility which you can do from the app's admin section and also from the "Products" section of the shopify.</dd>
                        </dl>
                    </div>
                </div>

                <h3 class="accordion-head">Is it required to register the organization at your end? How many Organization does it support?</h3>
                <div class="accordion-desc">
                    <p>It is not essential to register the organizations at our end. Our application provides flexibility of using any organizations irrespective of specific listing or countries. It can support to Multiple Organizations(Using Advance version of app) as per your requirement. It is suggested not to exceed count more then 8 just for better performance. </p>
                </div>


                <h3 class="accordion-head">Is it required to have organizations/firms/charity institutions from specific countries?</h3>
                <div class="accordion-desc">
                    <p>Our application do not have any such criteria. Organizations/firms/charity institutions can be selected irrespective of any countries.</p>
                </div>


                <h3 class="accordion-head">Can we update the labels and descriptions of the donation option?</h3>
                <div class="accordion-desc">
                    <p>Yes, you have the ability to change the donation section details from "General Settings".</p>
                </div>


                <h3 class="accordion-head">Can we have the Pre-determined donation amount as options given to the customer? </h3>
                <div class="accordion-desc">
                    <p>Yes, you can add those by selecting drop-down option field while creating/editing donation from the backend.</p>
                    <p>Easy Donation App provides you the ability to set the "Donation Amount" as a dropdown or textbox or price range.</p>

                    <ol class="ul-help">
                        <li><b>Dropdown: </b>Merchant can select a drop-down option and can add the values they want their customers to choose from.</li>
                        <li><b>Price Bar: </b>In this merchant have to define the range of donation amount like "Rs. 1 to Rs. 1000". So that the customer can select the amount in between that range. The set range will be defined as per your store currency.</li>
                        <li><b>Text Box: </b>If the merchant selects this option then the customer can write the donation amount of their own choice.</li>
                    </ol>                    
                </div>


                <h3 class="accordion-head">Do we have the ability to set a minimum donation amount?</h3>
                <div class="accordion-desc">
                    <p>Yes, Merchant has the ability to add a minimum amount while creating the donation from the backend.  While creating a donation, from the Field option select "textbox" and you can see below  "Add Minimum Amount" textbox add the minimum value here.</p>
                    <p>Easy donation app will then restrict the amount to go below the amount set at "Add Minimum Amount" and customers will see the message to enter the amount likewise.</p>
                </div>


                <h3 class="accordion-head">Do we have the ability to add/modify the design of the Donation Option? </h3>
                <div class="accordion-desc">
                    <p>Merchants have the ability to add additional CSS section from General Settings -> "Additional CSS" and input custom CSS here depending on your theme.</p>
                </div>


                <h3 class="accordion-head">Can we set up the donation option for the normal products of the store?</h3>
                <div class="accordion-desc">
                    <p>Yes, It is possible to set up for the other normal products. You can drop us an email at <a href="mailto:support@zestard.com">support@zestard.com</a> and will get back to you to set up after checking store theme and it's feasibility.</p>
                </div>


                <h3 class="accordion-head">How customers will be notified for the donations they are making in store?</h3>
                <div class="accordion-desc">
                    <p>Customers will receive a donation receipt in the form of email on behalf of the donations they make. Also,  Merchant can add any secondary email address in CC from General Settings ->  "Email Configuration" to receive a copy of the   donation receipt.</p>
                </div>

                <h3 class="accordion-head">Can we change the email subject of Donation receipt received by our Customers? </h3>
                <div class="accordion-desc">
                    <p>Yes, you can now edit the subject in our new version of the app from General Settings -> Email configuration.</p>
                </div>

                <h3 class="accordion-head">Can Merchant change the format of donation Receipt sent to our Customers?</h3>
                <div class="accordion-desc">
                    <p>Yes, Merchant can change the format of the receipt from General Settings -> "Email Template" option.</p>
                </div>

                <h3 class="accordion-head">How merchants can track the donations made by the customers?</h3>
                <div class="accordion-desc">
                    <p>Click on "Track Donation" on top of the app at the back end. Select a particular donation and year in-case of multiple donation version and press submit to see a chart below which indicates month wise donations against that particular year.</p>
                </div>


                <h3 class="accordion-head">How the donation amount is carried to the organization?</h3>
                <div class="accordion-desc">
                    <p>It is the responsibility of the store owner to carry the donation amount manually towards the organization. Our app will just create an option to take the donation. Merchant have to check the orders & then transfer the amount to the organization by themselves.</p>
                </div>


                <h3 class="accordion-head">Can we have the "Donation" option added in the home page? </h3>
                <div class="accordion-desc">
                    <p>Yes, It is possible and will be considered as customization. Drop us an email at <a href="mailto:support@zestard.com">support@zestard.com</a> and we can help you set the Donation option at home page in your store.</p>
                </div>



            </div>
        </article>		
    </section>
    <footer></footer>
    <script type="text/javascript">
        $(function() {
        $(".accordion").accordion({
        heightStyle: "content",
                collapsible: true,
                active: false,
        });
        });
    </script>
    @endsection
