@extends('headerpro')
@section('content')

<?php
        if (!session('shop')) {
            $shop = session('shop');
        } else if(isset($_REQUEST['shop'])) {
            $shop = $_REQUEST['shop'];
        }else{
            $shop = "";
        }        
        ?>
<script type="text/javascript">
    ShopifyApp.ready(function (e) {
    ShopifyApp.Bar.initialize({
    title: 'List Donations',
            buttons: {
            secondary: [
            {
            label: 'Dashboard',
                    href: '{{route("dashboard")}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'Track Donation',
                    href: '{{route("track_donationpro")}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'General Settings',
                    href: '{{route("email_configurationpro")}}?shop=<?php echo $shop; ?>',
                    loading: true
            },
            {
            label: 'Help',
                    href: '{{route("help_pro")}}?shop=<?php echo $shop; ?>',
                    loading: true
            }
            ]
            }
    });
    });</script>
<main class="full-width">
    <header>
        <div class="container">
            <div class="adjust-margin">
                @if(session()->has('success'))
                <div class="alert success alertHide">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Success</dt>
                        <dd>{{ Session('success') }}</dd>
                    </dl>
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert error alertHide" style="display: block;">
                    <a class="close btnClose"></a>
                    <dl>
                        <dt>Error</dt>
                        <dd>{{ Session('error') }}</dd>
                    </dl>
                </div>
                @endif
            </div>
        </div>
        <div class="container">
            <div class="adjust-margin toc-block">
                <h1 class="toc-title">List Donations</h1>
                <p class="toc-description">Different listing of the donations are available along with its full information.</p>
                <?php
                if ($shop_find->app_version == 1 || $shop_find->type_of_app == 1) {
                    $add_donation_button = 'basic_donation_create_edit';
                } else if ($shop_find->app_version == 2 || $shop_find->type_of_app == 2) {
                    $add_donation_button = 'advance_donation_create_edit';
                }
                ?>
                <div class="toc-block">
                    <a href="{{route($add_donation_button)}}">
                        <button class="btn btn-primary" type="submit" name="add_donation">Add Donation</button>
                    </a>
                </div>                
            </div>
        </div>
    </header>
    <section>
        <article>
            <div class="card">
                <div class="donation_div">
                    <table class="donation_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Donation Name</th>
                                <th>Donation Description</th>                                
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbody donationcls">  
                            <?php //echo count($donation_datas); die; ?>
                            @if(count($donation_datas) > 0 && $donation_datas != '')                            
                            <?php $i = 1; ?>
                            @foreach ($donation_datas as $donation_product)
                            <?php
                            if (is_object($donation_product)) {
                                $donation_product = get_object_vars($donation_product);
                            }
                            ?>
                            <tr>
                                <td class="increment">{{ $i }}</td>
                                <td class="dname">{{ $donation_product['donation_name'] }}</td>
                                <td class="ddesc">
                                    <?php
                                    if (isset($donation_product['donation_description']) && $donation_product['donation_description'] != '') {
                                        $desc = str_replace(array("\n", "\r"), '', $donation_product['donation_description']);
                                        $desc = preg_replace("/<img[^>]+\>/i", " ", $desc);
                                        ?>
                                        {!! strlen($desc) > 50 ? substr($desc,0,50)."..." : $desc; !!}
                                    <?php } ?>

                                </td>
                                <td>
                                    <?php if ($user_settings->type_of_app == 1 && $user_settings->app_version == 1) { ?>

                                        <a href="{{ route('basic_donation_create_edit',$donation_product['product_id']) }}"
                                           class="secondary icon-edit editlink"></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php } else if ($user_settings->type_of_app == 2 && $user_settings->app_version == 2) { ?>
                                        <a href="{{ route('advance_donation_create_edit',$donation_product['product_id']) }}"
                                           class="secondary icon-edit editlink"></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php } ?>
                                    <?php if ($user_settings->type_of_app == 1 && $user_settings->app_version == 1) { ?>
                                        <a href="{{ url('/donation-basic-delete/'.$donation_product['product_id'].'/'.$donation_config->id) }}"
                                           class="secondary icon-trash deletelink"
                                           onclick="return confirm('Are you sure you want to delete this item?');"></a>
                                       <?php } else if ($user_settings->type_of_app == 2 && $user_settings->app_version == 2) { ?>
                                        <a href="{{ url('/donation-pro-delete/'.$donation_product['product_id'].'/'.$donation_config->id) }}"
                                           class="secondary icon-trash deletelink"
                                           onclick="return confirm('Are you sure you want to delete this item?');"></a>
                                       <?php } ?>
                                </td>
                            </tr>
                            <?php $i = $i + 1; ?>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="5" style="text-align: center;">No record found.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </article>
    </section>
</main>
<script src="{{ asset('js/commonJS.js') }}"></script>
<script>
                                           $(".submitForm").click(function () {
                                           $(".overlay").show();
                                           });
</script>
@endsection