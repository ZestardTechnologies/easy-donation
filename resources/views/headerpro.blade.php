@yield('header')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <title>Easy Donation Zestard</title>
        <!-- Uptowncss -->
        <link rel="stylesheet" href="{{ asset('css/uptown/uptown_style.css') }}?t=<?php echo time(); ?>'">
        <link rel="stylesheet" href="{{ asset('css/uptown/uptown_custom.css') }}?t=<?php echo time(); ?>'">
        <link rel="stylesheet" href="{{ asset('css/validate/screen.css') }}">
        <link rel="stylesheet" href="{{ asset('css/uptown/custom_css.css') }}">
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">        
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>        
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <!-- shopify Script for fast load -->
        <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
        <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>    
        
        <link rel="stylesheet" href="{{ asset('css/introjs.css') }}">
        <script src="{{ asset('js/intro.js') }}"></script>
        
        <!-- HumDash -->
        <script type="text/javascript">
            var _ha = window._ha || [];
            / tracker methods like "setCustomDimension" should be called before "trackPageView" /
            _ha.push(['trackPageView']);
            _ha.push(['enableLinkTracking']);
            (function() {
                var u="https://app.humdash.com/";
                _ha.push(['setTrackerUrl', u+'humdash.php']);
                _ha.push(['setSiteId', '2049']);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'humdash.js'; s.parentNode.insertBefore(g,s);
            })();
            </script>
            <!-- End HumDash Code -->



        <script>
ShopifyApp.init({
    apiKey: '1fd26ba6d3629fb9b5771d89ad52d5ce',
    shopOrigin: '<?php echo "https://" . session('shop') ?>'
});
ShopifyApp.ready(function () {
    ShopifyApp.Bar.initialize({
        icon: "",
        title: '',
        buttons: {}
    });
});
        </script>
    </head>
    <body>
        @yield('navigation')
        <div class="overlay" style="display: none;">
            <img src="{{ asset('image/loadingthin.svg') }}" class="loadImg" id="loadImg" width="50px;" height="50px">
        </div>
        @yield('content')        
        <script src="{{ asset('js/jquery.copy-to-clipboard.js') }}"></script>
        <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ asset('js/simple-slider.js') }}"></script>
        <script src="{{ asset('js/javascript.js') }}"></script>
        <script type="text/javascript">
$(".clickDonation").click(function () {
    $(this).find('button').prop('disabled', true);
});
        </script>
        <script type="text/javascript">
            function copyToClipboard(element) {
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val($(element).text()).select();
                document.execCommand("copy");
                $temp.remove();
            }
        </script>
        <script>
            jQuery(document).ready(function () {
                jQuery(".copyMe").click(function () {
                    var count = jQuery('.show').length;
                    if (count == 0) {
                        jQuery(".show").show();
                        jQuery(".success-copied").after('<div class="alert success alert-success alert-dismissable show"><a class="close btnClose"></a><dl><dt>Success</dt><dd>Your shortcode has been copied.</dd><a href="#" class="close" data-dismiss="alert" aria-label="close"></a></dl></div>');
                    }
                });

                setTimeout(function () {
                    $('.alertHide').fadeOut('fast');
                }, 10000);

            });</script>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
            (function () {
                var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                s1.async = true;
                s1.src = 'https://embed.tawk.to/5a2e20e35d3202175d9b7782/default';
                s1.charset = 'UTF-8';
                s1.setAttribute('crossorigin', '*');
                s0.parentNode.insertBefore(s1, s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
    </body>
</html>