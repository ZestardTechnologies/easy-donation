<?php
$donation_settings_array = json_decode($donation_settings, true);
$donation_settings_object = ($donation_settings_array['donation_data']);
$count_donation  = 0;
//dd(images_json);
?>
<script type="text/javascript" src="{{ asset('js/latest_jquery.min.js') }}"></script>
<script src="{{ asset('js/latest_jquery-ui.js') }}" type="text/javascript"></script>
<link href="{{ asset('css/latest_jquery-ui.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/frontpreviewpro.css') }}" rel="stylesheet" type="text/css" />
<script src="https://zestardshop.com/shopifyapp/easy_donation/public/js/rangeslider.js"></script>
<script>
var shop_name = Shopify.shop;
if(shop_name == "n0m0r3sprdsh33t.myshopify.com"){
    $zestard_easy = window.jQuery;
}
function accordion() {
    var acc = document.getElementsByClassName("accordion");
    var i;
	//if(shop_name != "hta-atlanta.myshopify.com")
	{
		for (i = 0; i < acc.length; i++) {
			acc[i].addEventListener("click", function () {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.display === "block") {
					panel.style.display = "none";
				} else {
					panel.style.display = "block";
				}
			});
		}
	}
}
</script>
<script>
    var base_path_easydonation = "https://zestardshop.com/shopifyapp/easy_donation/public/";
    var product_variant_ids;    
    var shop_id = "{{ $id }}";	
	var temp_product_id = [];     
	var temp_product_id = [];   		
	var shop_id = '<?php echo $id; ?>';
	var page = '<?php echo $page; ?>';
	var productid = '<?php echo $productid; ?>';
    setdata = <?php echo $donation_settings ?>;
    popup_message = setdata.popup_message;
    
	if(setdata) 
	{                 
		var shop = setdata.shop_id;
		{
			product_image = <?php echo $images_json ?>;
		}                    
	}	
    
	$(".donation_loader").hide();	
    $zestard_easy(document).ready(function(){        
        //Commented by Girish for Increasing Speed
		/* $zestard_easy.ajax({
            type: "GET",
            url: base_path_easydonation + "front_previewpro",
            data: {
                'id': shop_id
            },
            success: function (data) {
                setdata = JSON.parse(data);
                if (setdata) {
                    var shop = setdata.shop_id;
                    //for getting the product image
                    $zestard_easy.ajax({
                        type: "GET",
                        url: "{{ url('productimagepro') }}",
                        async: false,
                        data: {
                            id: shop,
                            'shop_name': shop_name
                        },
                        success: function (product) {							
							
							if(shop_name == "hta-atlanta.myshopify.com")
							{
								$(".loading_message").hide();
							}
                            product_image = JSON.parse(product);
                        }
                    });
                } */				
				
                var countCard = setdata.donation_data.length;
				@if($page == "product" && in_array($productid, $temp_product_ids))
				{					
					product_index = {{ array_search($productid, $temp_product_ids) }};		
					var i = product_index;					
					if(shop_name != "hta-atlanta.myshopify.com")
					{
						/*  
							<h3 class="display_title_' + i + '"></h3>
							<p class="product-image product-image_' + i + '"></p>
                        */
                       //For rocks-tell-stories.myshopify.com Store
                       if(shop_name == "rocks-tell-stories.myshopify.com")
                       {
                        var front_card = '<div id="dropdown_preview_' + i + '"><form name="dropdownform" novalidate id="dropdownform_' + i + '">' +
						'<div class=""><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class=""></a><div class=""><div class="easy_donation_ztpl"><p class="display_description display_description_' + i + '"></p></div>' +
						'<div><select name="donation_amount" id="donation_amount_' + i + '" class="donationamount"></select><label class="dollar_span" id="dollar_span_' + i + '" style="display:none">$</label><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/><span style="display:none;">Number of Books: <input type="number" required name="number_of_books" id="number_of_books_' + i + '" class="number_of_books" step="any" min="0" /></span>' +
						'<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
						'<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
						'<div id="textbox_preview_' + i + '"><form name="textboxform" id="textboxform' + i + '">' +
						'<div class=""><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class=""></a><div class=""><div class="easy_donation_ztpl"><p class="display_description display_description_' + i + '"></p></div>' +
						'<div><label class="ed_minimum_donation minimum_donation_' + i + '" for="minimum_donation" style="disply:none;"></label><input type="number" name="textbox_amount" id="textbox_amount_' + i + '" class="textbox_amount" step="any" min="1" required>' +
						'<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
						'<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
						'<div id="pricebar_preview_' + i + '"><form id="pricebarform_' + i + '" class="form-horizontal form-pricing" role="form">' +
						'<div class=""><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class=""></a><div class=""><div class="easy_donation_ztpl"><p class="display_description display_description_' + i + '"></p></div><input type="range" id="pricebar_slider_' + i + '" />' +
						'<div><div class="price-form"><label class="amountsize" for="amount">Amount: </label><input type="hidden" id="amount"><p class="price lead" id="amount-label_' + i + '"></p><p class="price">.00</p></div>' +
						'<input type="submit" class="btn button frontbutton" value="Donate"><div class="loaderDiv_' + i + '" style="display:none;"><img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>';
                       }   
                       else
                       {                     
						var front_card = '<div id="dropdown_preview_' + i + '"><form name="dropdownform" id="dropdownform_' + i + '">' +
						'<div class=""><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class=""></a><div class=""><div class="easy_donation_ztpl"><p class="display_description display_description_' + i + '"></p></div>' +
						'<div><select name="donation_amount" id="donation_amount_' + i + '" class="donationamount"></select><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/>' +
						'<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
						'<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
						'<div id="textbox_preview_' + i + '"><form name="textboxform" id="textboxform' + i + '">' +
						'<div class=""><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class=""></a><div class=""><div class="easy_donation_ztpl"><p class="display_description display_description_' + i + '"></p></div>' +
						'<div><label class="ed_minimum_donation minimum_donation_' + i + '" for="minimum_donation" style="disply:none;"></label><input type="number" name="textbox_amount" id="textbox_amount_' + i + '" class="textbox_amount" step="any" min="1" required>' +
						'<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
						'<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
						'<div id="pricebar_preview_' + i + '"><form id="pricebarform_' + i + '" class="form-horizontal form-pricing" role="form">' +
						'<div class=""><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class=""></a><div class=""><div class="easy_donation_ztpl"><p class="display_description display_description_' + i + '"></p></div><input type="range" id="pricebar_slider_' + i + '" />' +
						'<div><div class="price-form"><label class="amountsize" for="amount">Amount: </label><input type="hidden" id="amount"><p class="price lead" id="amount-label_' + i + '"></p><p class="price">.00</p></div>' +
						'<input type="submit" class="btn button frontbutton" value="Donate"><div class="loaderDiv_' + i + '" style="display:none;"><img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>';
                       }                                            
						if ($("[itemprop=offers]")) 
						{
							$("[itemprop=offers]").css("display", "none");
						} 
						else 
						{
							$("[itemprop=price]").css("display", "none");

							var button = jQuery('button[type=submit][name=add]');
							if (button.length == 0)
							{
								button = jQuery('input[type=submit][name=add]');
							}
							if (button.length == 0)
							{
								var f = jQuery('form[action="/cart/add"]');
								button = jQuery(f).find(':submit');
							}
							$(button).css("display", "none");
						}
					}
					else
					{
                        //For hta-atlanta.myshopify.com Store
						var front_card = '<div id="dropdown_preview_' + i + '"><form name="dropdownform" id="dropdownform_' + i + '">' +
							'<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);"><h3 class="display_title_' + i + '"></h3></a><div class="new_panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
							'<div><select name="donation_amount" id="donation_amount_' + i + '" class="donationamount"></select><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/>' +
							'<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
							'<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
							'<div id="textbox_preview_' + i + '"><form name="textboxform" id="textboxform' + i + '">' +
							'<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);"><h3 class="display_title_' + i + '"></h3></a><div class="new_panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
							'<div><label class="ed_minimum_donation minimum_donation_' + i + '" for="minimum_donation" style="disply:none;"></label><input type="number" name="textbox_amount" id="textbox_amount_' + i + '" class="textbox_amount" step="any" min="1" required>' +
							'<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
							'<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
							'<div id="pricebar_preview_' + i + '"><form id="pricebarform_' + i + '" class="form-horizontal form-pricing" role="form">' +
							'<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);"><h3 class="display_title_' + i + '"></h3></a><div class="new_panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div><input type="range" id="pricebar_slider_' + i + '" />' +
							'<div><div class="price-form"><label class="amountsize" for="amount">Amount: </label><input type="hidden" id="amount"><p class="price lead" id="amount-label_' + i + '"></p><p class="price">.00</p></div>' +
							'<input type="submit" class="btn button frontbutton" value="Donate"><div class="loaderDiv_' + i + '" style="display:none;"><img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>';							
                            
							if ($("[itemprop=offers]")) 
							{
								$("[itemprop=offers]").css("display", "none");
							} 
							else 
							{
								$("[itemprop=price]").css("display", "none");

								var button = jQuery('button[type=submit][name=add]');
								if (button.length == 0)
								{
									button = jQuery('input[type=submit][name=add]');
								}
								if (button.length == 0)
								{
									var f = jQuery('form[action="/cart/add"]');
									button = jQuery(f).find(':submit');
								}
								$(button).css("display", "none");
							} 		
					}
					$zestard_easy('#preview_product_container').append(front_card);								
					$zestard_easy('.donationamount').on('change', function(){  
						var dropdown_value = $zestard_easy(this).val();
						if(dropdown_value == "other")
						{                                
                            $zestard_easy(this).next().css('display', 'inline-block');
                            if(shop_name == "rocks-tell-stories.myshopify.com")
                            {
                                $zestard_easy(this).next().next().css('display', 'inline-block');
                                $zestard_easy(this).next().next().next().css('display', 'inline-block');
                            }
							if(shop_name == "hta-atlanta.myshopify.com")
							{
								$zestard_easy(this).next().val(1);	
							}
						}
						else{
                            $zestard_easy(this).next().css('display', 'none');
                            if(shop_name == "rocks-tell-stories.myshopify.com")
                            {
                                $zestard_easy(this).next().next().css('display', 'none');
                                $zestard_easy(this).next().next().next().css('display', 'none');
                            }
						}
					});
					@if(count($donation_settings_array) > 0) 
					{
						var donation_title = setdata.title;
						var donation_button_text = setdata.donation_button_text;
						var additional_css = setdata.additional_css;
						var donation_name = setdata.donation_data[product_index].donation_name;
						var donation_description = setdata.donation_data[product_index].donation_description;
						var field_option = setdata.donation_data[product_index].field_option;
						var drop_down = setdata.donation_data[product_index].drop_down;
						var dropdown_other = setdata.donation_data[product_index].dropdown_other;
						var text_dropdown_other = setdata.donation_data[product_index].text_dropdown_other;
						var text_amount = setdata.donation_data[product_index].text_amount;
						var add_min_amount = setdata.donation_data[product_index].add_min_amount;
						var donation_min = setdata.donation_data[product_index].donation_min;
						var donation_max = setdata.donation_data[product_index].donation_max;
						var product_id = setdata.donation_data[product_index].product_id;
						var select_page = setdata.donation_data[product_index].select_page;
						var shop_currency = setdata.shop_currency;
						var product_verify_id = ""; 						
								
						$zestard_easy('#preview_product_container').css('display', 'block');				
						if (donation_title) {
							$zestard_easy('.easy_donation_title').html("<b>" + donation_title + "</b>");
						} else {
                                                        $zestard_easy('.easy_donation_title').html("<b>Donation</b>");
                                                }
                                                
						if (donation_button_text) {
							$zestard_easy(".frontbutton").attr("value", setdata.donation_button_text);
						} else {
							$zestard_easy(".frontbutton").attr("value", "Donate");
						}				
						if (field_option == "P") 
						{
							$zestard_easy('#dropdown_preview_' + i).remove();
							$zestard_easy('#textbox_preview_' + i).remove();
							$zestard_easy('#pricebar_preview_' + i).css('display', 'block');

							var min_val = parseInt(donation_min);
							var max_val = parseInt(donation_max);
							if (product_id) {
								$zestard_easy('.product_id_' + i).val(product_id);
								product_verify_id = $zestard_easy('.product_id_' + i).val();
							}
							if (donation_name) {
								//$zestard_easy('.display_title_' + i).text(donation_name);
							} else {
								$zestard_easy('.display_title_' + i).css('display', 'none');
							}
							if (donation_description) {
								$zestard_easy('.display_description_' + i).html(donation_description);
							} else {
								$zestard_easy('.display_description_' + i).css('display', 'none');
							}							
							if(product_image[product_verify_id] != "")
							{
								//$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
							}
							else
							{
								$zestard_easy('.product-image_' + i).remove();
							}
							$zestard_easy('#pricebar_slider_' + i).val(min_val);
							$zestard_easy('#pricebar_slider_' + i).attr("min", min_val);
							$zestard_easy('#pricebar_slider_' + i).attr("max", max_val);
							$zestard_easy('#amount-label_' + i).html(shop_currency + min_val);
							$zestard_easy('#pricebar_preview_' + i).css('display', 'block');
							pricebarAmount(i);
							submitPricesbar(i);
						} 
						else if (field_option == "D") 
						{
							$zestard_easy('#pricebar_preview_' + i).remove();
							$zestard_easy('#textbox_preview_' + i).remove();
							$zestard_easy('#dropdown_preview_' + i).css('display', 'block');
							$zestard_easy('#dropdown_range_' + i).css('display', 'block');
							var option_array = JSON.parse(drop_down);
							if (product_id) {
								$zestard_easy('.product_id_' + i).val(product_id);
								product_verify_id = $zestard_easy('.product_id_' + i).val();
							}
							if (donation_name) {
								$zestard_easy('.display_title_' + i).text(donation_name);
							} else {
								$zestard_easy('.display_title_' + i).css('display', 'none');
							}
							if (donation_description) {
								$zestard_easy('.display_description_' + i).html(donation_description);
							} else {
								$zestard_easy('.display_description_' + i).css('display', 'none');
							}
							if(product_image[product_verify_id] != "")
							{
								$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
							}
							else
							{
								$zestard_easy('.product-image_' + i).remove();
							}
							var j = 0;
							if (setdata.variant_ids)
							{
								product_variant_ids = JSON.parse(setdata.variant_ids);
								variant_ids = product_variant_ids[product_id];
								$zestard_easy.map(variant_ids, function (value, index) {
									$zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
										value: value,
										text: option_array[index]
									}));
									j++;
								});
								if(dropdown_other == 1){
									$zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
									   id: "click_drop_down",
									   value: "other",
									   text: text_dropdown_other
								   }));
								}
							} else
							{
								$zestard_easy.each(option_array, function (j) {
									$zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
										value: option_array[j],
										text: option_array[j]
									}));
									j++;
								});
								if(dropdown_other == 1){
									$zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
									   id: "click_drop_down",
									   value: "other",
									   text: text_dropdown_other
								   }));
								}
							}							
							submitDropdown(i);
						} 
						else if (field_option == "T") 
						{
							$zestard_easy('#dropdown_preview_' + i).remove();
							$zestard_easy('#pricebar_preview_' + i).remove();
							$zestard_easy('#textbox_preview_' + i).css('display', 'block');

							var text_value = parseInt(text_amount);
							if (product_id) {
								$zestard_easy('.product_id_' + i).val(product_id);
								product_verify_id = $zestard_easy('.product_id_' + i).val();
							}
							if (donation_name) {
								$zestard_easy('.display_title_' + i).text(donation_name);
							} else {
								$zestard_easy('.display_title_' + i).css('display', 'none');
							}
							if (donation_description) {
								$zestard_easy('.display_description_' + i).html(donation_description);
							} else {
								$zestard_easy('.display_description_' + i).css('display', 'none');
							}
							if (add_min_amount >= 1) {
								$zestard_easy(".minimum_donation_" + i).css('display', 'block');
								$zestard_easy(".minimum_donation_" + i).html("Minimum Donation: " + shop_currency + add_min_amount);
								$zestard_easy("#textbox_amount_" + i).attr("min", add_min_amount);
							} else {
								$zestard_easy(".minimum_donation_" + i).css('display', 'none');
							}
							if(product_image[product_verify_id] != "")
							{
								$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
							}
							else
							{
								$zestard_easy('.product-image_' + i).remove();
							}
							$zestard_easy('#textbox_amount_' + i).val(text_value);
							submitTextbox(i);
						} else {
							$zestard_easy('#bar_range_' + i).css('display', 'none');
							$zestard_easy('#text_range_' + i).css('display', 'none');
							$zestard_easy('#dropdown_range_' + i).css('display', 'none');
						}
						$zestard_easy('#default_preview_' + i).css('display', 'none');
						$zestard_easy('#data_preview_' + i).css('display', 'block');
						$zestard_easy('.easy_donation_additional_css').html("<style>" + additional_css + "</style>");	
					}			
					@else 
					{
						$zestard_easy('#bar_range_' + product_index).css('display', 'none');
						$zestard_easy('#text_range_' + product_index).css('display', 'none');
						$zestard_easy('#dropdown_range_' + product_index).css('display', 'none');
						$zestard_easy('#default_preview_' + product_index).css('display', 'block');
						$zestard_easy('#data_preview_' + product_index).css('display', 'none');
					}
					@endif
				} 					
				@else
				{	
					
                for(var i = 0; i < countCard; i++) {										
                    var donation_title = setdata.title;
                    var donation_button_text = setdata.donation_button_text;
                    var additional_css = setdata.additional_css;
                    var donation_name = setdata.donation_data[i].donation_name;
                    var donation_description = setdata.donation_data[i].donation_description;
                    var field_option = setdata.donation_data[i].field_option;
                    var drop_down = setdata.donation_data[i].drop_down;
                    var dropdown_other = setdata.donation_data[i].dropdown_other;
                    var text_dropdown_other = setdata.donation_data[i].text_dropdown_other;
                    var text_amount = setdata.donation_data[i].text_amount;
                    var add_min_amount = setdata.donation_data[i].add_min_amount;
                    var donation_min = setdata.donation_data[i].donation_min;
                    var donation_max = setdata.donation_data[i].donation_max;
                    var product_id = setdata.donation_data[i].product_id;
                    var select_page = setdata.donation_data[i].select_page;
                    var shop_currency = setdata.shop_currency;
                    var product_verify_id = "";                    
                    if(select_page == undefined){
                        select_page = 1;
                    }       					
                    $zestard_easy('.easy_donation_additional_css').html("<style>" + additional_css + "</style>");
                    
                    if (select_page == 1 && page == "cart" || select_page == 3 && page == "cart")                     
                    {
						if(shop_name != "hta-atlanta.myshopify.com")
						{
							var front_card = '<div id="dropdown_preview_' + i + '"><form name="dropdownform" id="dropdownform_' + i + '">' +
                                '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
                                '<div><select name="donation_amount" id="donation_amount_' + i + '" class="donationamount"></select><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/>' +
                                '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
                                '<div id="textbox_preview_' + i + '"><form name="textboxform" id="textboxform' + i + '">' +
                                '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
                                '<div><label class="ed_minimum_donation minimum_donation_' + i + '" for="minimum_donation" style="disply:none;"></label><input type="number" name="textbox_amount" id="textbox_amount_' + i + '" class="textbox_amount" step="any" min="1" required>' +
                                '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
                                '<div id="pricebar_preview_' + i + '"><form id="pricebarform_' + i + '" class="form-horizontal form-pricing" role="form">' +
                                '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div><input type="range" id="pricebar_slider_' + i + '" />' +
                                '<div><div class="price-form"><label class="amountsize" for="amount">Amount: </label><input type="hidden" id="amount"><p class="price lead" id="amount-label_' + i + '"></p><p class="price">.00</p></div>' +
                                '<input type="submit" class="btn button frontbutton" value="Donate"><div class="loaderDiv_' + i + '" style="display:none;"><img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>';
                                if(shop_name == "amika-1.myshopify.com"){
                                    var front_card = '<div id="dropdown_preview_' + i + '"><form name="dropdownform" id="dropdownform_' + i + '">' +
                                        '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
                                        '<div><select name="donation_amount" id="donation_amount_' + i + '" class="donationamount"></select><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/>' +
                                        '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                        '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
                                        '<div id="textbox_preview_' + i + '"><form name="textboxform" id="textboxform' + i + '">' +
                                        '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
                                        '<div><label class="ed_minimum_donation minimum_donation_' + i + '" for="minimum_donation" style="disply:none;"></label><input type="number" name="textbox_amount" id="textbox_amount_' + i + '" class="textbox_amount" step="any" min="1" required>' +
                                        '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                        '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
                                        '<div id="pricebar_preview_' + i + '"><form id="pricebarform_' + i + '" class="form-horizontal form-pricing" role="form">' +
                                        '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div><div class="ztpl_easy_slider"><input type="range" id="pricebar_slider_' + i + '" data-rangeslider /></div>' +
                                        '<div><div class="price-form"><label class="amountsize" for="amount">Amount: </label><input type="hidden" id="amount"><p class="price lead" id="amount-label_' + i + '"></p><p class="price">.00</p></div>' +
                                        '<input type="submit" class="btn button frontbutton" value="Donate"><div class="loaderDiv_' + i + '" style="display:none;"><img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>';
                                }
                                
						}
						else
						{
							var front_card = '<div id="dropdown_preview_' + i + '"><form name="dropdownform" id="dropdownform_' + i + '">' +
                                '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);"><h3 class="display_title_' + i + '"></h3></a><div class="new_panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
                                '<div><select name="donation_amount" id="donation_amount_' + i + '" class="donationamount"></select><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/>' +
                                '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
                                '<div id="textbox_preview_' + i + '"><form name="textboxform" id="textboxform' + i + '">' +
                                '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);"><h3 class="display_title_' + i + '"></h3></a><div class="new_panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
                                '<div><label class="ed_minimum_donation minimum_donation_' + i + '" for="minimum_donation" style="disply:none;"></label><input type="number" name="textbox_amount" id="textbox_amount_' + i + '" class="textbox_amount" step="any" min="1" required>' +
                                '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
                                '<div id="pricebar_preview_' + i + '"><form id="pricebarform_' + i + '" class="form-horizontal form-pricing" role="form">' +
                                '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);"><h3 class="display_title_' + i + '"></h3></a><div class="new_panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div><input type="range" id="pricebar_slider_' + i + '" />' +
                                '<div><div class="price-form"><label class="amountsize" for="amount">Amount: </label><input type="hidden" id="amount"><p class="price lead" id="amount-label_' + i + '"></p><p class="price">.00</p></div>' +
                                '<input type="submit" class="btn button frontbutton" value="Donate"><div class="loaderDiv_' + i + '" style="display:none;"><img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>';																	
						}
                        $zestard_easy('#preview_container').append(front_card);
                        
                        $zestard_easy('.donationamount').on('change', function(){  
                            var dropdown_value = $zestard_easy(this).val();
                            if(dropdown_value == "other"){                                
                                $zestard_easy(this).next().css('display', 'inline-block');
                            }else{
                                $zestard_easy(this).next().css('display', 'none');
                            }
                        });
                        if (setdata) {
                            $zestard_easy('#preview_container').css('display', 'inline-block');
                            $zestard_easy('.donation_title').css('display', 'block');                                                        
                            if (donation_title) {
                                $zestard_easy('.easy_donation_title').html("<b>" + donation_title + "</b>");
                            } else {
                                $zestard_easy('.easy_donation_title').html("<b>Donation</b>");
                            }   
                            if (donation_button_text) {
                                $zestard_easy(".frontbutton").attr("value", setdata.donation_button_text);
                            } else {
                                $zestard_easy(".frontbutton").attr("value", "Donate");
                            }

                            
							if (field_option == "P") {

                                $zestard_easy('#dropdown_preview_' + i).remove();
                                $zestard_easy('#textbox_preview_' + i).remove();
                                $zestard_easy('#pricebar_preview_' + i).css('display', 'block');

                                var min_val = parseInt(donation_min);
                                var max_val = parseInt(donation_max);
                                if (product_id) {
                                    $zestard_easy('.product_id_' + i).val(product_id);
                                    product_verify_id = $zestard_easy('.product_id_' + i).val();
                                }
                                if (donation_name) {
                                    $zestard_easy('.display_title_' + i).text(donation_name);
                                } else {
                                    $zestard_easy('.display_title_' + i).css('display', 'none');
                                }
                                if (donation_description) {
                                    $zestard_easy('.display_description_' + i).html(donation_description);
                                } else {
                                    $zestard_easy('.display_description_' + i).css('display', 'none');
                                }
                                if(product_image[product_verify_id] != "")
								{
									$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
								}
								else
								{
									$zestard_easy('.product-image_' + i).remove();
								}
                                $zestard_easy('#pricebar_slider_' + i).val(min_val);
                                $zestard_easy('#pricebar_slider_' + i).attr("min", min_val);
                                $zestard_easy('#pricebar_slider_' + i).attr("max", max_val);
                                $zestard_easy('#amount-label_' + i).html(shop_currency + min_val);
                                $zestard_easy('#pricebar_preview_' + i).css('display', 'block');
                                pricebarAmount(i);
                                submitPricesbar(i);
								if(shop_name == "amika-1.myshopify.com")
								{									
									var price_tip = '<div id="price_map" style="display: block;width: 160px;margin: 10px 50px;position: relative;">'+
									'<span style="position: absolute;left: 0;">$' + min_val + '</span>'+
									'<span style="position: absolute;left: 40%;">$' + Math.floor((parseInt(min_val) + parseInt(max_val)) / 2) + '</span>'+
									'<span style="position: absolute;right: 0;">$' + max_val + '</span>'+
									'</div>';   
									$(".price-slider").find("input[type='range']").after(price_tip);
									$(".price-slider").find("input[type='range']").val(Math.floor((parseInt(min_val) + parseInt(max_val)) / 1));
                                
                                    //$(".lead").html("$"+Math.floor((parseInt(min_val) + parseInt(max_val)) / 2));
                                    $(".lead").html("$10");
								}
                            } else if (field_option == "D") {
                                $zestard_easy('#pricebar_preview_' + i).remove();
                                $zestard_easy('#textbox_preview_' + i).remove();
                                $zestard_easy('#dropdown_preview_' + i).css('display', 'block');
                                $zestard_easy('#dropdown_range_' + i).css('display', 'block');
                                var option_array = JSON.parse(drop_down);
                                if (product_id) {
                                    $zestard_easy('.product_id_' + i).val(product_id);
                                    product_verify_id = $zestard_easy('.product_id_' + i).val();
                                }
                                if (donation_name) {
                                    $zestard_easy('.display_title_' + i).text(donation_name);
                                } else {
                                    $zestard_easy('.display_title_' + i).css('display', 'none');
                                }
                                if (donation_description) {
                                    $zestard_easy('.display_description_' + i).html(donation_description);
                                } else {
                                    $zestard_easy('.display_description_' + i).css('display', 'none');
                                }
                                if(product_image[product_verify_id] != "")
								{
									$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
								}
								else
								{
									$zestard_easy('.product-image_' + i).remove();
								}
                                var j = 0;
                                if (setdata.variant_ids)
                                {
                                    product_variant_ids = JSON.parse(setdata.variant_ids);
                                    variant_ids = product_variant_ids[product_id];
                                    $zestard_easy.map(variant_ids, function (value, index) {
                                        $zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
                                            value: value,
                                            text: option_array[index]
                                        }));
                                        j++;
                                    });
                                    if(dropdown_other == 1){
                                        $zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
                                           id: "click_drop_down",
                                           value: "other",
                                           text: text_dropdown_other
                                       }));
                                    }
                                } else
                                {
                                    $zestard_easy.each(option_array, function (j) {
                                        $zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
                                            value: option_array[j],
                                            text: option_array[j]
                                        }));
                                        j++;
                                    });
                                    if(dropdown_other == 1){
                                        $zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
                                           id: "click_drop_down",
                                           value: "other",
                                           text: text_dropdown_other
                                       }));
                                    }
                                }
                                submitDropdown(i);
                            } else if (field_option == "T") {
                                $zestard_easy('#dropdown_preview_' + i).remove();
                                $zestard_easy('#pricebar_preview_' + i).remove();
                                $zestard_easy('#textbox_preview_' + i).css('display', 'block');

                                var text_value = parseInt(text_amount);
                                if (product_id) {
                                    $zestard_easy('.product_id_' + i).val(product_id);
                                    product_verify_id = $zestard_easy('.product_id_' + i).val();
                                }
                                if (donation_name) {
                                    $zestard_easy('.display_title_' + i).text(donation_name);
                                } else {
                                    $zestard_easy('.display_title_' + i).css('display', 'none');
                                }
                                if (donation_description) {
                                    $zestard_easy('.display_description_' + i).html(donation_description);
                                } else {
                                    $zestard_easy('.display_description_' + i).css('display', 'none');
                                }
                                if (add_min_amount >= 1) {
                                    $zestard_easy(".minimum_donation_" + i).css('display', 'block');
                                    $zestard_easy(".minimum_donation_" + i).html("Minimum Donation: " + shop_currency + add_min_amount);
                                    $zestard_easy("#textbox_amount_" + i).attr("min", add_min_amount);
                                } else {
                                    $zestard_easy(".minimum_donation_" + i).css('display', 'none');
                                }
								
                                if(product_image[product_verify_id] != "")
								{
									$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
								}
								else
								{
									$zestard_easy('.product-image_' + i).remove();
								}
                                $zestard_easy('#textbox_amount_' + i).val(text_value);
                                submitTextbox(i);
                            } else {
                                $zestard_easy('#bar_range_' + i).css('display', 'none');
                                $zestard_easy('#text_range_' + i).css('display', 'none');
                                $zestard_easy('#dropdown_range_' + i).css('display', 'none');
                            }
                            $zestard_easy('#default_preview_' + i).css('display', 'none');
                            $zestard_easy('#data_preview_' + i).css('display', 'block');
                        } else {
                            $zestard_easy('#bar_range_' + i).css('display', 'none');
                            $zestard_easy('#text_range_' + i).css('display', 'none');
                            $zestard_easy('#dropdown_range_' + i).css('display', 'none');
                            $zestard_easy('#default_preview_' + i).css('display', 'block');
                            $zestard_easy('#data_preview_' + i).css('display', 'none');
                        }
                    }                    
					//if (select_page == 2 && page == "product" && productid == product_id || select_page == 3 && page == "product" && productid == product_id) {                        
                    if(select_page == 2 && page == "product" || select_page == 3 && page == "product"){				
                        console.log(product_image);
                        if(shop_name == "the-younique-foundation.myshopify.com" || shop_name == "biofield-tuning-store.myshopify.com") 
						{
							
						}
						else
						{
							if ($("[itemprop=offers]")) {
								$("[itemprop=offers]").css("display", "none");
							} else {
								$("[itemprop=price]").css("display", "none");

								var button = jQuery('button[type=submit][name=add]');
								if (button.length == 0)
								{
									button = jQuery('input[type=submit][name=add]');
								}
								if (button.length == 0)
								{
									var f = jQuery('form[action="/cart/add"]');
									button = jQuery(f).find(':submit');
								}
								$(button).css("display", "none");
							}						
						}
						var front_card = '<div id="dropdown_preview_' + i + '"><form name="dropdownform" id="dropdownform_' + i + '">' +
                                '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
                                '<div><select name="donation_amount" id="donation_amount_' + i + '" class="donationamount"></select><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/>' +
                                '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
                                '<div id="textbox_preview_' + i + '"><form name="textboxform" id="textboxform' + i + '">' +
                                '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div>' +
                                '<div><label class="ed_minimum_donation minimum_donation_' + i + '" for="minimum_donation" style="disply:none;"></label><input type="number" name="textbox_amount" id="textbox_amount_' + i + '" class="textbox_amount" step="any" min="1" required>' +
                                '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>' +
                                '<div id="pricebar_preview_' + i + '"><form id="pricebarform_' + i + '" class="form-horizontal form-pricing" role="form">' +
                                '<div class="price-slider"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><a href="javascript:void(0);" class="accordion"><h3 class="display_title_' + i + '"></h3></a><div class="panel"><div class="easy_donation_ztpl"><p class="product-image product-image_' + i + '"></p><p class="display_description display_description_' + i + '"></p></div><input type="range" id="pricebar_slider_' + i + '" />' +
                                '<div><div class="price-form"><label class="amountsize" for="amount">Amount: </label><input type="hidden" id="amount"><p class="price lead" id="amount-label_' + i + '"></p><p class="price">.00</p></div>' +
                                '<input type="submit" class="btn button frontbutton" value="Donate"><div class="loaderDiv_' + i + '" style="display:none;"><img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></div></form></div>';	

								/*                         
								var front_card = '<div id="dropdown_preview_' + i + '"><a href="javascript:void(0);"><h3 class="display_title_' + i + '"></h3><form name="dropdownform" id="dropdownform_' + i + '">' +
                                '<div class="donation-section"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '">' +
                                '<div><select name="donation_amount" id="donation_amount_' + i + '" class="donationamount"></select><input type="number" name="dropdown_textbox" id="dropdown_textbox_' + i + '" class="dropdown_textbox" step="any" min="1" style="display:none;"/>' +
                                '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></form></div>' +
                                '<div id="textbox_preview_' + i + '"><a href="javascript:void(0);"><h3 class="display_title_' + i + '"></h3><form name="textboxform" id="textboxform' + i + '">' +
                                '<div class="donation-section"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '">' +
                                '<div><label class="ed_minimum_donation minimum_donation_' + i + '" for="minimum_donation" style="disply:none;"></label><input type="number" name="textbox_amount" id="textbox_amount_' + i + '" class="textbox_amount" step="any" min="1" required>' +
                                '<input type="submit" name="donate" value="Donate" class="btn button frontbutton"/><div class="loaderDiv_' + i + '" style="display:none;">' +
                                '<img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></form></div>' +
                                '<div id="pricebar_preview_' + i + '"><a href="javascript:void(0);"><h3 class="display_title_' + i + '"></h3><form id="pricebarform_' + i + '" class="form-horizontal form-pricing" role="form">' +
                                '<div class="donation-section"><input type="hidden" name="product_id_' + i + '" class="product_id_' + i + '"><input type="range" id="pricebar_slider_' + i + '" />' +
                                '<div><div class="price-form"><label class="amountsize" for="amount">Amount: </label><input type="hidden" id="amount"><p class="price lead" id="amount-label_' + i + '"></p><p class="price">.00</p></div>' +
                                '<input type="submit" class="btn button frontbutton" value="Donate"><div class="loaderDiv_' + i + '" style="display:none;"><img src="' + base_path_easydonation + 'image/loader.svg"></div></div></div></form></div>'; */
                        $zestard_easy('#preview_product_container').append(front_card);
                        $zestard_easy('.donationamount').on('change', function(){  
                            var dropdown_value = $zestard_easy(this).val();
                            if(dropdown_value == "other"){                                
                                $zestard_easy(this).next().css('display', 'inline-block');
                            }else{
                                $zestard_easy(this).next().css('display', 'none');
                            }
                        });
                        if (setdata) {
                            $zestard_easy('#preview_product_container').css('display', 'block');                            
                            if (donation_title) {
                                $zestard_easy('.easy_donation_title').html("<b>" + donation_title + "</b>");
								//$zestard_easy('#preview_product_container .donation_title').show();
                            } else {
                                $zestard_easy('.easy_donation_title').html("<b>Donation</b>");
                            }
                            if (donation_button_text) {
                                $zestard_easy(".frontbutton").attr("value", setdata.donation_button_text);
                            } else {
                                $zestard_easy(".frontbutton").attr("value", "Donate");
                            }

                            if (field_option == "P") {
                                $zestard_easy('#dropdown_preview_' + i).remove();
                                $zestard_easy('#textbox_preview_' + i).remove();
                                $zestard_easy('#pricebar_preview_' + i).css('display', 'block');

                                var min_val = parseInt(donation_min);
                                var max_val = parseInt(donation_max);
                                if (product_id) {
                                    $zestard_easy('.product_id_' + i).val(product_id);
                                    product_verify_id = $zestard_easy('.product_id_' + i).val();
                                }
                                if (donation_name) {
                                    $zestard_easy('.display_title_' + i).text(donation_name);
                                } else {
                                    $zestard_easy('.display_title_' + i).css('display', 'none');
                                }
                                if (donation_description) {
                                    $zestard_easy('.display_description_' + i).html(donation_description);
                                } else {
                                    $zestard_easy('.display_description_' + i).css('display', 'none');
                                }
                                if(product_image[product_verify_id] != "")
								{
									$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
								}
								else
								{
									$zestard_easy('.product-image_' + i).remove();
								}
                                $zestard_easy('#pricebar_slider_' + i).val(min_val);
                                $zestard_easy('#pricebar_slider_' + i).attr("min", min_val);
                                $zestard_easy('#pricebar_slider_' + i).attr("max", max_val);
                                $zestard_easy('#amount-label_' + i).html(shop_currency + min_val);
                                $zestard_easy('#pricebar_preview_' + i).css('display', 'block');
                                pricebarAmount(i);
                                submitPricesbar(i);
                            } else if (field_option == "D") {
                                $zestard_easy('#pricebar_preview_' + i).remove();
                                $zestard_easy('#textbox_preview_' + i).remove();
                                $zestard_easy('#dropdown_preview_' + i).css('display', 'block');
                                $zestard_easy('#dropdown_range_' + i).css('display', 'block');
                                var option_array = JSON.parse(drop_down);
                                if (product_id) {
                                    $zestard_easy('.product_id_' + i).val(product_id);
                                    product_verify_id = $zestard_easy('.product_id_' + i).val();
                                }
                                if (donation_name) {
                                    $zestard_easy('.display_title_' + i).text(donation_name);
                                } else {
                                    $zestard_easy('.display_title_' + i).css('display', 'none');
                                }
                                if (donation_description) {
                                    $zestard_easy('.display_description_' + i).html(donation_description);
                                } else {
                                    $zestard_easy('.display_description_' + i).css('display', 'none');
                                }                                
								if(product_image[product_verify_id] != "")
								{
									$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
								}
								else
								{
									$zestard_easy('.product-image_' + i).remove();
								}
                                var j = 0;
                                if (setdata.variant_ids)
                                {
                                    product_variant_ids = JSON.parse(setdata.variant_ids);
                                    variant_ids = product_variant_ids[product_id];
                                    $zestard_easy.map(variant_ids, function (value, index) {
                                        $zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
                                            value: value,
                                            text: option_array[index]
                                        }));
                                        j++;
                                    });
                                    if(dropdown_other == 1){
                                        $zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
                                           id: "click_drop_down",
                                           value: "other",
                                           text: text_dropdown_other
                                       }));
                                    }
                                } else
                                {
                                    $zestard_easy.each(option_array, function (j) {
                                        $zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
                                            value: option_array[j],
                                            text: option_array[j]
                                        }));
                                        j++;
                                    });
                                    if(dropdown_other == 1){
                                        $zestard_easy('#donation_amount_' + i).append($zestard_easy('<option>', {
                                           id: "click_drop_down",
                                           value: "other",
                                           text: text_dropdown_other
                                       }));
                                    }
                                }
                                submitDropdown(i);
                            } else if (field_option == "T") {
                                $zestard_easy('#dropdown_preview_' + i).remove();
                                $zestard_easy('#pricebar_preview_' + i).remove();
                                $zestard_easy('#textbox_preview_' + i).css('display', 'block');

                                var text_value = parseInt(text_amount);
                                if (product_id) {
                                    $zestard_easy('.product_id_' + i).val(product_id);
                                    product_verify_id = $zestard_easy('.product_id_' + i).val();
                                }
                                if (donation_name) {
                                    $zestard_easy('.display_title_' + i).text(donation_name);
                                } else {
                                    $zestard_easy('.display_title_' + i).css('display', 'none');
                                }
                                if (donation_description) {
                                    $zestard_easy('.display_description_' + i).html(donation_description);
                                } else {
                                    $zestard_easy('.display_description_' + i).css('display', 'none');
                                }
                                if (add_min_amount >= 1) {
                                    $zestard_easy(".minimum_donation_" + i).css('display', 'block');
                                    $zestard_easy(".minimum_donation_" + i).html("Minimum Donation: " + shop_currency + add_min_amount);
                                    $zestard_easy("#textbox_amount_" + i).attr("min", add_min_amount);
                                } else {
                                    $zestard_easy(".minimum_donation_" + i).css('display', 'none');
                                }								
								if(product_image[product_verify_id])
								{
									$zestard_easy('.product-image_' + i).html($zestard_easy('<img>', {id: 'ztpl-product-image', src: product_image[product_verify_id], width: 100}));
								}
								else
								{
									$zestard_easy('.product-image_' + i).remove();
								}
                                $zestard_easy('#textbox_amount_' + i).val(text_value);
                                submitTextbox(i);
                            } else {
                                $zestard_easy('#bar_range_' + i).css('display', 'none');
                                $zestard_easy('#text_range_' + i).css('display', 'none');
                                $zestard_easy('#dropdown_range_' + i).css('display', 'none');
                            }
                            $zestard_easy('#default_preview_' + i).css('display', 'none');
                            $zestard_easy('#data_preview_' + i).css('display', 'block');
                        } else {
                            $zestard_easy('#bar_range_' + i).css('display', 'none');
                            $zestard_easy('#text_range_' + i).css('display', 'none');
                            $zestard_easy('#dropdown_range_' + i).css('display', 'none');
                            $zestard_easy('#default_preview_' + i).css('display', 'block');
                            $zestard_easy('#data_preview_' + i).css('display', 'none');
                        }
                    }                   
                }
				}
				@endif
                accordion();
/*             }
        });  */       
    });

    function pricebarAmount(i) {
        $zestard_easy('#pricebar_slider_' + i).change(function () {
            var amounttext = $zestard_easy(this).val();
            var parent = $zestard_easy(this).parents('#pricebarform_' + i);
            var child = parent.find('#amount-label_' + i);
            child.html(setdata.shop_currency + amounttext);
            
        });
    }

    function submitDropdown(i) {
        $zestard_easy("#dropdownform_" + i).submit(function (e) {            
        var data = $zestard_easy("#donation_amount_" + i).val();
        if(data != "other"){
            
			if(shop_name == "hta-atlanta.myshopify.com")
			{								
				/* $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + data,
				function(result){
					reloadcart();	
				}); */
				$zestard_easy.ajax({
					url: '/cart/add.js',
					data: {quantity: 1, id: data},
					async: false					
				});
				/* $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + data); */
				reloadcart();
			}
			else
			{                
                $zestard_easy.ajax({
                    url: '/cart/add.js',
                    data: {quantity: 1, id: data},
                    async: false				
                });             	
				$zestard_easy(".loaderDiv_" + i).hide();
                if(setdata.show_popup == 1)                    
                {
                    $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                    });
                    $zestard_easy(".donate-close").click(function () {
                        $zestard_easy(".overlay").css({
                            'display': 'none'
                        });
                        reloadcart()
                    });
                }
                else
                {    
                    setTimeout(function(){ window.location.reload(true); }, 400);           				
                }
			}
			e.preventDefault();
        }
		else
		{
            var price = $zestard_easy("#dropdown_textbox_" + i).val();
            var product_id = $zestard_easy(".product_id_" + i).val();
            $zestard_easy.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/savepro') }}",
                data: {
                    'price': price,
                    'product_id': product_id,
                    'id': shop_id
                },
                beforeSend: function () {
                    $zestard_easy('.loaderDiv_' + i).css('display', 'inline-block');
                },
                success: function (productId) {					
					$zestard_easy.ajax({
                        url: '/cart/change.js',
                        data: {quantity: 0, id: parseInt(productId)},
                        async: false
                    });
                    $zestard_easy(".loaderDiv_" + i).hide();
                    if(setdata.show_popup == 1)                    
                    {
                        $zestard_easy(".overlay").css({
                        'display': 'inline-block',
                        'visibility': 'visible',
                        'opacity': '1'
                        });					
                        $zestard_easy(".donate-close").click(function () {
                            $zestard_easy(".overlay").css({
                                'display': 'none'
                            });
                            reloadcart()
                        });					 
                    }
                    else
                    {
                        setTimeout(function(){ window.location.reload(true); }, 400);
                    }
                    if(shop_name == "rocks-tell-stories.myshopify.com")
                    {                        
                        $zestard_easy.ajax({
                            url: '/cart/add.js',
                            data: {
                                quantity: 1, id: productId, properties:{'Number of Books' : $zestard_easy("#number_of_books_" + i).val()}
                            },
                            //async: false				
                        });
                    }
                    else
                    {
                        $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
                    }
					if(shop_name == "hta-atlanta.myshopify.com")
					{
						reloadcart();
					}
                }
            });
            e.preventDefault();
        }
        });
    }

    function reloadcart() {
        //setTimeout(function(){ window.location.reload(true); }, 400);
        window.location.href = "/cart";
    }

    function submitPricesbar(i) {
        $zestard_easy("#pricebarform_" + i).submit(function (e) {
            var price = $zestard_easy("#pricebar_slider_" + i).val();
            var product_id = $zestard_easy(".product_id_" + i).val();
            $zestard_easy.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/savepro') }}",
                data: {
                    'price': price,
                    'product_id': product_id,
                    'id': shop_id
                },
                beforeSend: function () {
                    $zestard_easy('.loaderDiv_' + i).css('display', 'inline-block');
                },
                success: function (productId) {

                    $zestard_easy(".loaderDiv_" + i).hide();
                    /* 14-08-2019 changes */
                    if(shop_name == "amika-1.myshopify.com")
					{
						$zestard_easy.ajax({ url: '/cart/change.js',data: {quantity: 0, id: productId},async: false });  
					    $zestard_easy.ajax({ url: '/cart/add.js',data: {quantity: 1, id: productId},async: false });   
					} else {
                        $zestard_easy.post('/cart/add.js', 'quantity=' + 1 + '&id=' + productId);
                    }                                    
                    /* end changes 14-08-2019 */
                                        
                    if(setdata.show_popup == 1)
                    { 
                        $zestard_easy(".overlay").css({
                            'display': 'inline-block',
                            'visibility': 'visible',
                            'opacity': '1'
                        });
                        $zestard_easy(".donate-close").click(function () {
                            $zestard_easy(".overlay").css({
                                'display': 'none'
                            });
                            reloadcart();
                        });
                    }
                    else
                    {
                        setTimeout(function(){ window.location.reload(true); }, 400);
                    }
                    
					if(shop_name == "hta-atlanta.myshopify.com")
					{
						reloadcart();
					}
                }
            });
            e.preventDefault();
        });
    }

    function submitTextbox(i) {
        $zestard_easy("#textboxform" + i).submit(function (e) {
            var price = $zestard_easy("#textbox_amount_" + i).val();
            var product_id = $zestard_easy(".product_id_" + i).val();
            $zestard_easy.ajax({
                type: "POST",
                url: "{{ url('frontend/dropdown/savepro') }}",
				async:false,
                data: {
                    'price': price,
                    'product_id': product_id,
                    'id': shop_id
                },
                beforeSend: function () {
                    $zestard_easy('.loaderDiv_' + i).css('display', 'inline-block');
                },
                success: function (productId){					
                    $zestard_easy(".loaderDiv_" + i).hide();
                    if(shop_name != "hta-atlanta.myshopify.com")
					{
                        if(setdata.show_popup == 1)
                        {
                            $zestard_easy(".overlay").css({
                                'display': 'inline-block',
                                'visibility': 'visible',
                                'opacity': '1'
                            });
                            $zestard_easy(".donate-close").click(function () {
                                $zestard_easy(".overlay").css({
                                    'display': 'none'
                                });
                                reloadcart()
                            });
                        }
                        else
                        {
                            //location.reload();
                            setTimeout(function(){ window.location.reload(true); }, 400);
                        }
					}                    
					$zestard_easy.ajax({ url: '/cart/change.js',data: {quantity: 0, id: productId},async: false });  
					$zestard_easy.ajax({ url: '/cart/add.js',data: {quantity: 1, id: productId},async: false });              
                }
            });
			if(shop_name == "hta-atlanta.myshopify.com")
			{
				reloadcart();
			}
            e.preventDefault();
        });
    }
</script>
<div id="donate-popup" class="overlay" style="display: none" align = "center">
    <div class="popup">
        <h2>Thanks For Donating!</h2>
        <a class="donate-close" href="#">&times;</a>        
    </div>
</div>
<script>
    if(setdata.popup_message)
    {
        $(".popup h2").html(setdata.popup_message);
    }
</script>
<div class="easy_donation_additional_css"></div>
<div id="preview_container" class="fontpreview" style="display:none;">
    <div class="donation_title" style="display:none;"><h3 class="easy_donation_title"></h3></div>
</div>
<div id="preview_product_container" class="product_fontpreview" style="display:none;">
	<div class="donation_title" style="display:none;"><h3 class="easy_donation_title"></h3></div>
</div>
<style>
        *,
        *:before,
        *:after {
            -webkit-box-sizing: border-box;
               -moz-box-sizing: border-box;
                    box-sizing: border-box;
        }
        output {
            display: block;
            font-size: 30px;
            font-weight: bold;
            text-align: center;
            margin: 30px 0;
            width: 100%;
        }

        .u-left {
            float: left;
        }

        .u-cf:before,
        .u-cf:after {
            content: "";
            display: table;
        }
        .u-cf:after {
            clear: both;
        }

        .u-text-left {
            text-align: left;
        }
    </style>
<script>
$zestard_easy(function() {
    if(shop_name == "amika-1.myshopify.com"){
    var $document = $zestard_easy(document);
    var selector = '[data-rangeslider]';
    var $element = $zestard_easy(selector);
    //var textContent = $zestard_easy(".textContent");
    // For ie8 support
    //var textContent = ('.textContent' in document) ? 'textContent' : 'innerText';

    // Example functionality to demonstrate a value feedback
    // function valueOutput($element) {
    //     var value = $element.value;
    //     var output = $element.parentNode.getElementsByTagName('output')[0] || $element.parentNode.parentNode.getElementsByTagName('output')[0];
    //     output[textContent] = value;
    // }

    // $document.on('input', 'input[type="range"], ' + selector, function(e) {
    //     valueOutput(e.target);
    // });

    // Example functionality to demonstrate disabled functionality
    $document .on('click', '#js-example-disabled button[data-behaviour="toggle"]', function(e) {
        var $inputRange = $zestard_easy(selector, e.target.parentNode);

        if ($inputRange[0].disabled) {
            $inputRange.prop("disabled", false);
        }
        else {
            $inputRange.prop("disabled", true);
        }
        $inputRange.rangeslider('update');
    });

    // Example functionality to demonstrate programmatic value changes
    $document.on('click', '#js-example-change-value button', function(e) {
        var $inputRange = $zestard_easy(selector, e.target.parentNode);
        var value = $zestard_easy('input[type="number"]', e.target.parentNode)[0].value;

        $inputRange.val(value).change();
    });

    // Example functionality to demonstrate programmatic attribute changes
    $document.on('click', '#js-example-change-attributes button', function(e) {
        var $inputRange = $zestard_easy(selector, e.target.parentNode);
        var attributes = {
                min: $zestard_easy('input[name="min"]', e.target.parentNode)[0].value,
                max: $zestard_easy('input[name="max"]', e.target.parentNode)[0].value,
                step: $zestard_easy('input[name="step"]', e.target.parentNode)[0].value
            };

        $inputRange.attr(attributes);
        $inputRange.rangeslider('update', true);
    });

    // Example functionality to demonstrate destroy functionality
    $document
        .on('click', '#js-example-destroy button[data-behaviour="destroy"]', function(e) {
            $zestard_easy(selector, e.target.parentNode).rangeslider('destroy');
        })
        .on('click', '#js-example-destroy button[data-behaviour="initialize"]', function(e) {
            $zestard_easy(selector, e.target.parentNode).rangeslider({ polyfill: false });
        });

    // Example functionality to test initialisation on hidden elements
    $document
        .on('click', '#js-example-hidden button[data-behaviour="toggle"]', function(e) {
            var $container = $zestard_easy(e.target.previousElementSibling);
            $container.toggle();
        });

    // Basic rangeslider initialization
    $element.rangeslider({

        // Deactivate the feature detection
        polyfill: false,

        // Callback function
        onInit: function() {
            //valueOutput(this.$element[0]);
        },

        // Callback function
        onSlide: function(position, value) {
            //console.log('onSlide');
            //console.log('position: ' + position, 'value: ' + value);
        },

        // Callback function
        onSlideEnd: function(position, value) {
            //console.log('onSlideEnd');
            //console.log('position: ' + position, 'value: ' + value);
        }
    });
    }

});
</script>