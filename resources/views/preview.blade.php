<div id="preview_container">
    <div id="dropdown_preview" style="display:none;">
        <form name="dropdownform">
            <div class="price-slider">
                <h2 class="display_title"></h2>
                <div>
                    <p class="product-image"></p>
                    <p class="display_description"></p>
                </div>
                <select class="donationamount" name="donation_amount" id="donation_amount"></select>
                <input type="submit" name="donate" value="Donate" class="btn button preview-button" disabled="disabled" />
            </div>          
        </form>
    </div>
    <div id="textbox_preview" style="display:none;">
        <form name="textbox_form">
            <div class="price-slider">
                <h2 class="display_title"></h2>
                <div>
                    <p class="product-image"></p>
                    <p class="display_description"></p>
                </div>
                <input type="number" name="textbox"/>
                <input type="submit" name="donate" value="Donate" class="btn button preview-button" disabled="disabled" />
            </div>          
        </form>
    </div>
    <div id="pricebar_preview" style="display:none;">
        <div class="price-box">
            <form class="form-horizontal form-pricing" role="form" name="pricebar_form">
                <div class="price-slider">
                    <h2 class="display_title"></h2>
                    <div>
                        <p class="product-image"></p>
                        <p class="display_description"></p>
                    </div>
                    <!-- <span id="displaymin_span"></span>-->
                    <input type="range" id="pricebar_slider" />
                </div>
                <div class="price-form">
                    <label class="amountsize" for="amount">Amount: </label>
                    <input type="hidden" id="amount">
                    <p class="price lead" id="amount-label"></p>
                    <p class="price">.00</p>
                </div>
                <div class="price-slider">
                    <button type="submit" class="btn button preview-button" disabled="disabled">Donate </button>
                </div>
            </form>
        </div>
    </div>
</div>