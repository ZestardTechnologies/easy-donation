
jQuery(document).ready(function () {

    //Display field options based on selection
    $('.range-field').change(function () {
        var option_change = $(this).val();
        if (option_change == "P") {
            $('.field-option').css('display', 'none');
            $('#bar_range').css('display', 'block');
        } else if (option_change == "D") {
            $('.field-option').css('display', 'none');
            $('#dropdown_range').css('display', 'block');
        } else if (option_change == "T") {
            $('.field-option').css('display', 'none');
            $('#text_range').css('display', 'block');
        } else {
            $('.field-option').css('display', 'none');
        }
    });
    //----------------- ADD DONATION PAGE START ------------------------------------
    //Add amount while dropdown field select
    $(".addrow").on('click', function () {
        var new_add = '<div class="range-inner-div dropdown_add">' +
                '<label class="amount" for="dropdown_option">Amount</label>' +
                '<div class="cus-row">' +
                '<div class="col-nine"><input name="dropdown_option[]" type="text" class="validate option_value form-control dropdown_option"></div>' +
                '<div class="col-one"><a class="removerow"><i class="fa fa-times" aria-hidden="true"></i></a></div>' +
                '</div>' +
                '<div class="error_dropval" style="color:red;"></div>';

        var parent = $(this).parents("#dropdown_range");
        parent.children('.dropdown-wrapper').append(new_add);
    });

    // Remove Amount
    $(document).on('click', '.col-one a.removerow', function () {
        $(this).parent().parent().parent('.dropdown_add').remove();
    });

    //Upload image given as Link
    $("#uploadImage").click(function () {
        $("#upload_donation_image").trigger('click');
    });

    /*
     * Donation Listing Page
     */
    $('#addDonateLink').click(function () {
        $("#addDonateLink").attr("disabled", "disabled");
        $("#addDonateLinkBtn .btn-loader-icon").css("display", "block");
        $("#addDonateLinkBtn").css("padding-left", "30px");
        return true;
    });

    /*
     * Donation Listing Page DataTable
     */
    $('.donation_table').DataTable({
        "lengthChange": false,
        "pageLength": 10,
        "paging": true,
        //"showNEntries" : false,
        "bInfo": false,
        language: {
            paginate: {
                next: '<button class="secondary icon-next"></button>', // or '?'
                previous: '<button class="secondary icon-prev"></button>' // or '?' 
            }
        },
        "fnDrawCallback": function (oSettings) {            
            var totalPage = $('.donation_table').DataTable().page.info();            
            if (!totalPage.page) {
                $('.dataTables_paginate').hide();
            }
            if (totalPage.pages > 1){
                $('.dataTables_paginate').show();
            }
        }
    });
    $(".btnClose").click(function () {
        $(".alertHide").hide();
    });

    /*
     * Time in Milliseconds Hide after 5 sec
     */
    setTimeout(function () {
        $('.alertHide').fadeOut('fast');
    }, 10000);


});