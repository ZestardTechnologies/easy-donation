var base_path_easydonation = "https://zestardshop.com/shopifyapp/easy_donation/public/";
var $zestard_easy = "";
var id;
var shop_name = Shopify.shop;
var element = document.getElementsByClassName("donation");
if (element.length > 0) {
    //Check if Jquery is Undefined or Not available.
    if (typeof jQuery == 'undefined' || shop_name == "bossy-cosmetics-inc.myshopify.com") {
        //var id = document.getElementsByClassName('donation')[0].id;
        id = document.getElementsByClassName("donation")[0].getAttribute("id");
        //If Undefined or Not available, Then Load			  
        var jscript = document.createElement("script");
        jscript.src = base_path_easydonation + "js/jquery_3.3.1.js";
        jscript.type = 'text/javascript';
        jscript.async = false;
        var el = document.getElementById(id),
            elChild = document.createElement('div');
        elChild.id = 'easy_donation_jquery';
        el.insertBefore(jscript, el.firstChild);

        //alert(document.getElementById('easy_donation_jquery'));
        jscript.onload = function() {
            //Assigning Jquery Object to Zestard_jq				
            $zestard_easy = window.jQuery;
            easy_donation();
        };
    } else {
        $zestard_easy = window.jQuery;
        easy_donation();
    }

    function easy_donation() {
        /* 
        var id = $zestard_easy('.donation').attr('id');
        var page = $zestard_easy('.donation').attr('page');
        */
        id = document.getElementsByClassName("donation")[0].getAttribute("id");
        var page = document.getElementsByClassName("donation")[0].getAttribute("page");
        var productid = document.getElementsByClassName("donation")[0].getAttribute("productid");
        //var productid = $zestard_easy('.donation').attr('productid');
        //document.getElementsByClassName('donation')[0].id;		
        if (id) {
            //$(".donation").html("Donation is Loading.... Please Wait....");
            $.ajax({
                type: "GET",
                url: base_path_easydonation + "preview",
                crossDomain: true,
                data: {
                    'id': id,
                    'page': page,
                    'productid': productid
                },
                success: function(data) {
                    if ($(window).width() <= 499) {
                        console.log('less-then-499');
                        $(".donation").html("");
                        $(".donation-mobile").html(data);
                    } else {
                        console.log('Greated-then-499');
                        $(".donation-mobile").html("");
                        $(".donation").html(data);
                    }
                    $(window).resize(function() {
                        if ($(window).width() <= 499) {
                            console.log('less-then-499');
                            $(".donation").html("");
                            $(".donation-mobile").html(data);
                        } else {
                            console.log('Greated-then-499');
                            $(".donation-mobile").html("");
                            $(".donation").html(data);
                        }
                    });


                }
            });
        }
    }
}