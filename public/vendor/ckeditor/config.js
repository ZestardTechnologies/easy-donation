/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
        config.allowedContent=true;
        config.extraAllowedContent = 'style';
        config.removeButtons = 'Save,NewPage,CreateDiv,Print,Templtes,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,ShowBlocks,Iframe,BidiLtr,BidiRtl,Language,Smiley,PageBreak,Preview,Find,Replace,SelectAll,Underline,Strikethrough,Subscript,Superscript,Font,Size,TextColor,BGColor,JustifyCenter,JustifyBlock,JustifyLeft,JustifyRight,FontSize,Templates';
        config.removePlugins = 'elementspath';
        config.resize_enabled = false;
};

